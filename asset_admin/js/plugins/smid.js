(function($){

	var tags = new Bloodhound({
		datumTokenizer: function (datum) {
			return Bloodhound.tokenizers.whitespace(datum.value);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: SMID.baseURL + 'backend/event/event/get_name_regency?term=%QUERY',
			wildcard: '%QUERY',
			filter: function (tags) {
				return $.map(tags, function (tag) {
					return {
						id: tag.id,
						name: tag.name
					};
				});
			}
		}
	});
	tags.initialize();

	$('.regencyfield').tagsinput({
		tagClass: 'label label-danger label-important',
		typeaheadjs: {
			name: 'tags',
			limit: 1000000,
			displayKey: 'name',
			source: tags.ttAdapter()
		}
	});

	var events = new Bloodhound({
		datumTokenizer: function (datum){
			return Bloodhound.tokenizers.whitespace(datum.value);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: SMID.baseURL + 'backend/event/event/get_name_event?term=%QUERY',
			wildcard: '%QUERY',
			filter: function (events) {
				return $.map(events, function (event) {
					return {
						id: event.id,
						name: event.name
					};
				});
			}
		}
	});

	$('.eventfield').tagsinput({
		tagClass:'label label-danger label-important',
		typeaheadjs: {
			name: 'tags',
			limit: 1000000,
			displayKey: 'name',
			source: events.ttAdapter()
		}
	});

	var tagging = new Bloodhound({
		datumTokenizer: function (datum){
			return Bloodhound.tokenizers.whitespace(datum.value);
		},
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: SMID.baseURL + 'backend/posts/tags/get_name_tag?term=%QUERY',
			wildcard: '%QUERY',
			filter: function (tagging) {
				return $.map(tagging, function (tag) {
					return {
						id: tag.id,
						name: tag.name
					};
				});
			}
		}
	});

	$('.tagfield').tagsinput({
		tagClass:'label label-danger label-important',
		typeaheadjs: {
			name: 'tags',
			limit: 1000000,
			displayKey: 'name',
			source: tagging.ttAdapter()
		}
	});

	$('input[maxlength]').maxlength({
		alwaysShow: true
	});

	$('textarea[maxlength]').maxlength({
		alwaysShow: true
	});

})(jQuery);