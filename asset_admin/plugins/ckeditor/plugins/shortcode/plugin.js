CKEDITOR.plugins.add( 'shortcode', {
    init: function( editor ) {
        editor.addCommand('bacajuga', {
            exec: function( editor ) {
                editor.insertHtml("[bacajuga]");
            }
        });

        editor.addCommand('pagebreak', {
            exec: function( editor ) {
                editor.insertHtml("[pagebreak]");
            }
        });

        editor.ui.addButton( 'bacajugabtn', {
            label: 'BacaJuga',
            command: 'bacajuga',
            toolbar: 'others'
        });

        editor.ui.addButton( 'pagebreakbtn', {
            label: 'PageBreak',
            command: 'pagebreak',
            toolbar: 'others'
        });
    }
});