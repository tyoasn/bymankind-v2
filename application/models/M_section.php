<?php

/**
 * Description of Category
 *
 * @author aris
 */
class M_section extends CI_Model {

    public function get(){
        $this->db->select('*');
        $this->db->where('page_is_trash', 0);
        $this->db->where('page_status', 1);
        $this->db->where('page_type_id', 2);
        $this->db->from('cms_section_page');
        $this->db->order_by('page_name', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_parent($id){
        $this->db->select('*');
        $this->db->where_not_in('page_id', $id);
        $this->db->where('page_is_trash', 0);
        $this->db->where('page_status', 1);
        $this->db->where('page_type_id', 2);
        $this->db->from('cms_section_page');
        $this->db->order_by('page_id', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function add($data) {
        $this->db->insert('cms_section_page', $data);
    }

    public function edit($data, $id){
        $this->db->where('page_id', $id);
        $this->db->update('cms_section_page', $data);
    }

    public function delete($data, $id){
        $this->db->where('page_id', $id);
        $this->db->update('cms_section_page', $data);
    }

    public function get_id_category($id){
        $this->db->where('page_id', $id);
        $query = $this->db->get('cms_section_page');

        return $query->row();
    }
    
}