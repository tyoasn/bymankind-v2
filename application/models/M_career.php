<?php

/**
 * Description of Post
 *
 * @author aris
 */
class M_career extends CI_Model {

    public function add($data) {
        $this->db->insert('cms_career', $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id){
        $this->db->where('career_id', $id);
        $this->db->update('cms_career', $data);
    }

    public function delete($data, $id){
        $this->db->where('career_id', $id);
        $this->db->update('cms_career', $data);
    }

    public function get_id_post($id){
        $this->db->where('career_id', $id);
        $query = $this->db->get('cms_career');

        return $query->row();
    }

    
}