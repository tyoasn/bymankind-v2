<?php

/**
 * Description of User
 *
 * @author aris
 */
class M_user extends CI_Model {

    public function get(){
        $this->db->select('*');
        $this->db->from('cms_action');
        $this->db->order_by('action_id', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function add($data) {
        $this->db->insert('cms_users', $data);
    }

    public function edit($data, $id){
        $this->db->where('user_id', $id);
        $this->db->update('cms_users', $data);
    }

    public function delete($data, $id){
        $this->db->where('user_id', $id);
        $this->db->update('cms_users', $data);
    }

    public function get_id_user($id){
        $this->db->where('user_id', $id);
        $query = $this->db->get('cms_users');

        return $query->row();
    }

    public function check_access_group_action($access_id,$group_id,$action_id){
    	$this->db->select('*');
    	$this->db->where('access_id', $access_id);
    	$this->db->where('group_id', $group_id);
    	$this->db->where('action_id', $action_id);
    	$this->db->from('cms_access_group_action');
    	$query = $this->db->get();

    	return $query;
    }

    public function check_access_action($access_id,$action_id){
    	$this->db->select('*');
    	$this->db->where('access_id', $access_id);
    	$this->db->where('action_id', $action_id);
    	$this->db->from('cms_access_action');
    	$query = $this->db->get();

    	return $query;
    }
	
}