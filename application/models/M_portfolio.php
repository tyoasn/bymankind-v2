<?php

/**
 * Description of Post
 *
 * @author aris
 */
class M_portfolio extends CI_Model {

    public function add($data) {
        $this->db->insert('cms_portfolio', $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id){
        $this->db->where('portfolio_id', $id);
        $this->db->update('cms_portfolio', $data);
    }

    public function delete($data, $id){
        $this->db->where('portfolio_id', $id);
        $this->db->update('cms_portfolio', $data);
    }


    public function get_id_post($id){
        $this->db->where('portfolio_id', $id);
        $query = $this->db->get('cms_portfolio');

        return $query->row();
    }
	
}