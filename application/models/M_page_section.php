<?php

/**
 * Description of User
 *
 * @author aris
 */
class M_page_section extends CI_Model {

    public function add($data) {
        $this->db->insert('cms_section_page', $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id){
        $this->db->where('page_id', $id);
        $this->db->update('cms_section_page', $data);
    }

    public function delete($data, $id){
        $this->db->where('page_id', $id);
        $this->db->update('cms_section_page', $data);
    }

    public function get_page_id($id){
        $this->db->where('page_id', $id);
        $query = $this->db->get('cms_section_page');

        return $query->row();
    }

    public function get_id_page($id){
        // if($id == 1){
        //     $query = 'E-Magazine';
        // } elseif ($id == 2) {
        //     $query = 'Event';
        // } elseif ($id == 3) {
        //     $query = 'Organization';
        // } elseif ($id == 4) {
        //     $query = 'Formulir Permohonan Informasi Publik';
        // } elseif ($id == 5) {
        //     $query = 'Formulir Keberatan atas Informasi Publik';
        // } elseif ($id == 6) {
        //     $query = 'FAQ';
        // } elseif ($id == 0) {
        //     $query = 'Parent';
        // } else{
        //     // $this->db->where('page_id', $id)->where_in('page_url', array('', '#'));
        //     // $query = $this->db->get('cms_pages')->row()->page_name;

        //     $this->db->where('page_id', $id);
        //     $query = $this->db->get('cms_pages')->row()->page_name;
        // }

        if($id == 0){
            $query = 'Parent';
        } else {
            $this->db->where('page_id', $id);
            $query = $this->db->get('cms_section_page')->row()->page_name;
        }

        return $query;
    }

    public function get($type){
        $query = $this->db->where('page_status', 1)->where('page_is_trash', 0)->where('page_type_id', $type)->order_by('page_name', 'asc')->get('cms_section_page');

        return $query->result();
    }

    public function get_parent($id){
        $query = $this->db->where_not_in('page_id', $id)->where('page_status', 1)->where('page_is_trash', 0)->where('page_type_id', 1)->order_by('page_id', 'desc')->get('cms_section_page');

        return $query->result();
    }
    
}