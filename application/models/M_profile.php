<?php


class M_profile extends CI_Model {

    public function get(){
        $this->db->select('*');
        $this->db->from('action');
        $this->db->order_by('action_id', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function add($data) {
        $this->db->insert('users', $data);
    }

    public function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function delete($data, $id){
        $this->db->where('id', $id);
        $this->db->update('users', $data);
    }

    public function get_id_user($id){
        $this->db->where('id', $id);
        $query = $this->db->get('users');

        return $query->row();
    }

	
}