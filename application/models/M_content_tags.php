<?php

/**
 * Description of Tags
 *
 * @author aris
 */
class M_content_tags extends CI_Model {

    public function add($data) {
        $this->db->insert('cms_taxonomy', $data);
    }

    public function edit($data, $id){
        $this->db->where('taxonomy_id', $id);
        $this->db->update('cms_taxonomy', $data);
    }

    public function delete($id){
        $this->db->delete('cms_taxonomy', array('taxonomy_id' => $id));
    }

    public function get_id_tag($id){
        $this->db->where('taxonomy_id', $id);
        $query = $this->db->get('cms_taxonomy');

        return $query->row();
    }

    public function get_name_tag($term){
        $this->db->select('taxonomy_id, taxonomy_name');
        $this->db->like('taxonomy_name', $term);
        $this->db->order_by('taxonomy_id', 'desc');
        $query = $this->db->get('cms_taxonomy');
        $get = $query->result();

        if (count($get) < 1)
            return false;

        return $get;
    }

    public function get_name_tag_id($id){
        $this->db->select('*');
        $this->db->from('cms_taxonomy');
        $this->db->where('taxonomy_id', $id);
        $this->db->where('taxonomy_is_trash',0);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row()->taxonomy_name;
        }
        return false;
    }
    
}