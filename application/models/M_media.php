<?php

/**
 * Description of Postmedia
 *
 * @author aris
 */
class M_media extends CI_Model {

	public function get($type, $limit, $offset){
		$this->db->where('media_type', $type);
		$this->db->where('media_directory', base_url().'asset_admin/assets/uploads/media/image/');
		$this->db->order_by('media_id', 'desc');
		$this->db->limit($limit,$offset);
		$query = $this->db->get('cms_media');

        return $query->result();
	}

	public function get_current($postid, $type, $limit, $offset){
		$this->db->where('cms_media.media_type', $type);
		$this->db->where('cms_post_media.post_id', $postid);
		$this->db->order_by('cms_post_media.media_id', 'desc');
		$this->db->limit($limit,$offset);
		$this->db->join('cms_media', 'cms_media.media_id = cms_post_media.media_id');
		$query = $this->db->get('cms_post_media');

        return $query->result();
	}

	public function get_current_page($pageid, $type, $limit, $offset){
		$this->db->where('cms_media.media_type', $type);
		$this->db->where('cms_page_media.page_id', $pageid);
		$this->db->order_by('cms_page_media.media_id', 'desc');
		$this->db->limit($limit,$offset);
		$this->db->join('cms_media', 'cms_media.media_id = cms_page_media.media_id');
		$query = $this->db->get('cms_page_media');

        return $query->result();
	}

	public function count($type){
		$this->db->where('media_type', $type);
		$query = $this->db->get('cms_media');

        return $query->num_rows();
	}

	public function count_current($postid){
		$this->db->where('post_id', $postid);
		$query = $this->db->get('cms_post_media');

        return $query->num_rows();
	}

	public function count_current_page($page_id){
		$this->db->where('page_id', $page_id);
		$query = $this->db->get('cms_page_media');

        return $query->num_rows();
	}

	public function add($data) {
        $this->db->insert('cms_media', $data);
        return $this->db->insert_id();
    }

    public function add_post_media($data) {
        $this->db->insert('cms_post_media', $data);
        return $this->db->insert_id();
    }

    public function add_page_media($data){
    	$this->db->insert('cms_page_media', $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id){
        $this->db->where('id', $id);
        $this->db->update('cms_media', $data);
    }

    public function delete($id){
    	$this->db->delete('cms_post_media', array('media_id' => $id));
    	$this->db->delete('cms_media', array('media_id' => $id));
    }

    public function get_media_id($id){
    	$this->db->where('media_id', $id);
    	$query = $this->db->get('cms_media');

    	return $query->row();
    }

}