<?php

/**
 * Description of Access
 *
 * @author aris
 */
class M_access extends CI_Model {

    public function get(){
        $this->db->select('*');
        //hide category and tags
        // $this->db->where_not_in('access_id', 4);
        // $this->db->where_not_in('access_id', 5);
        $this->db->from('cms_access');
        $this->db->order_by('access_order_by', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_access_group_action($group_id, $action_id, $access_id=""){
    	$this->db->select('cms_access.*');
    	$this->db->where('cms_access_group_action.group_id', $group_id);
        $this->db->where('cms_access_group_action.action_id', $action_id);
        if($access_id != ""){
            $this->db->where('cms_access_group_action.access_id', $access_id);
        }

    	$this->db->order_by('cms_access.access_order_by', 'asc');
    	//$this->db->group_by('cms_access_group_action.access_id');
        $this->db->group_by('cms_access.access_name');
        $this->db->from('cms_access');
        $this->db->join('cms_access_group_action', 'cms_access_group_action.access_id = cms_access.access_id', 'left');
        
        $query = $this->db->get();

        return $query->result();
    }

    // public function get_access_exists($access_alias){
    //     $this->db->select('*');
    //     //hide groups
    //     $this->db->where_not_in('access_id', 7);
    //     $this->db->like('access_alias', $access_alias);
    //     $this->db->from('cms_access');
    //     $this->db->order_by('access_order_by', 'asc');
    //     $query = $this->db->get();
    //     return $query->result();
    // }

    public function get_access_exists($access_alias, $group_id, $action_id, $access_id=""){
        $this->db->select('cms_access.*');
        $this->db->like('cms_access.access_alias', $access_alias);
        $this->db->where('cms_access_group_action.group_id', $group_id);
        $this->db->where('cms_access_group_action.action_id', $action_id);
        if($access_id != ""){
            $this->db->where('cms_access_group_action.access_id', $access_id);
        }

        $this->db->order_by('cms_access.access_order_by', 'asc');
        //$this->db->group_by('cms_access_group_action.access_id');
        //$this->db->group_by('cms_access.access_name');
        $this->db->from('cms_access');
        $this->db->join('cms_access_group_action', 'cms_access_group_action.access_id = cms_access.access_id', 'left');
        
        $query = $this->db->get();
        //hide category and tags
        // $this->db->where_not_in('access_id', 4);
        // $this->db->where_not_in('access_id', 5);
        return $query->result();
    }
	
}