<?php

/**
 * Description of Action
 *
 * @author aris
 */
class M_action extends CI_Model {

    public function get(){
        $this->db->select('*');
        $this->db->from('cms_action');
        $this->db->order_by('action_id', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_id_action($id){
        $this->db->where('action_id', $id);
        $query = $this->db->get('cms_action');

        return $query->row();
    }

    public function check_access_group_action($access_id,$group_id,$action_id){
    	$this->db->select('*');
    	$this->db->where('access_id', $access_id);
    	$this->db->where('group_id', $group_id);
    	$this->db->where('action_id', $action_id);
    	$this->db->from('cms_access_group_action');
    	$query = $this->db->get();

    	return $query;
    }

    public function check_access_action($access_id,$action_id){
    	$this->db->select('*');
    	$this->db->where('access_id', $access_id);
    	$this->db->where('action_id', $action_id);
    	$this->db->from('cms_access_action');
    	$query = $this->db->get();

    	return $query;
    }
	
}