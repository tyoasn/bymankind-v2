<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_frontend extends CI_Model {

    function __construct() {
        parent::__construct();
    }


    function get_detail_config($config_key){
        $this->db->select('config_value');
        $this->db->where('config_key', $config_key);
        $query = $this->db->get('cms_config');

        return $query->row();
    }
    function get_detail_content($post_title){
        $this->db->select('post_content');
        $this->db->where('post_title', $post_title);
        $query = $this->db->get('cms_contents');

        return $query->row();
    }

    public function getPortfolioDetail($permalink){
        $this->db->from('cms_portfolio');
        $this->db->where('portfolio_slug', $permalink);
        $this->db->where('portfolio_status', 1);
        $this->db->where('portfolio_is_trash', 0);
        $query = $this->db->get()->row();

        if($query){
            return $query;
        }

        return false;
    }





}