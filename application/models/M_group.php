<?php

/**
 * Description of Group
 *
 * @author aris
 */
class M_group extends CI_Model {

    public function get(){
        $this->db->select('*');
        $this->db->from('cms_groups');
        $this->db->order_by('group_id', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

	public function add($data) {
        $this->db->insert('cms_groups', $data);
    }

    public function edit($data, $id){
    	$this->db->where('group_id', $id);
        $this->db->update('cms_groups', $data);
    }

    public function delete($data, $id){
    	$this->db->where('group_id', $id);
        $this->db->update('cms_groups', $data);
    }

    public function get_id_group($id){
    	$this->db->where('group_id', $id);
        $query = $this->db->get('cms_groups');

        return $query->row();
    }

    public function add_access_group_action($data){
        $this->db->insert('cms_access_group_action', $data);
    }

    public function delete_access_group_action($id){
        $this->db->delete('cms_access_group_action', array('group_id' => $id));
    }
	
}