<?php

/**
 * Description of Post
 *
 * @author aris
 */
class M_content extends CI_Model {

    public function add($data) {
        $this->db->insert('cms_contents', $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id){
        $this->db->where('post_id', $id);
        $this->db->update('cms_contents', $data);
    }

    public function delete($data, $id){
        $this->db->where('post_id', $id);
        $this->db->update('cms_contents', $data);
    }

    public function delete_post_taxonomy($id){
        $this->db->where('post_id', $id);
        $this->db->delete('cms_post_taxonomy');
    }

    public function get_id_post($id){
        $this->db->where('post_id', $id);
        $query = $this->db->get('cms_contents');

        return $query->row();
    }

    public function get_tags_post($id){
        $this->db->select('*');
        $this->db->from('cms_post_taxonomy');
        $this->db->where('post_id', $id);
        $query = $this->db->get();

        return $query->result();
    }

    public function selection_tags($arr_tags, $id){
        $this->db->select('taxonomy_id');
        $this->db->from('cms_taxonomy');
        $this->db->where('taxonomy_name', $arr_tags);
        $query = $this->db->get();

        if ($query->num_rows() == 0) {
            $name_small = strtolower($arr_tags);

            $this->db->insert('cms_taxonomy', array('taxonomy_name' => $name_small, 'taxonomy_description' => $name_small, 'taxonomy_slug' => url_title($name_small, 'dash', true),  'taxonomy_type' => 'tag', 'taxonomy_created_at' => date('Y-m-d H:i:s'), 'taxonomy_created_by' => $this->sess_id, 'taxonomy_is_trash' => 0));

            $id_tags = $this->db->insert_id();

            $this->db->insert('cms_post_taxonomy', array('post_id' => $id, 'taxonomy_id' => $id_tags));
        } else {
            $id_tags = $query->row()->taxonomy_id;

            $this->db->insert('cms_post_taxonomy', array('post_id' => $id, 'taxonomy_id' => $id_tags));
        }
    }
    
}