<?php

/**
 * Description of Category
 *
 * @author aris
 */
class M_division extends CI_Model {

    public function get(){
        $this->db->select('*');
        $this->db->where('division_is_trash', 0);
        $this->db->where('division_status', 1);
        $this->db->where('division_type_id', 2);
        $this->db->from('cms_division');
        $this->db->order_by('division_name', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function get_parent($id){
        $this->db->select('*');
        $this->db->where_not_in('division_id', $id);
        $this->db->where('division_is_trash', 0);
        $this->db->where('division_status', 1);
        $this->db->where('division_type_id', 2);
        $this->db->from('cms_division');
        $this->db->order_by('division_id', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    public function add($data) {
        $this->db->insert('cms_division', $data);
    }

    public function edit($data, $id){
        $this->db->where('division_id', $id);
        $this->db->update('cms_division', $data);
    }

    public function delete($data, $id){
        $this->db->where('division_id', $id);
        $this->db->update('cms_division', $data);
    }

    public function get_id_category($id){
        $this->db->where('division_id', $id);
        $query = $this->db->get('cms_division');

        return $query->row();
    }
	
}