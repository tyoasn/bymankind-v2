<?php

/**
 * Description of Post
 *
 * @author aris
 */
class M_team extends CI_Model {

    public function add($data) {
        $this->db->insert('cms_team', $data);
        return $this->db->insert_id();
    }

    public function edit($data, $id){
        $this->db->where('team_id', $id);
        $this->db->update('cms_team', $data);
    }

    public function delete($data, $id){
        $this->db->where('team_id', $id);
        $this->db->update('cms_team', $data);
    }

    public function get_id_post($id){
        $this->db->where('team_id', $id);
        $query = $this->db->get('cms_team');

        return $query->row();
    }

    
}