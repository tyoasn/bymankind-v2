<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	    public function __construct() {
        parent::__construct();

        $this->load->model('m_frontend');
        $this->load->model('m_media');
         $this->load->model('m_content');
         $this->load->helper("custom");

        //general

        $this->title = $this->m_frontend->get_detail_config('Site Title');
        $this->keywords = $this->m_frontend->get_detail_config('Keywords');
        $this->description = $this->m_frontend->get_detail_config('Description');
        $this->linkedin = $this->m_frontend->get_detail_config('Linkedin');
        $this->instagram = $this->m_frontend->get_detail_config('Instagram');
        $this->favicon = $this->m_frontend->get_detail_config('Favicon');
        $this->logo = $this->m_frontend->get_detail_config('Site Logo');

        // content

        // this is
        $this->about_1 = $this->m_frontend->get_detail_content('This is mankind');
        $this->about_2 = $this->m_frontend->get_detail_content('Who we are');
        $this->about_3 = $this->m_frontend->get_detail_content('Personalized collaboration');

        // how
        $this->services_1 = $this->m_frontend->get_detail_content('How mankind do it');
        $this->services_2 = $this->m_frontend->get_detail_content('Digital marketing');
        $this->services_3 = $this->m_frontend->get_detail_content('Mobile development');
        $this->services_4 = $this->m_frontend->get_detail_content('Website development');
        $this->services_5 = $this->m_frontend->get_detail_content('Advertising');
        $this->services_6 = $this->m_frontend->get_detail_content('Social media');
        $this->services_7 = $this->m_frontend->get_detail_content('Branding');

        // the team
        $this->team_1 = $this->m_frontend->get_detail_content('Mankind Frasa');
        $this->team_2 = $this->m_frontend->get_detail_content('Mankind Studio');
        $this->team_3 = $this->m_frontend->get_detail_content('Mankind Technology');

        // career
        $this->career_1 = $this->m_frontend->get_detail_content('Career');
        $this->career_2 = $this->m_frontend->get_detail_content('Current openings'); ;

    }

	public function index()
	{
		$data['base'] = 'home';
		$data['title'] = 'Home';
		$data['mainpage'] = 'frontend/home';
		$data['footer'] = 'frontend/footer';
		$this->load->view('frontend/template', $data);
	}
	public function thisis()
	{
		$data['base'] = 'thisis';
		$data['title'] = 'About';
		$data['mainpage'] = 'frontend/thisis';
		$data['footer'] = 'frontend/footer';
		$this->load->view('frontend/template', $data);
	}
	public function how()
	{
		$data['base'] = 'how';
		$data['title'] = 'Services';
		$data['mainpage'] = 'frontend/how';
		$data['footer'] = 'frontend/footer';
		$this->load->view('frontend/template', $data);
	}
	public function we()
	{
		// division 1
		$this->db->where_in('team_category_id', array('1'));
		$this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "asc");
        $photo_query = $this->db->get('cms_team');
        $data['team1'] = $photo_query->result_array();
        // division 2
        $this->db->where_in('team_category_id', array('2'));
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "asc");
        $photo_query = $this->db->get('cms_team');
        $data['team2'] = $photo_query->result_array();
        // division 3
        $this->db->where_in('team_category_id', array('3'));
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "asc");
        $photo_query = $this->db->get('cms_team');
        $data['team3'] = $photo_query->result_array();
        // division 4
        $this->db->where_in('team_category_id', array('4'));
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "asc");
        $photo_query = $this->db->get('cms_team');
        $data['team4'] = $photo_query->result_array();

		$data['base'] = 'we';
		$data['title'] = 'The Team';
		$data['mainpage'] = 'frontend/we';
		$data['footer'] = 'frontend/footer';
		$this->load->view('frontend/template', $data);
	}
	public function doit()
	{
        $this->db->where_in('portfolio_is_trash', array('0'));
        $this->db->order_by("portfolio_id", "asc");
        $photo_query = $this->db->get('cms_portfolio');
        $data['portfolio'] = $photo_query->result_array();


		$data['base'] = 'doit';
		$data['title'] = 'Works';
		$data['mainpage'] = 'frontend/doit';
		$data['footer'] = 'frontend/footer';
		$this->load->view('frontend/template', $data);
	}
	public function career()
	{

		$this->db->where_in('career_is_trash', array('0'));
        $this->db->order_by("career_id", "asc");
        $photo_query = $this->db->get('cms_career');
        $data['career'] = $photo_query->result_array();
		$data['base'] = 'career';
		$data['title'] = 'Career';
		$data['mainpage'] = 'frontend/career';
		$data['footer'] = 'frontend/footer';
		$this->load->view('frontend/template', $data);
	}                                                        
	public function contact()
	{
		$data['base'] = 'contact';
		$data['title'] = 'Contact';

        if ($this->input->post('submit')) {
            $valid = $this->form_validation;
            $valid->set_rules('name', 'Name', 'required');
            $valid->set_rules('company', 'Company', 'required');
            $valid->set_rules('email', 'Email', 'required|valid_email');
            $valid->set_rules('message', 'Message', 'required');

            if ($valid->run() == false) {
                // run
            } else {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.gmail.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'aris@bymankind.com', 
                    'smtp_pass' => 'apaajaboleh',
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8',
                    'wordwrap'  => true
                );

                // $config = array(
                //     'useragent' => 'Codeigniter',
                //     'mailpath' => '/usr/sbin/sendmail',
                //     'protocol' => 'smtp',
                //     'smtp_host' => 'localhost',
                //     'smtp_port' => 25,
                //     'mailtype'  => 'html',
                //     'charset'   => 'utf-8',
                //     'wordwrap'  => true
                // );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from($this->input->post('email'), $this->input->post('name'));
                $this->email->to('info@bymankind.com');
                $this->email->subject($this->input->post('name').''.$this->input->post('company').' - BYMANKIND');
                $isi = "Message: \n".$this->input->post('message');
                $this->email->message($isi);

                if($this->email->send()){
                    $this->session->set_flashdata('success', 'Message Sent');
                    redirect('contact');
                } else {
                    $this->session->set_flashdata('error', 'Message Failed to Send');

                    redirect('contact');
                }
            }
        }

		$data['mainpage'] = 'frontend/contact';
		$data['footer'] = 'frontend/footer';
		$this->load->view('frontend/template', $data);
	}
    public function portfolio()

    {
        $slug = $this->uri->segment(2);
        $data['detail'] = $this->m_frontend->getPortfolioDetail($slug);

        if($data['detail'] == FALSE)
            show_404();

        $data['base'] = $data['detail']->portfolio_id;
        $data['title'] = $data['detail']->portfolio_title;

        if($data['detail']->portfolio_is_thumbnail == 1){
            $media = $this->m_media->get_media_id($data['detail']->portfolio_thumbnail);
            if($media){
                $thumbpath = pathinfo($media->media_name);
                $image = base_url().'asset_admin/assets/uploads/media/image/'.$thumbpath['filename'].'_slider.'.$thumbpath['extension'];
            } else {
                $image = 'https://via.placeholder.com/1920x1080';
            }
        } else {
            $image = 'https://via.placeholder.com/1920x1080';
        }
        $data['metadata']['image_share'] = $image;
        $data['metadata']['image_filename'] = $image;

        $data['mainpage'] = 'frontend/portfolio';
        $data['footer'] = 'frontend/footer';
        $this->load->view('frontend/template', $data);
    }


}
