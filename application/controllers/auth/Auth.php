<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Created by tyo.
 * User: mankind
 * Date: 20/04/17
 * Time: 10:46 AM
 */
class Auth extends CI_Controller {

    public function index() {
        if ($this->session->userdata('login')) {
            redirect('backend/dashboard/dashboard');
        }
        $this->load->view('auth/login');
    }

    public function check() {
        $this->load->model('m_auth');
        $user = $this->input->post('username');
        $pass = md5($this->input->post('password'));

        $login = $this->m_auth->check_login($user, $pass);

        $row = $login->row();

        if ($login->num_rows() == 1) {
            $session = array('user_id' => $row->user_id, 'user_username' => $row->user_username,
                             'user_photo' => $row->user_photo, 'login' => true, 'group_id' => $row->group_id);
            $this->session->set_userdata($session);

            $last_login = array('user_last_login' => time());
            $this->db->where('user_id', $row->user_id);
            $this->db->update('cms_users', $last_login);

            redirect('backend/dashboard/dashboard');
        }
        $this->session->set_flashdata('error', 'The username or password is incorrect');
        redirect('admin');
    }

    function logout() {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_username');
        $this->session->unset_userdata('login');
        $this->session->sess_destroy();

        //kcfinder session
        session_start();
        unset($_SESSION['ses_kcfinder']);

        redirect('admin');
    }

    function forgotpassword(){
        $data['base'] = 'Forgot_Password';

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

            if ($this->form_validation->run() == false) {

            } else {
                $this->db->where('user_email', $this->input->post('email'));
                $this->db->where('user_status', '1');
                $queryemail = $this->db->get('cms_users');

                if ($queryemail->num_rows() > 0) {
                    $dataemail = $queryemail->row_array();

                    $user_forgotten_password_time = time();
                    //$user_activation_code = date("YmdHis") . $this->generateString(5);
                    $user_forgotten_password_code = $this->generateString(12);

                    $update_log = array(
                        //'user_activation_code' => $user_activation_code,
                        'user_forgotten_password_code' => $user_forgotten_password_code,
                        'user_forgotten_password_time' => $user_forgotten_password_time,
                        'user_forgot_status' => 1
                    );

                    $this->db->where('user_email', $dataemail['user_email']);
                    $this->db->update('cms_users', $update_log);

                    // $config = array(
                    //     'protocol' => 'smtp',
                    //     'smtp_host' => 'ssl://smtp.gmail.com',
                    //     'smtp_port' => 465,
                    //     'smtp_user' => 'aris@bymankind.com', 
                    //     'smtp_pass' => 'apaajaboleh',
                    //     'mailtype'  => 'html',
                    //     'charset'   => 'utf-8',
                    //     'wordwrap'  => true
                    // );

                    $config = array(
                        'useragent' => 'Codeigniter',
                        'mailpath' => '/usr/sbin/sendmail',
                        'protocol' => 'smtp',
                        'smtp_host' => 'localhost',
                        'smtp_port' => 25,
                        'mailtype'  => 'html',
                        'charset'   => 'utf-8',
                        'wordwrap'  => true
                    );

                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");

                    $this->email->from('no-reply@supermusic.id', 'Invasion');
                    $this->email->to($dataemail['user_email']);
                    //$this->email->cc('deka@somemail.com');
                    //$this->email->bcc('them@their-example.com');
                    $this->email->subject('Lupa Password Invasion');
                    $this->email->message('Klik link berikut atau copy link dan paste ke browser Anda untuk mengganti password akun Invasion 
                    ' . site_url('auth/auth/passwordreset/' . $user_forgotten_password_code));

                    if ($this->email->send()) {
                        //$data['status'] = "Silahkan cek email Anda untuk mengubah password.";
                        $this->session->set_flashdata('success', 'Silahkan cek email Anda untuk mengubah password.');
                        redirect('admin');
                    } else {
                        //$data['status'] = "Server gagal mengirim email. Silahkan coba beberapa saat lagi.";
                        $this->session->set_flashdata('error', 'Server gagal mengirim email. Silahkan coba beberapa saat lagi.');
                        redirect('admin');
                    }
                } else {
                    //$data['status'] = "Email yang Anda masukkan tidak terdaftar atau belum tervalidasi";
                    $this->session->set_flashdata('error', 'Email yang Anda masukkan tidak terdaftar atau belum tervalidasi.');
                    redirect('admin');
                }
            }
        }


    }

    public function passwordreset() {
        $data['base'] = 'Forgot_Password';
        $data['status'] = "1";
        $data['mess'] = "";

        if ($this->uri->segment(4)) {
            $this->db->where('user_forgotten_password_code', $this->uri->segment(4));
            $this->db->where('user_forgot_status', '1');
            $querylog = $this->db->get('cms_users');

            if ($querylog->num_rows() > 0) {
                $datalog = $querylog->row_array();

                $this->db->where('user_email', $datalog['user_email']);
                $this->db->where('user_status', '1');
                $queryuser = $this->db->get('cms_users');
                if ($queryuser->num_rows() > 0) {

                    if ($this->input->post('submit')) {
                        $this->form_validation->set_rules('password', 'Password', 'required');
                        $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required');

                        if ($this->form_validation->run() == false) {
                            
                        } else {
                            $pass1 = $this->input->post('password');
                            $pass2 = $this->input->post('confirmpassword');

                            if ($pass1 == $pass2) {
                                $datauser = $queryuser->row_array();

                                $updateuser = array(
                                    'user_password' => md5($pass1),
                                    'user_forgot_status' => 0
                                );

                                $this->db->where('user_id', $datauser['user_id']);
                                $this->db->update('cms_users', $updateuser);

                                // $updatelog = array(
                                //     'status' => '0'
                                // );

                                // $this->db->where('id', $datalog['id']);
                                // $this->db->update('log', $updatelog);

                                //$data['status'] = "0";
                                //$data['mess'] = "Password Anda berhasil diubah, silahkan Login dengan password baru Anda";

                                // $this->session->set_flashdata('registrasi', '<strong><font color=red>Password Anda berhasil diubah, silahkan Login dengan password baru Anda</font></strong>');
                                // redirect('/');

                                $this->session->set_flashdata('success', 'Password Anda berhasil diubah, silahkan Login dengan password baru Anda.');
                                redirect('admin');
                            } else {
                                //$data['error'] = "<strong>Password di Confirm Password tidak sama.</strong>";
                                $this->session->set_flashdata('error', 'Password di Confirm Password tidak sama.');
                                redirect('admin');
                            }
                        }
                    }
                } else {
                    // $data['status'] = "0";
                    // $data['mess'] = "Link yang Anda gunakan tidak valid!";
                    $this->session->set_flashdata('error', 'Link yang Anda gunakan tidak valid!');
                    redirect('admin');
                }
            } else {
                // $data['status'] = "0";
                // $data['mess'] = "Link yang Anda gunakan tidak valid!";
                $this->session->set_flashdata('error', 'Link yang Anda gunakan tidak valid!');
                redirect('admin');
            }
        } else {
            // $data['status'] = "0";
            // $data['mess'] = "Link yang Anda gunakan tidak valid!";
            $this->session->set_flashdata('error', 'Link yang Anda gunakan tidak valid!');
            redirect('admin');
        }

        $this->load->view('auth/passwordreset', $data);
    }

    public function activation(){
        $data['base'] = 'Activation';
        $data['status'] = "1";
        $data['mess'] = "";

        if ($this->uri->segment(4)) {
            $this->db->where('user_activation_code', $this->uri->segment(4));
            $this->db->where('user_status', '0');
            $querylog = $this->db->get('cms_users');

            if ($querylog->num_rows() > 0) {
                $datalog = $querylog->row_array();

                $updatelog = array(
                    'user_status' => '1'
                );

                $this->db->where('user_id', $datalog['user_id']);
                $this->db->update('cms_users', $updatelog);

                $this->session->set_flashdata('success', 'Akun anda berhasil diaktifasi, silahkan login dengan username dan password anda.');
                redirect('admin');
            } else {
                // $data['status'] = "0";
                // $data['mess'] = "Link yang Anda gunakan tidak valid!";
                $this->session->set_flashdata('error', 'Link yang Anda gunakan tidak valid!');
                redirect('admin');
            }
        } else {
            // $data['status'] = "0";
            // $data['mess'] = "Link yang Anda gunakan tidak valid!";
            $this->session->set_flashdata('error', 'Link yang Anda gunakan tidak valid!');
            redirect('admin');
        }
    }

    public function generateString($length) {
        // mulai dengan string kosong
        $v_string = "";

        // definisikan karakter-karakter yang diperbolehkan
        $possible = "0123456789bcdfghjkmnpqrstvwxyz";

        // set up sebuah counter
        $i = 0;

        // tambahkan karakter acak ke $v_string sampai $length tercapai
        while ($i < $length) {
            // ambil sebuah karakter acak dari beberapa
            // kemungkinan yang sudah ditentukan tadi
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

            // kami tidak ingin karakter ini jika sudah ada pada string
            if (!strstr($v_string, $char)) {
                $v_string .= $char;
                $i++;
            }
        }
        return $v_string;
    }

}