<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Portfolio extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_portfolio');
        $this->load->model('m_page');
        $this->load->model('m_user');
        $this->load->model('m_media');
        $this->load->model('m_action');
        $this->load->helper("custom");

        $this->access_id = 15;
	}

    public function index(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Portfolio';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where('portfolio_status', 1);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $this->db->order_by("portfolio_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_portfolio');
        //var_dump($this->db->last_query());die;
        $data['portfolio'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where('portfolio_status', 1);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 2);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('portfolio_created_by', $this->sess_id);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('portfolio_created_by', $this->sess_id);
            }
            $this->db->where('portfolio_status', 3);
            $this->db->where_in('portfolio_is_trash', array('0'));
            $query_total = $this->db->get('cms_portfolio');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total']);

        $data['mainpage'] = 'backend/portfolio/portfolio';
        $this->load->view('backend/templates', $data);
    }

    public function all(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Portfolioall';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where_in('portfolio_is_trash', array('0'));
        $this->db->order_by("portfolio_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_portfolio');
        //var_dump($this->db->last_query());die;
        $data['portfolio'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 1);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 2);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('portfolio_created_by', $this->sess_id);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('portfolio_created_by', $this->sess_id);
            }
            $this->db->where('portfolio_status', 3);
            $this->db->where_in('portfolio_is_trash', array('0'));
            $query_total = $this->db->get('cms_portfolio');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_all']);

        $data['mainpage'] = 'backend/portfolio/portfolio';
        $this->load->view('backend/templates', $data);
    }

    public function main(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Portfoliomain';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where('portfolio_created_by', $this->sess_id);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $this->db->order_by("portfolio_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_portfolio');
        //var_dump($this->db->last_query());die;
        $data['portfolio'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 1);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 2);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_all'] = $query_total->num_rows();

        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where('portfolio_created_by', $this->sess_id);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('portfolio_created_by', $this->sess_id);
            }
            $this->db->where('portfolio_status', 3);
            $this->db->where_in('portfolio_is_trash', array('0'));
            $query_total = $this->db->get('cms_portfolio');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_main']);

        $data['mainpage'] = 'backend/portfolio/portfolio';
        $this->load->view('backend/templates', $data);
    }

    public function draft(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Portfoliodraft';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where('portfolio_status', 2);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $this->db->order_by("portfolio_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_portfolio');
        $data['portfolio'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where('portfolio_status', 2);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 1);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('portfolio_created_by', $this->sess_id);
        $this->db->where_in('portfoliot_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('portfolio_created_by', $this->sess_id);
            }
            $this->db->where('portfolio_status', 3);

            $this->db->where_in('portfolio_is_trash', array('0'));
            $query_total = $this->db->get('cms_portfolio');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_draft']);

        $data['mainpage'] = 'backend/portfolio/portfolio';
        $this->load->view('backend/templates', $data);
    }

    public function pending(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Postpending';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('portfolio_title', $title);
        }
        $this->db->where('portfolio_status', 3);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $this->db->order_by("portfolio_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_portfolio');
        $data['portfolio'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 2);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where('portfolio_status', 1);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('portfolio_created_by', $this->sess_id);
        }
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('portfolio_created_by', $this->sess_id);
        $this->db->where_in('portfolio_is_trash', array('0'));
        $query_total = $this->db->get('cms_portfolio');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('portfolio_created_by', $this->sess_id);
            }
            if($title != ""){
                $this->db->like('portfolio_title', $title);
            }
            $this->db->where('portfolio_status', 3);

            $this->db->where_in('portfolio_is_trash', array('0'));
            $query_total = $this->db->get('cms_portfolio');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_pending']);

        $data['mainpage'] = 'backend/portfolio/portfolio';
        $this->load->view('backend/templates', $data);
    }

	public function add(){
		$add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
        if(count($add_action) < 1)
            show_404();

        if ($this->input->post('submit')) {
        	//var_dump($_POST);die;
        	// validation
            $valid = $this->form_validation;
            $valid->set_rules('portfolio_title', 'Title', 'required');
            if($this->group_id != 3){
                $valid->set_rules('portfolio_status', 'Status', 'required');
            }
            $valid->set_rules('portfolio_publish_date', 'Publish Date', 'required');

            if ($valid->run() == false) {
                // run
                //var_dump(validation_errors());die;
            } else {
            	$format_upload = $this->upload('portfolio_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload2 = $this->upload2('portfolio2_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload3 = $this->upload3('portfolio3_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload4 = $this->upload4('portfolio4_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload5 = $this->upload5('portfolio5_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload6 = $this->upload6('portfolio6_'.time().'_'.$this->input->post('portfolio_title'));
                if(isset($_POST['portfolio_is_thumbnail']) && $_POST['portfolio_is_thumbnail'] == 1){
                    $no_thumbnail = 0;
                } else {
                    $no_thumbnail = 1;
                }
            	$data = array(
                    'portfolio_author' => $this->sess_id,
                    'portfolio_title' => $this->input->post('portfolio_title'),
                    'portfolio_link' => $this->input->post('portfolio_link'),
                    'portfolio_slug' => url_title($this->input->post('portfolio_title'), 'dash', true),
                    'portfolio_content' => $this->input->post('portfolio_content'),
                    'portfolio_content_2' => $this->input->post('portfolio_content_2'),
                    'portfolio_content_3' => $this->input->post('portfolio_content_3'),
                    'portfolio_content_4' => $this->input->post('portfolio_content_4'),
                    'portfolio_content_5' => $this->input->post('portfolio_content_5'),
                    'portfolio_content_6' => $this->input->post('portfolio_content_6'),
                    'portfolio_content_7' => $this->input->post('portfolio_content_7'),
                    'portfolio_thumbnail' => $format_upload,
                    'portfolio_thumbnail_2' => $format_upload2,
                    'portfolio_thumbnail_3' => $format_upload3,
                    'portfolio_thumbnail_4' => $format_upload4,
                    'portfolio_thumbnail_5' => $format_upload5,
                    'portfolio_thumbnail_6' => $format_upload6,
                    'portfolio_is_thumbnail' => $no_thumbnail,
                    'portfolio_created_at' => date('Y-m-d H:i:s'),
                    'portfolio_created_by' => $this->sess_id,
                    'portfolio_publish_date' => $this->input->post('portfolio_publish_date') . " " . $this->input->post('jam') . ":" . $this->input->post('menit'),
                    'portfolio_status' => $this->input->post('portfolio_status'),
                    'portfolio_is_trash' => 0
                );

                $portfolio_id = $this->m_portfolio->add($data);

                $this->session->set_flashdata('success', 'Portfolio successfully created.');
                redirect('backend/portfolio/portfolio/all');
            }
        }

        $data['base'] = 'Portfolio';
        $data['iniform'] = true;
        $data['mainpage'] = 'backend/portfolio/add_portfolio';
        $this->load->view('backend/templates', $data);
	}

    public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

        $id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                $valid->set_rules('portfolio_title', 'Title', 'required');
                //$valid->set_rules('post_subtitle', 'Subtitle', 'required');
                //$valid->set_rules('portfolio_content', 'Content', 'required');
                if($this->group_id != 3){
                    $valid->set_rules('portfolio_status', 'Status', 'required');
                }
                $valid->set_rules('portfolio_publish_date', 'Publish Date', 'required');
                //$valid->set_rules('post_taxonomy', 'Tags', 'required');

                if ($valid->run() == false) {
                    // run
                } else {

                    $format_upload = $this->upload('portfolio_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload2 = $this->upload2('portfolio2_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload3 = $this->upload3('portfolio3_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload4 = $this->upload4('portfolio4_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload5 = $this->upload5('portfolio5_'.time().'_'.$this->input->post('portfolio_title'));
                    $format_upload6 = $this->upload6('portfolio6_'.time().'_'.$this->input->post('portfolio_title'));
                    if(isset($_POST['portfolio_is_thumbnail']) && $_POST['portfolio_is_thumbnail'] == 1){
                        $no_thumbnail = 0;
                        
                        $update_media = array(
                            'portfolio_thumbnail' => 0
                        );
                        $this->m_portfolio->edit($update_media, $id);
                    } else {
                        $no_thumbnail = 1;
                    }
                    if ($format_upload != "") {
                        $data = array(
                            'portfolio_title' => $this->input->post('portfolio_title'),
                            'portfolio_link' => $this->input->post('portfolio_link'),
                            'portfolio_slug' => url_title($this->input->post('portfolio_title'), 'dash', true),
                            'portfolio_content' => $this->input->post('portfolio_content'),
                            'portfolio_content_2' => $this->input->post('portfolio_content_2'),
                            'portfolio_content_3' => $this->input->post('portfolio_content_3'),
                            'portfolio_content_4' => $this->input->post('portfolio_content_4'),
                            'portfolio_content_5' => $this->input->post('portfolio_content_5'),
                            'portfolio_content_6' => $this->input->post('portfolio_content_6'),
                            'portfolio_content_7' => $this->input->post('portfolio_content_7'),
                            'portfolio_thumbnail' => $format_upload,
                            'portfolio_thumbnail_2' => $format_upload2,
                            'portfolio_thumbnail_3' => $format_upload3,
                            'portfolio_thumbnail_4' => $format_upload4,
                            'portfolio_thumbnail_5' => $format_upload5,
                            'portfolio_thumbnail_6' => $format_upload6,
                            'portfolio_is_thumbnail' => $no_thumbnail,
                            //'post_is_editor_choice' => $this->input->post('post_is_editor_choice') ? 1 : 0,
                            'portfolio_modified_at' => date('Y-m-d H:i:s'),
                            'portfolio_modified_by' => $this->sess_id,
                            'portfolio_publish_date' => $this->input->post('portfolio_publish_date') . " " . $this->input->post('jam') . ":" . $this->input->post('menit'),
                            'portfolio_status' => $this->input->post('portfolio_status'),
                            'portfolio_is_trash' => 0
                        );

                        $this->m_portfolio->edit($data, $id);
                    } else {
                        $data = array(
                            'portfolio_title' => $this->input->post('portfolio_title'),
                            'portfolio_link' => $this->input->post('portfolio_link'),
                            'portfolio_slug' => url_title($this->input->post('portfolio_title'), 'dash', true),
                            'portfolio_content' => $this->input->post('portfolio_content'),
                            'portfolio_content_2' => $this->input->post('portfolio_content_2'),
                            'portfolio_content_3' => $this->input->post('portfolio_content_3'),
                            'portfolio_content_4' => $this->input->post('portfolio_content_4'),
                            'portfolio_content_5' => $this->input->post('portfolio_content_5'),
                            'portfolio_content_6' => $this->input->post('portfolio_content_6'),
                            'portfolio_content_7' => $this->input->post('portfolio_content_7'),
                            'portfolio_thumbnail' => $format_upload,
                            'portfolio_thumbnail_2' => $format_upload2,
                            'portfolio_thumbnail_3' => $format_upload3,
                            'portfolio_thumbnail_4' => $format_upload4,
                            'portfolio_thumbnail_5' => $format_upload5,
                            'portfolio_thumbnail_6' => $format_upload6,
                            'portfolio_is_thumbnail' => $no_thumbnail,
                            //'post_is_editor_choice' => $this->input->post('post_is_editor_choice') ? 1 : 0,
                            'portfolio_modified_at' => date('Y-m-d H:i:s'),
                            'portfolio_modified_by' => $this->sess_id,
                            'portfolio_publish_date' => $this->input->post('portfolio_publish_date') . " " . $this->input->post('jam') . ":" . $this->input->post('menit'),
                            'portfolio_status' => $this->input->post('portfolio_status'),
                            'portfolio_is_trash' => 0
                        );

                        $this->m_portfolio->edit($data, $id);
                    }

                    redirect('backend/portfolio/portfolio/all');
                }
            }

            $data['base'] = 'Group';
            $data['iniform'] = true;


            $data['portfolio'] = $this->m_portfolio->get_id_post($id);
            $data['mainpage'] = 'backend/portfolio/edit_portfolio';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/portfolio/portfolio/all');
        }
    }

    public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $data = array('portfolio_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_portfolio->delete($data, $id);
            redirect('backend/portfolio/portfolio/all');
        } else {
            redirect('backend/portfolio/portfolio/all');
        }
    }

	/**
     * Upload images
     * @return string
     */
    private function upload($rename) {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload();

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '50%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '50%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload = $mediaId;
        }

        return $format_upload;
    }

    private function upload2($rename) {
        $this->load->library('image_lib');
        $format_upload2 = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile2']['name']) && $_FILES['userfile2']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile2');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '50%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '50%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload2 = $mediaId;
        }

        return $format_upload2;
    }

    private function upload3($rename) {
        $this->load->library('image_lib');
        $format_upload3 = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile3']['name']) && $_FILES['userfile3']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile3');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '50%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '50%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload3 = $mediaId;
        }

        return $format_upload3;
    }
    private function upload4($rename) {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile4']['name']) && $_FILES['userfile4']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile4');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '50%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '50%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload4 = $mediaId;
        }

        return $format_upload4;
    }
    private function upload5($rename) {
        $this->load->library('image_lib');
        $format_upload5 = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile5');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '50%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '50%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload5 = $mediaId;
        }

        return $format_upload5;
    }
        private function upload6($rename) {
        $this->load->library('image_lib');
        $format_upload6 = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile6');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '50%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '50%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload6 = $mediaId;
        }

        return $format_upload6;
    }

}