<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Content extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('m_content');
        $this->load->model('m_page_section');
        $this->load->model('m_tags');
        $this->load->model('m_user');
        $this->load->model('m_media');
        $this->load->model('m_action');
        $this->load->helper("custom");

        $this->access_id = 11;
    }

    public function index(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Content';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where('post_status', 1);
        $this->db->where_in('post_is_trash', array('0'));
        $this->db->order_by("post_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_contents');
        //var_dump($this->db->last_query());die;
        $data['post'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where('post_status', 1);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 2);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('post_created_by', $this->sess_id);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('post_created_by', $this->sess_id);
            }
            $this->db->where('post_status', 3);
            $this->db->where_in('post_is_trash', array('0'));
            $query_total = $this->db->get('cms_contents');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total']);

        $data['mainpage'] = 'backend/content/content';
        $this->load->view('backend/templates', $data);
    }

    public function all(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Contentall';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where_in('post_is_trash', array('0'));
        $this->db->order_by("post_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_contents');
        //var_dump($this->db->last_query());die;
        $data['post'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 1);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 2);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('post_created_by', $this->sess_id);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('post_created_by', $this->sess_id);
            }
            $this->db->where('post_status', 3);
            $this->db->where_in('post_is_trash', array('0'));
            $query_total = $this->db->get('cms_contents');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_all']);

        $data['mainpage'] = 'backend/content/content';
        $this->load->view('backend/templates', $data);
    }

    public function main(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Contentmain';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where('post_created_by', $this->sess_id);
        $this->db->where_in('post_is_trash', array('0'));
        $this->db->order_by("post_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_contents');
        //var_dump($this->db->last_query());die;
        $data['post'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 1);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 2);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_all'] = $query_total->num_rows();

        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where('post_created_by', $this->sess_id);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('post_created_by', $this->sess_id);
            }
            $this->db->where('post_status', 3);
            $this->db->where_in('post_is_trash', array('0'));
            $query_total = $this->db->get('cms_contents');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_main']);

        $data['mainpage'] = 'backend/content/content';
        $this->load->view('backend/templates', $data);
    }

    public function draft(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Contentdraft';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where('post_status', 2);
        $this->db->where_in('post_is_trash', array('0'));
        $this->db->order_by("post_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_contents');
        $data['post'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where('post_status', 2);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 1);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('post_created_by', $this->sess_id);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('post_created_by', $this->sess_id);
            }
            $this->db->where('post_status', 3);

            $this->db->where_in('post_is_trash', array('0'));
            $query_total = $this->db->get('cms_contents');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_draft']);

        $data['mainpage'] = 'backend/content/content';
        $this->load->view('backend/templates', $data);
    }

    public function pending(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Contentpending';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 3){
            $this->db->where('post_created_by', $this->sess_id);
        }
        if($title != ""){
            $this->db->like('post_title', $title);
        }
        $this->db->where('post_status', 3);
        $this->db->where_in('post_is_trash', array('0'));
        $this->db->order_by("post_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_contents');
        $data['post'] = $photo_query->result_array();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 2);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_draft'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where('post_status', 1);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total'] = $query_total->num_rows();

        if($this->group_id == 3 || $this->group_id == 4){
            $this->db->where('post_created_by', $this->sess_id);
        }
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where('post_created_by', $this->sess_id);
        $this->db->where_in('post_is_trash', array('0'));
        $query_total = $this->db->get('cms_contents');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($this->group_id == 3){
                $this->db->where('post_created_by', $this->sess_id);
            }
            if($title != ""){
                $this->db->like('post_title', $title);
            }
            $this->db->where('post_status', 3);

            $this->db->where_in('post_is_trash', array('0'));
            $query_total = $this->db->get('cms_contents');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_pending']);

        $data['mainpage'] = 'backend/content/content';
        $this->load->view('backend/templates', $data);
    }

    public function add(){
        $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
        if(count($add_action) < 1)
            show_404();

        if ($this->input->post('submit')) {
            //var_dump($_POST);die;
            // validation
            $valid = $this->form_validation;
            $valid->set_rules('post_title', 'Title', 'required');
            //$valid->set_rules('post_subtitle', 'Subtitle', 'required');
            //$valid->set_rules('post_content', 'Content', 'required');
            if($this->group_id != 3){
                $valid->set_rules('post_status', 'Status', 'required');
            }
            $valid->set_rules('post_publish_date', 'Publish Date', 'required');
            // $valid->set_rules('post_category_id', 'Categories', 'required');
            //$valid->set_rules('post_taxonomy', 'Tags', 'required');

            if ($valid->run() == false) {
                // run
                //var_dump(validation_errors());die;
            } else {
                $format_upload = $this->upload('post_'.time().'_'.$this->input->post('post_title'));
                if(isset($_POST['post_is_thumbnail']) && $_POST['post_is_thumbnail'] == 1){
                    $no_thumbnail = 0;
                } else {
                    $no_thumbnail = 1;
                }
                $data = array(
                    'post_author' => $this->sess_id,
                    'post_title' => $this->input->post('post_title'),
                    'post_slug' => url_title($this->input->post('post_title'), 'dash', true),
                    'post_content' => $this->input->post('post_content'),
                    //'post_is_editor_choice' => $this->input->post('post_is_editor_choice') ? 1 : 0,
                    'post_views' => 0,
                    'post_created_at' => date('Y-m-d H:i:s'),
                    'post_created_by' => $this->sess_id,
                    'post_publish_date' => $this->input->post('post_publish_date') . " " . $this->input->post('jam') . ":" . $this->input->post('menit'),
                    'post_status' => $this->input->post('post_status'),
                    'post_is_trash' => 0
                );

                $post_id = $this->m_content->add($data);

                if($this->input->post('post_taxonomy') != ""){
                    //save tags
                    $arr_tag = explode(',', $this->input->post('post_taxonomy'));
                    foreach($arr_tag as $arr_tags){
                        $name_small             = strtolower($arr_tags);
                        $this->db->select('taxonomy_id, taxonomy_name');
                        $this->db->from('cms_taxonomy');
                        $this->db->where('taxonomy_name', $name_small);
                        $query = $this->db->get();

                        if ($query->num_rows() == 0) {
                            //if not exist, insert cms_taxonomy
                            $this->db->insert('cms_taxonomy', array('taxonomy_name' => $name_small, 'taxonomy_description' => $name_small, 'taxonomy_slug' => url_title($name_small, 'dash', true), 'taxonomy_created_at' => date('Y-m-d H:i:s'), 'taxonomy_created_by' => $this->sess_id, 'taxonomy_is_trash' => 0));
                            $id_tags = $this->db->insert_id();
                            //insert cms_post_taxonomy
                            $this->db->insert('cms_content_taxonomy', array('post_id' => $post_id, 'taxonomy_id' => $id_tags));
                        } else {
                            //if exist, insert cms_post_taxonomy only
                            $id_tags = $query->row()->taxonomy_id;
                            $this->db->insert('cms_content_taxonomy', array('post_id' => $post_id, 'taxonomy_id' => $id_tags));
                        }
                    }
                    //end of tags
                }

                $this->session->set_flashdata('success', 'Content successfully created.');
                redirect('backend/content/content/all');
            }
        }

        $data['base'] = 'Content';
        $data['iniform'] = true;
        $data['section'] = $this->m_page_section->get(2);
        $data['mainpage'] = 'backend/content/add_content';
        $this->load->view('backend/templates', $data);
    }

    public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

        $id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                // $valid->set_rules('post_title', 'Title', 'required');
                //$valid->set_rules('post_subtitle', 'Subtitle', 'required');
                //$valid->set_rules('post_content', 'Content', 'required');
                if($this->group_id != 3){
                    $valid->set_rules('post_status', 'Status', 'required');
                }
                $valid->set_rules('post_publish_date', 'Publish Date', 'required');
                //$valid->set_rules('post_taxonomy', 'Tags', 'required');

                if ($valid->run() == false) {
                    // run
                } else {
                    //delete all tags
                    $this->m_content->delete_post_taxonomy($id);

                    //seleksi
                    $arr_tags = explode(",", $this->input->post('post_taxonomy'));
                    foreach ($arr_tags as $v) {
                        $this->m_content->selection_tags($v, $id);
                    }

                    $format_upload = $this->upload('post_'.time().'_'.$this->input->post('post_title'));
                    if(isset($_POST['post_is_thumbnail']) && $_POST['post_is_thumbnail'] == 1){
                        $no_thumbnail = 0;
                        
                        $update_media = array(
                            'post_thumbnail' => 0
                        );
                        $this->m_content->edit($update_media, $id);
                    } else {
                        $no_thumbnail = 1;
                    }
                    if ($format_upload != "") {
                        $data = array(
                            'post_title' => $this->input->post('post_title'),
                            'post_slug' => url_title($this->input->post('post_title'), 'dash', true),
                            'post_content' => $this->input->post('post_content'),
                            //'post_is_editor_choice' => $this->input->post('post_is_editor_choice') ? 1 : 0,
                            'post_modified_at' => date('Y-m-d H:i:s'),
                            'post_modified_by' => $this->sess_id,
                            'post_publish_date' => $this->input->post('post_publish_date') . " " . $this->input->post('jam') . ":" . $this->input->post('menit'),
                            'post_status' => $this->input->post('post_status'),
                            'post_is_trash' => 0
                        );

                        $this->m_content->edit($data, $id);
                    } else {
                        $data = array(
                            'post_title' => $this->input->post('post_title'),
                            'post_slug' => url_title($this->input->post('post_title'), 'dash', true),
                            'post_content' => $this->input->post('post_content'),
                            //'post_is_editor_choice' => $this->input->post('post_is_editor_choice') ? 1 : 0,
                            'post_modified_at' => date('Y-m-d H:i:s'),
                            'post_modified_by' => $this->sess_id,
                            'post_publish_date' => $this->input->post('post_publish_date') . " " . $this->input->post('jam') . ":" . $this->input->post('menit'),
                            'post_status' => $this->input->post('post_status'),
                            'post_is_trash' => 0
                        );

                        $this->m_content->edit($data, $id);
                    }

                    redirect('backend/content/content/all');
                }
            }

            $data['base'] = 'Group';
            $data['iniform'] = true;

            //tag
            $tags = $this->m_content->get_tags_post($id);
            $data['tags'] = '';
            if($tags){
                $arr = array();
                foreach ($tags as $v) {
                    $arr[] = $this->m_tags->get_name_tag_id($v->taxonomy_id);
                }
                $data['tags'] = implode(",", $arr);
            }
            //end of tags

            $data['section'] = $this->m_page_section->get(2);
            $data['content'] = $this->m_content->get_id_post($id);
            $data['mainpage'] = 'backend/content/edit_content';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/content/content/all');
        }
    }


    public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $data = array('post_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_content->delete($data, $id);
            redirect('backend/content/content/all');
        } else {
            redirect('backend/content/content/all');
        }
    }

    /**
     * Upload images
     * @return string
     */
    private function upload($rename) {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload();

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '100%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '75%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload = $mediaId;
        }

        return $format_upload;
    }

}