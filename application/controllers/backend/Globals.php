<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of Globals
 *
 * @author aris
 */
class Globals extends My_Controller {

	public function index(){
		if(empty($_FILES['file']))
        {
            exit(); 
        }
        $errorImgFile = "./img/img_upload_error.jpg";
        //$destinationFilePath = './img-uploads/'.$_FILES['file']['name'];
        $destinationFilePath = APPPATH . '../asset_admin/assets/uploads/body_upload/'.$_FILES['file']['name'];
        if(!move_uploaded_file($_FILES['file']['tmp_name'], $destinationFilePath)){
            echo $errorImgFile;
        }
        else{
            echo base_url() . 'asset_admin/assets/uploads/body_upload/'.$_FILES['file']['name'];
        }
	}

}