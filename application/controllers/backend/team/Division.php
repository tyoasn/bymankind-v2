<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Division extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_division');
        $this->load->model('m_action');

        $this->access_id = 10;
	}

	public function index() {
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

		$data['base'] = 'Division';

		$limit = 10;

        $this->db->where_in('division_is_trash', 0);
        $this->db->where('division_status', 1);
        $this->db->where('division_type_id', 2);
        $this->db->order_by("division_id", "desc");
        $photo_query = $this->db->get('cms_division');
        $data['division'] = $photo_query->result_array();

        $this->db->where_in('division_is_trash', 0);
        $this->db->where('division_status', 1);
        $this->db->where('division_type_id', 2);
        $query_total = $this->db->get('cms_division');
        $data['total'] = $query_total->num_rows();

        $this->load->library('pagination');
        $config['base_url'] = site_url('backend/team/division');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $config['num_links'] = 2;
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
        $config['prev_tag_close'] = '</span></span></li>';
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li><span aria-hidden="true">';
        $config['next_tag_close'] = '</span></li>';
        $config['last_link'] = '';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';
        $config['first_link'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['num_tag_open'] = '<li><span>';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();

        if ($this->input->post('submit')) {
            $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
            if(count($add_action) < 1)
                show_404();

        	// validation
            $valid = $this->form_validation;
            $valid->set_rules('division_name', 'Name', 'required');
            //$valid->set_rules('page_description', 'Description', 'required');

            if ($valid->run() == false) {
                // run
            } else {
            	$data = array(
            		'division_name' => $this->input->post('division_name'),
            		'division_description' => $this->input->post('division_description'),
                    'division_type_id' => 2,
            		'division_created_at' => date('Y-m-d H:i:s'),
            		'division_created_by' => $this->sess_id,
                    'division_publishdate' => date('Y-m-d H:i:s'),
                    'division_status' => 1,
            		'division_is_trash' => 0
            	);

            	$this->m_division->add($data);

                $this->session->set_flashdata('success', 'Division successfully created.');
                redirect('backend/team/division');
            }
        }

        $data['divisions'] = $this->m_division->get();
		$data['mainpage'] = 'backend/team/division';
		$this->load->view('backend/templates', $data);
	}

	public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

		$id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                $valid->set_rules('division_name', 'Name', 'required');
                //$valid->set_rules('page_description', 'Description', 'required');

                if ($valid->run() == false) {
                    // run
                } else {

                	$data = array(
                        'division_name' => $this->input->post('division_name'),
                        'division_description' => $this->input->post('division_description'),
                        'division_modified_at' => date('Y-m-d H:i:s'),
                        'division_modified_by' => $this->sess_id,
                    );

	            	$this->m_division->edit($data, $id);

                    redirect('backend/team/division');
                }
            }

            $data['base'] = 'Group';
            $data['division'] = $this->m_division->get_id_category($id);
            $data['mainpage'] = 'backend/team/edit_division';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/team/division');
        }
	}

	public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $data = array('division_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_division->delete($data, $id);
            redirect('backend/team/division');
        } else {
            redirect('backend/team/division');
        }
    }

}