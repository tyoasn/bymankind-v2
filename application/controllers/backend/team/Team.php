<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Team extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('m_team');
        $this->load->model('m_division');
        $this->load->model('m_user');
        $this->load->model('m_media');
        $this->load->model('m_action');
        $this->load->helper("custom");

        $this->access_id = 8;
    }

    public function index(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Team';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where('team_status', 1);
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_team');
        //var_dump($this->db->last_query());die;
        $data['team'] = $photo_query->result_array();

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where('team_status', 1);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total'] = $query_total->num_rows();

        $this->db->where('team_status', 2);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

        $this->db->where('team_status', 3);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_pending'] = $query_total->num_rows();

        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total']);

        $data['mainpage'] = 'backend/team/team';
        $this->load->view('backend/templates', $data);
    }

    public function all(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Teamall';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_team');
        //var_dump($this->db->last_query());die;
        $data['team'] = $photo_query->result_array();

        $this->db->where('team_status', 1);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total'] = $query_total->num_rows();

        $this->db->where('team_status', 2);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_draft'] = $query_total->num_rows();

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

            $this->db->where('team_status', 3);
            $this->db->where_in('team_is_trash', array('0'));
            $query_total = $this->db->get('cms_team');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_all']);

        $data['mainpage'] = 'backend/team/team';
        $this->load->view('backend/templates', $data);
    }

    public function main(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Teammain';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_team');
        //var_dump($this->db->last_query());die;
        $data['team'] = $photo_query->result_array();

        $this->db->where('team_status', 1);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total'] = $query_total->num_rows();

        $this->db->where('team_status', 2);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_all'] = $query_total->num_rows();

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

            $this->db->where('team_status', 3);
            $this->db->where_in('team_is_trash', array('0'));
            $query_total = $this->db->get('cms_team');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_main']);

        $data['mainpage'] = 'backend/team/team';
        $this->load->view('backend/templates', $data);
    }

    public function draft(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Teamdraft';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where('team_status', 2);
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_team');
        $data['team'] = $photo_query->result_array();

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where('team_status', 2);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where('team_status', 1);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

            $this->db->where('team_status', 3);

            $this->db->where_in('team_is_trash', array('0'));
            $query_total = $this->db->get('cms_team');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_draft']);

        $data['mainpage'] = 'backend/team/team';
        $this->load->view('backend/templates', $data);
    }

    public function pending(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Teampending';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('team_name', $title);
        }
        $this->db->where('team_status', 3);
        $this->db->where_in('team_is_trash', array('0'));
        $this->db->order_by("team_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_team');
        $data['team'] = $photo_query->result_array();

        $this->db->where('team_status', 2);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where('team_name', 1);
        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('team_is_trash', array('0'));
        $query_total = $this->db->get('cms_team');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($title != ""){
                $this->db->like('team_name', $title);
            }
            $this->db->where('team_status', 3);

            $this->db->where_in('team_is_trash', array('0'));
            $query_total = $this->db->get('cms_team');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_pending']);

        $data['mainpage'] = 'backend/team/team';
        $this->load->view('backend/templates', $data);
    }

    public function add(){
        $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
        if(count($add_action) < 1)
            show_404();

        if ($this->input->post('submit')) {
            //var_dump($_POST);die;
            // validation
            $valid = $this->form_validation;
            $valid->set_rules('team_name', 'Name', 'required');
            //$valid->set_rules('post_subtitle', 'Subtitle', 'required');
            //$valid->set_rules('post_content', 'Content', 'required');
            if($this->group_id != 3){
                $valid->set_rules('team_status', 'Status', 'required');
            }
            $valid->set_rules('team_category_id', 'Division', 'required');
            // $valid->set_rules('post_category_id', 'Categories', 'required');
            //$valid->set_rules('post_taxonomy', 'Tags', 'required');

            if ($valid->run() == false) {
                // run
                //var_dump(validation_errors());die;
            } else {
                $format_upload = $this->upload('team1_'.time().'_'.$this->input->post('team_name'));
                $format_upload2 = $this->upload2('team2_'.time().'_'.$this->input->post('team_name'));
                if(isset($_POST['team_is_thumbnail']) && $_POST['team_is_thumbnail'] == 1){
                    $no_thumbnail = 0;
                } else {
                    $no_thumbnail = 1;
                }
                $data = array(
                    'team_name' => $this->input->post('team_name'),
                    'team_role' => $this->input->post('team_role'),
                    'team_category_id' => $this->input->post('team_category_id'),
                    'team_thumbnail' => $format_upload,
                    'team_thumbnail_2' => $format_upload2,
                    'team_is_thumbnail' => $no_thumbnail,
                    'team_status' => $this->input->post('team_status'),
                    'team_is_trash' => 0
                );

                $team_id = $this->m_team->add($data);

                $this->session->set_flashdata('success', 'Member successfully added.');
                redirect('backend/team/team/all');
            }
        }

        $data['base'] = 'Team';
        $data['division'] = $this->m_division->get(2);
        $data['iniform'] = true;
        $data['mainpage'] = 'backend/team/add_team';
        $this->load->view('backend/templates', $data);
    }

    public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

        $id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                // $valid->set_rules('post_title', 'Title', 'required');
                //$valid->set_rules('post_subtitle', 'Subtitle', 'required');
                //$valid->set_rules('post_content', 'Content', 'required');
                if($this->group_id != 3){
                    $valid->set_rules('team_status', 'Status', 'required');
                }
                $valid->set_rules('team_category_id', 'Division', 'required');
                //$valid->set_rules('post_taxonomy', 'Tags', 'required');

                if ($valid->run() == false) {
                    // run
                } else {
                    $format_upload = $this->upload('team1_'.time().'_'.$this->input->post('team_name'));
                    $format_upload2 = $this->upload2('team2_'.time().'_'.$this->input->post('team_name'));
                    if(isset($_POST['post_is_thumbnail']) && $_POST['post_is_thumbnail'] == 1){
                        $no_thumbnail = 0;
                        
                        $update_media = array(
                            'post_thumbnail' => 0
                        );
                        $this->m_post->edit($update_media, $id);
                    } else {
                        $no_thumbnail = 1;
                    }


                    if ($format_upload != "") {
                        $data = array(
                            'team_name' => $this->input->post('team_name'),
                            'team_role' => $this->input->post('team_role'),
                            'team_category_id' => $this->input->post('team_category_id'),
                            'team_thumbnail' => $format_upload,
                            'team_thumbnail_2' => $format_upload2,
                            'team_is_thumbnail' => $no_thumbnail,
                            'team_status' => $this->input->post('team_status'),
                            'team_is_trash' => 0
                                );

                        $this->m_team->edit($data, $id);
                    } else {
                        $data = array(
                            'team_name' => $this->input->post('team_name'),
                            'team_role' => $this->input->post('team_role'),
                            'team_category_id' => $this->input->post('team_category_id'),
                            'team_thumbnail' => $format_upload,
                            'team_thumbnail_2' => $format_upload2,
                            'team_is_thumbnail' => $no_thumbnail,
                            'team_status' => $this->input->post('team_status'),
                            'team_is_trash' => 0
                        );

                        $this->m_team->edit($data, $id);
                    }

                    redirect('backend/team/team/all');
                }
            }

            $data['base'] = 'Group';
            $data['iniform'] = true;
            $data['division'] = $this->m_division->get(2);
            $data['team'] = $this->m_team->get_id_post($id);
            $data['mainpage'] = 'backend/team/edit_team';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/team/team/all');
        }
    }


    public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $data = array('team_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_team->delete($data, $id);
            redirect('backend/team/team/all');
        } else {
            redirect('backend/teamr/team/all');
        }
    }

    /**
     * Upload images
     * @return string
     */
    private function upload($rename) {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload();

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '100%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '75%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload = $mediaId;
        }

        return $format_upload;
    }

    private function upload2($rename) {
        $this->load->library('image_lib');
        $format_upload2 = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile2']['name']) && $_FILES['userfile2']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '300', 'height' => '200', 'type' => 'small'),
                array('width' => '600', 'height' => '400', 'type' => 'medium'),
                array('width' => '920', 'height' => '400', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '1080', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile2');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '100%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '75%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    // 'x_axis' => '0',
                    // 'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload2 = $mediaId;
        }

        return $format_upload2;
    }

   

}