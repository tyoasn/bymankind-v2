<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Media extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('m_action');
        $this->load->model('m_post');
        $this->load->model('m_media');
        $this->load->helper("custom");

        $this->access_id = 6;
    }

    public function index(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($this->group_id == 4){
            if($title != ""){
                $this->db->like('media_name', $title);
            }
            $this->db->where('media_created_by', $this->sess_id);
            $this->db->limit($limit, $offset);
            $query = $this->db->get('cms_media');

            $data['media'] = $query->result_array();

            if($title != ""){
                $this->db->like('media_name', $title);
            }
            $this->db->where('media_created_by', $this->sess_id);
            $query = $this->db->get('cms_media');

            $data['total'] = $query->num_rows();
            //var_dump($data['media']);die;
        } else {
            if($title != ""){
                $this->db->like('media_name', $title);
            }
            $this->db->limit($limit, $offset);
            $this->db->order_by('media_id', 'desc');
            $query = $this->db->get('cms_media');
            $data['media'] = $query->result_array();

            if($title != ""){
                $this->db->like('media_name', $title);
            }
            $query = $this->db->get('cms_media');
            $data['total'] = $query->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total']);

        $data['mainpage'] = 'backend/media/media';
        $this->load->view('backend/templates', $data);
    }

    public function add(){
        $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
        if(count($add_action) < 1)
            show_404();

        if ($this->input->post('submit')) {
            $format_upload = $this->upload();
            if($format_upload != ""){
                $path = pathinfo($format_upload);
                if($path['extension'] == 'jpg' || $path['extension'] == 'JPG' || $path['extension'] == 'jpeg' || $path['extension'] == 'JPEG' || $path['extension'] == 'png'){
                    $type = 'image';
                } elseif ($path['extension'] == 'pdf') {
                    $type = 'pdf';
                } elseif ($path['extension'] == 'doc' || $path['extension'] == 'docx') {
                    $type = 'doc';
                } elseif ($path['extension'] == 'xls' || $path['extension'] == 'xlsx') {
                    $type = 'xls';
                } elseif ($path['extension'] == 'ppt' || $path['extension'] == 'pptx') {
                    $type = 'ppt';
                } elseif ($path['extension'] == 'rar' || $path['extension'] == 'zip') {
                    $type = 'rar';
                } elseif ($path['extension'] == 'mp3' || $path['extension'] == 'mp4') {
                    $type = 'audio';
                }

                $data = array(
                    'media_name' => $format_upload,
                    'media_type' => $type,
                    'media_directory' => base_url().'asset_admin/assets/uploads/media/'.$type.'/',
                    'media_created_at' => date('Y-m-d H:i:s'),
                    'media_created_by' => $this->sess_id
                );

                $this->m_media->add($data);

                $this->session->set_flashdata('success', 'Media successfully created.');
                redirect('backend/media/media');
            }
        }

        $data['base'] = 'Media';
        $data['mainpage'] = 'backend/media/add_media';
        $this->load->view('backend/templates', $data);
    }

    private function upload(){
        $format = '';
        $rename = 'media_'.time() . '_' . $_FILES['userfile']['name'];
        if ($_FILES['userfile']['name'] == "") {
            $config['file_name'] = "";
        } else {
            $config['file_name'] = $rename;
        }

        $path = pathinfo($_FILES['userfile']['name']);
        if($path['extension'] == 'jpg' || $path['extension'] == 'JPG' || $path['extension'] == 'jpeg' || $path['extension'] == 'JPEG' || $path['extension'] == 'png' || $path['extension'] == 'gif'){
            $directory = 'image';
        } elseif ($path['extension'] == 'pdf') {
            $directory = 'pdf';
        } elseif ($path['extension'] == 'doc' || $path['extension'] == 'docx') {
            $directory = 'doc';
        } elseif ($path['extension'] == 'xls' || $path['extension'] == 'xlsx') {
            $directory = 'xls';
        } elseif ($path['extension'] == 'ppt' || $path['extension'] == 'pptx') {
            $directory = 'ppt';
        } elseif ($path['extension'] == 'rar' || $path['extension'] == 'zip') {
            $directory = 'rar';
        } elseif ($path['extension'] == 'mp3' || $path['extension'] == 'mp4') {
            $directory = 'audio';
        }

        $config['upload_path'] = 'asset_admin/assets/uploads/media/'.$directory.'/';
        $config['allowed_types'] = 'jpg|jpeg|png|pdf|doc|docx|xls|xlsx|ppt|pptx|rar|zip|mp3|mp4';
        $config['max_size']      = 300048;
        $this->load->library('upload', $config);
        if($this->upload->do_upload('userfile')){
            $uploadData = $this->upload->data();

            $thumbpath = pathinfo($uploadData['file_name']);
            $img1 = $thumbpath['filename'].'_large.'.$thumbpath['extension'];
            $img2 = $thumbpath['filename'].'_small.'.$thumbpath['extension'];
            copy($uploadData['full_path'], $uploadData['file_path'].$img1);
            copy($uploadData['full_path'], $uploadData['file_path'].$img2);

            $format = $uploadData['file_name'];

            return $format;
        } else {
            $this->session->set_flashdata('error', $this->upload->display_errors());
            redirect('backend/media/media');
            die;
        }
    }

    public function mediauploadPage(){
        $this->load->library('image_lib');
        $format_upload = '';
        $thumbpath = pathinfo($_FILES['upload']['name']);
        $rename = 'media_'.time().'_'.$thumbpath['filename'];
        //$rename = str_replace('-','_',url_title('media-'.time().'-'.$_FILES['upload']['name'],'dash',true));
        $pageid = $this->input->get('id');
        $mediapageid = $this->session->userdata('mediapageid');
        $data['callback'] = $this->input->get('CKEditorFuncNum');

        if (isset($_FILES['upload']['name']) && $_FILES['upload']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '150', 'height' => '150', 'type' => 'small'),
                array('width' => '371', 'height' => '204', 'type' => 'medium'),
                array('width' => '970', 'height' => '455', 'type' => 'large'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size' => '20048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('upload');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['width']
                );

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            $format_upload = $rename . $image_data['file_ext'];
            copy($ori_path.$format_upload,$base_path.$format_upload);

            if($format_upload != ""){
                if ($pageid == 0 && !$mediapageid){
                    //insert for draft
                    // $insert = array(
                    //     'page_parent_id' => 0,
                    //     'page_type_id' => 1,
                    //     'language_id' => $this->session->userdata('language_id'),
                    //     'page_status' => 2,
                    //     'page_is_trash' => 0,
                    //     'page_created_at' => date('Y-m-d H:i:s'),
                    //     'page_created_by' => $this->sess_id,
                    //     'page_publishdate' => date('Y-m-d H:i:s')
                    // );

                    // $pageid = $this->m_page->add($insert);

                    // $this->session->set_userdata('mediapageid', $pageid);
                    
                } elseif ($pageid == 0 && $mediapageid){
                    //$pageid = $mediapageid;
                }

                $insert_media = array(
                    'media_name' => $format_upload,
                    'media_type' => 'image',
                    'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
                );

                $mediaId = $this->m_media->add($insert_media);

                // $insert_page_media = array(
                //     'page_id' => $pageid,
                //     'media_id' => $mediaId
                // );

                // $this->m_media->add_page_media($insert_page_media);

                $data['pageid'] = 0;
                $data['url'] = base_url().'asset_admin/assets/uploads/media/image/'.$format_upload;
            }
        }

        // return $format_upload;
        $this->load->view('backend/media/upload_media_page', $data);
    }

    public function mediaupload(){
        $this->load->library('image_lib');
        $format_upload = '';
        $thumbpath = pathinfo($_FILES['upload']['name']);
        $rename = 'media_'.time().'_'.$thumbpath['filename'];
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        $postid = $this->input->get('id');
        $mediapostid = $this->session->userdata('mediapostid');
        $data['callback'] = $this->input->get('CKEditorFuncNum');

        if (isset($_FILES['upload']['name']) && $_FILES['upload']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '150', 'height' => '150', 'type' => 'small'),
                array('width' => '371', 'height' => '204', 'type' => 'medium'),
                array('width' => '970', 'height' => '455', 'type' => 'large'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size' => '20048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('upload');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['width']
                );

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            $format_upload = $rename . $image_data['file_ext'];
            copy($ori_path.$format_upload,$base_path.$format_upload);

            if($format_upload != ""){
                if ($postid == 0 && !$mediapostid){
                    //insert for draft
                    // $insert = array(
                    //     'post_author' => $this->sess_id,
                    //     'post_category_id' => 177,
                    //     'language_id' => $this->session->userdata('language_id'),
                    //     'post_views' => 0,
                    //     'post_status' => 2,
                    //     'post_is_trash' => 0,
                    //     'post_created_at' => date('Y-m-d H:i:s'),
                    //     'post_created_by' => $this->sess_id,
                    //     'post_publish_date' => date('Y-m-d H:i:s')
                    // );

                    // $postid = $this->m_post->add($insert);

                    // $this->session->set_userdata('mediapostid', $postid);
                    
                } elseif ($postid == 0 && $mediapostid){
                    //$postid = $mediapostid;
                }

                $insert_media = array(
                    'media_name' => $format_upload,
                    'media_type' => 'image',
                    'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
                );

                $mediaId = $this->m_media->add($insert_media);

                // $insert_post_media = array(
                //     'post_id' => $postid,
                //     'media_id' => $mediaId
                // );

                // $this->m_media->add_post_media($insert_post_media);

                $data['postid'] = 0;
                $data['url'] = base_url().'asset_admin/assets/uploads/media/image/'.$format_upload;
            }
        }

        // return $format_upload;
        $this->load->view('backend/media/upload_media', $data);
    }

    public function mediabrowsePage(){
        $data['callback'] = $this->input->get('CKEditorFuncNum');
        $data['pageid'] = $this->input->get('id');
        $data['show'] = $this->input->get('show');
        $mediapageid = $this->session->userdata('mediapageid');

        $data['allcount'] = $this->m_media->count('image');
        $data['currentcount'] = $this->m_media->count_current_page($data['pageid']);

        //$offset = $this->uri->segment(5);
        $limit = 20;
        $page = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        $offset = ($page-1)*$limit;

        if($data['pageid'] == 0 && $mediapageid){
            $data['pageid'] = $mediapageid;
        }

        if($data['show'] == 'all'){
            $count = $this->m_media->count('image');
            $data['paging'] = paginate_function($limit,$page,$count);

            $data['media'] = $this->m_media->get('image',$limit,$offset);

            // $data['media'] = $this->m_post_media->get($limit,$offset);
            // $count = $this->m_post_media->count();

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        } else {
            $count = $this->m_media->count_current_page($data['pageid']);
            $data['paging'] = paginate_function($limit,$page,$count);
            $data['media'] = $this->m_media->get_current_page($data['pageid'],'image',$limit,$offset);

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        }

        $this->load->view('backend/media/browse_media_page', $data);
    }

    public function mediabrowseContent(){
        $data['callback'] = $this->input->get('CKEditorFuncNum');
        $data['contentid'] = $this->input->get('id');
        $data['show'] = $this->input->get('show');
        $mediapageid = $this->session->userdata('mediapageid');

        $data['allcount'] = $this->m_media->count('image');
        $data['currentcount'] = $this->m_media->count_current_page($data['contentid']);

        //$offset = $this->uri->segment(5);
        $limit = 20;
        $page = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        $offset = ($page-1)*$limit;

        if($data['contentid'] == 0 && $mediapageid){
            $data['contentid'] = $mediapageid;
        }

        if($data['show'] == 'all'){
            $count = $this->m_media->count('image');
            $data['paging'] = paginate_function($limit,$page,$count);

            $data['media'] = $this->m_media->get('image',$limit,$offset);

            // $data['media'] = $this->m_post_media->get($limit,$offset);
            // $count = $this->m_post_media->count();

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        } else {
            $count = $this->m_media->count_current_page($data['contentid']);
            $data['paging'] = paginate_function($limit,$page,$count);
            $data['media'] = $this->m_media->get_current_page($data['contentid'],'image',$limit,$offset);

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        }

        $this->load->view('backend/media/browse_media_content', $data);
    }

     public function mediabrowseCareer(){
        $data['callback'] = $this->input->get('CKEditorFuncNum');
        $data['careerid'] = $this->input->get('id');
        $data['show'] = $this->input->get('show');
        $mediapageid = $this->session->userdata('mediapageid');

        $data['allcount'] = $this->m_media->count('image');
        $data['currentcount'] = $this->m_media->count_current_page($data['careerid']);

        //$offset = $this->uri->segment(5);
        $limit = 20;
        $page = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        $offset = ($page-1)*$limit;

        if($data['careerid'] == 0 && $mediapageid){
            $data['careerid'] = $mediapageid;
        }

        if($data['show'] == 'all'){
            $count = $this->m_media->count('image');
            $data['paging'] = paginate_function($limit,$page,$count);

            $data['media'] = $this->m_media->get('image',$limit,$offset);

            // $data['media'] = $this->m_post_media->get($limit,$offset);
            // $count = $this->m_post_media->count();

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        } else {
            $count = $this->m_media->count_current_page($data['careerid']);
            $data['paging'] = paginate_function($limit,$page,$count);
            $data['media'] = $this->m_media->get_current_page($data['careerid'],'image',$limit,$offset);

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        }

        $this->load->view('backend/media/browse_media_career', $data);
    }

    public function mediabrowse(){
        $data['callback'] = $this->input->get('CKEditorFuncNum');
        $data['postid'] = $this->input->get('id');
        $data['show'] = $this->input->get('show');
        $mediapostid = $this->session->userdata('mediapostid');

        $data['allcount'] = $this->m_media->count('image');
        $data['currentcount'] = $this->m_media->count_current($data['postid']);

        //$offset = $this->uri->segment(5);
        $limit = 20;
        $page = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        $offset = ($page-1)*$limit;

        if($data['postid'] == 0 && $mediapostid){
            $data['postid'] = $mediapostid;
        }

        if($data['show'] == 'all'){
            $count = $this->m_media->count('image');
            $data['paging'] = paginate_function($limit,$page,$count);

            $data['media'] = $this->m_media->get('image',$limit,$offset);

            // $data['media'] = $this->m_post_media->get($limit,$offset);
            // $count = $this->m_post_media->count();

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        } else {
            $count = $this->m_media->count_current($data['postid']);
            $data['paging'] = paginate_function($limit,$page,$count);
            $data['media'] = $this->m_media->get_current($data['postid'],'image',$limit,$offset);

            // $this->load->library('pagination');
      //       $config['base_url'] = site_url('backend/post/media/mediabrowse');
      //       $config['total_rows'] = $count;
      //       $config['per_page'] = $limit;
      //       $config['uri_segment'] = 5;
      //       $config['num_links'] = 2;
      //       $config['prev_link'] = '&laquo;';
      //       $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
      //       $config['prev_tag_close'] = '</span></span></li>';
      //       $config['next_link'] = '»';
      //       $config['next_tag_open'] = '<li><span aria-hidden="true">';
      //       $config['next_tag_close'] = '</span></li>';
      //       $config['last_link'] = '';
      //       $config['last_tag_open'] = '';
      //       $config['last_tag_close'] = '';
      //       $config['first_link'] = '';
      //       $config['first_tag_open'] = '';
      //       $config['first_tag_close'] = '';
      //       $config['num_tag_open'] = '<li><span>';
      //       $config['num_tag_close'] = '</span></li>';
      //       $config['cur_tag_open'] = '<li class="active"><span>';
      //       $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
      //       $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
      //       $config['full_tag_close'] = '</ul>';

      //       $this->pagination->initialize($config);
      //       $data['page_links'] = $this->pagination->create_links();
        }

        $this->load->view('backend/media/browse_media', $data);
    }

    public function mediadelete($mediaId){
        $this->m_media->delete($mediaId);

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function mediacaption($mediaId){
        $edit = array(
            'caption' => $this->input->post('caption')
        );

        $this->m_media->edit($edit,$mediaId);
    }

    public function regenerate(){

    }

}