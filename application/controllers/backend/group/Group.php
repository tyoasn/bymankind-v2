<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Group extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_group');
        /* saya komen karna sudah di load di My_Controller */
		//$this->load->model('m_access');
		$this->load->model('m_action');

        $this->access_id = 102;
	}

	public function index() {
        $seacr_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($seacr_action) < 1)
            show_404();

		$data['base'] = 'Group';

		$limit = 10;

        $this->db->where_in('group_status', array('1'));
        $this->db->where_in('group_is_trash', array('0'));
        $this->db->order_by("group_id", "asc");
        $this->db->limit($limit, $this->uri->segment(4));
        $photo_query = $this->db->get('cms_groups');
        $data['groups'] = $photo_query->result_array();

        $this->db->where_in('group_status', array('1'));
        $this->db->where_in('group_is_trash', array('0'));
        $query_total = $this->db->get('cms_groups');
        $data['total'] = $query_total->num_rows();

        $this->load->library('pagination');
        $config['base_url'] = site_url('backend/group/group');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $config['num_links'] = 2;
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
        $config['prev_tag_close'] = '</span></span></li>';
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li><span aria-hidden="true">';
        $config['next_tag_close'] = '</span></li>';
        $config['last_link'] = '';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';
        $config['first_link'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['num_tag_open'] = '<li><span>';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();

		$data['mainpage'] = 'backend/group/group';
		$this->load->view('backend/templates', $data);
    }

    public function add(){
        $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
        if(count($add_action) < 1)
            show_404();

    	if ($this->input->post('submit')) {
    		// validation
            $valid = $this->form_validation;
            $valid->set_rules('group_name', 'Name', 'required');
            $valid->set_rules('group_description', 'Name', 'required');
            $valid->set_rules('group_status', 'Name', 'required');

            if ($valid->run() == false) {
                // run
            } else {
            	$data = array(
            		'group_name' => $this->input->post('group_name'),
            		'group_description' => $this->input->post('group_description'),
            		'group_created_at' => date('Y-m-d H:i:s'),
            		'group_created_by' => $this->sess_id,
            		'group_status' => $this->input->post('group_status'),
            		'group_is_trash' => 0
            	);

            	$this->m_group->add($data);

                $this->session->set_flashdata('success', 'Group successfully created.');
            	redirect('backend/group/group');
            }
    	}

    	$data['base'] = 'Group';
    	$data['mainpage'] = 'backend/group/add_group';
		$this->load->view('backend/templates', $data);
    }

    public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

    	$id = $this->uri->segment(5);
    	if ($id) {
    		if ($this->input->post('submit')) {
    			// validation
	            $valid = $this->form_validation;
	            $valid->set_rules('group_name', 'Name', 'required');
	            $valid->set_rules('group_description', 'Name', 'required');
	            $valid->set_rules('group_status', 'Name', 'required');

	            if ($valid->run() == false) {
	                // run
	            } else {
	            	$data = array(
	            		'group_name' => $this->input->post('group_name'),
	            		'group_description' => $this->input->post('group_description'),
	            		'group_modified_at' => date('Y-m-d H:i:s'),
	            		'group_modified_by' => $this->sess_id,
	            		'group_status' => $this->input->post('group_status')
	            	);

	            	$this->m_group->edit($data, $id);
	            	redirect('backend/group/group');
	            }
    		}

    		$data['base'] = 'Group';
    		$data['group'] = $this->m_group->get_id_group($id);
	    	$data['mainpage'] = 'backend/group/edit_group';
			$this->load->view('backend/templates', $data);
    	} else {
    		redirect('backend/group/group');
    	}
    }

    public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

    	if ($this->uri->segment(5)) {
            $data = array('group_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_group->delete($data, $id);
            redirect('backend/group/group');
        } else {
            redirect('backend/group/group');
        }
    }

    public function access(){
    	$id = $this->uri->segment(5);
    	if($id){
    		$data['base'] = 'Group';

    		$data['group_id'] = $id;
    		$data['access'] = $this->m_access->get();
    		$data['action'] = $this->m_action->get();

	    	$data['mainpage'] = 'backend/group/access_group';
			$this->load->view('backend/templates', $data);
    	} else {
    		redirect('backend/group/group');
    	}
    }

    public function access_post(){
        if ($this->input->post('submit')) {
            $group_id = $this->input->post('group_id');

            $delete_access_group_action = $this->m_group->delete_access_group_action($group_id);

            foreach($_POST['acc_name'] as $key => $value){
                foreach($value as $data){
                    $insert = array(
                        'access_id' => $key,
                        'group_id' => $group_id,
                        'action_id' => $data
                    );

                    $this->m_group->add_access_group_action($insert);
                }
            }

            redirect('backend/group/group');
        }
    }

}