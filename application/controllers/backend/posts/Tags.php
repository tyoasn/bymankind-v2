<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tags extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_tags');
        $this->load->model('m_action');

        $this->access_id = 5;
	}

	public function index() {
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

		$data['base'] = 'Tags';

		$limit = 10;

        $this->db->where_in('taxonomy_is_trash', array('0'));
        $this->db->order_by("taxonomy_id", "desc");
        $photo_query = $this->db->get('cms_taxonomy');
        $data['tags'] = $photo_query->result_array();

        $this->db->where_in('taxonomy_is_trash', array('0'));
        $query_total = $this->db->get('cms_taxonomy');
        $data['total'] = $query_total->num_rows();

        if ($this->input->post('submit')) {
            $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
            if(count($add_action) < 1)
                show_404();

        	// validation
            $valid = $this->form_validation;
            $valid->set_rules('taxonomy_name', 'Name', 'required');
            //$valid->set_rules('taxonomy_description', 'Description', 'required');

            if ($valid->run() == false) {
                // run
            } else {
            	$data = array(
            		'taxonomy_name' => $this->input->post('taxonomy_name'),
            		'taxonomy_description' => $this->input->post('taxonomy_description'),
            		'taxonomy_slug' => url_title($this->input->post('taxonomy_name'), 'dash', true),
            		'taxonomy_created_at' => date('Y-m-d H:i:s'),
            		'taxonomy_created_by' => $this->sess_id,
            		'taxonomy_is_trash' => 0
            	);

            	$this->m_tags->add($data);

                $this->session->set_flashdata('success', 'Tag successfully created.');
                redirect('backend/posts/tags');
            }
        }

		$data['mainpage'] = 'backend/post/tags';
		$this->load->view('backend/templates', $data);
	}

	public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

		$id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                $valid->set_rules('taxonomy_name', 'Name', 'required');
            	//$valid->set_rules('taxonomy_description', 'Description', 'required');

                if ($valid->run() == false) {
                    // run
                } else {

                	$data = array(
	            		'taxonomy_name' => $this->input->post('taxonomy_name'),
	            		'taxonomy_description' => $this->input->post('taxonomy_description'),
	            		'taxonomy_slug' => url_title($this->input->post('taxonomy_name'), 'dash', true),
	            		'taxonomy_modified_at' => date('Y-m-d H:i:s'),
	            		'taxonomy_modified_by' => $this->sess_id,
	            		'taxonomy_is_trash' => 0
	            	);

	            	$this->m_tags->edit($data, $id);

                    redirect('backend/posts/tags');
                }
            }

            $data['base'] = 'Group';
            $data['tag'] = $this->m_tags->get_id_tag($id);
            $data['mainpage'] = 'backend/post/edit_tags';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/posts/tags');
        }
	}

	public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $id = $this->uri->segment(5);
            $this->m_tags->delete($id);
            redirect('backend/posts/tags');
        } else {
            redirect('backend/posts/tags');
        }
    }

    public function get_name_tag(){
        $term = $_GET['term'];
        $data['tags'] = $this->m_tags->get_name_tag($term);
        $data['content'] = $this->load->view('backend/post/get_name_tags', $data);
    }

}