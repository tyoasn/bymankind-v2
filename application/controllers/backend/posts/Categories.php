<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_category');
        $this->load->model('m_action');

        $this->access_id = 4;
	}

	public function index() {
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

		$data['base'] = 'Category';

		$limit = 10;

        $this->db->where_in('page_is_trash', 0);
        $this->db->where('page_status', 1);
        $this->db->where('page_type_id', 2);
        $this->db->order_by("page_id", "desc");
        $photo_query = $this->db->get('cms_pages');
        $data['category'] = $photo_query->result_array();

        $this->db->where_in('page_is_trash', 0);
        $this->db->where('page_status', 1);
        $this->db->where('page_type_id', 2);
        $query_total = $this->db->get('cms_pages');
        $data['total'] = $query_total->num_rows();

        $this->load->library('pagination');
        $config['base_url'] = site_url('backend/post/category');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $config['num_links'] = 2;
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
        $config['prev_tag_close'] = '</span></span></li>';
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li><span aria-hidden="true">';
        $config['next_tag_close'] = '</span></li>';
        $config['last_link'] = '';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';
        $config['first_link'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['num_tag_open'] = '<li><span>';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();

        if ($this->input->post('submit')) {
            $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
            if(count($add_action) < 1)
                show_404();

        	// validation
            $valid = $this->form_validation;
            $valid->set_rules('page_name', 'Name', 'required');
            $valid->set_rules('page_parent_id', 'Parent', 'required');
            //$valid->set_rules('page_description', 'Description', 'required');

            if ($valid->run() == false) {
                // run
            } else {
            	$data = array(
            		'page_name' => $this->input->post('page_name'),
                    'page_slug' => url_title($this->input->post('page_name'), 'dash', true),
            		'page_description' => $this->input->post('page_description'),
                    'page_parent_id' => $this->input->post('page_parent_id'),
                    'page_type_id' => 2,
            		'page_created_at' => date('Y-m-d H:i:s'),
            		'page_created_by' => $this->sess_id,
                    'page_publishdate' => date('Y-m-d H:i:s'),
                    'page_status' => 1,
            		'page_is_trash' => 0
            	);

            	$this->m_category->add($data);

                $this->session->set_flashdata('success', 'Category successfully created.');
                redirect('backend/posts/categories');
            }
        }

        $data['categories'] = $this->m_category->get();
		$data['mainpage'] = 'backend/post/category';
		$this->load->view('backend/templates', $data);
	}

	public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

		$id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                $valid->set_rules('page_name', 'Name', 'required');
                $valid->set_rules('page_parent_id', 'Parent', 'required');
                //$valid->set_rules('page_description', 'Description', 'required');

                if ($valid->run() == false) {
                    // run
                } else {

                	$data = array(
                        'page_name' => $this->input->post('page_name'),
                        'page_slug' => url_title($this->input->post('page_name'), 'dash', true),
                        'page_description' => $this->input->post('page_description'),
                        'page_parent_id' => $this->input->post('page_parent_id'),
                        'page_modified_at' => date('Y-m-d H:i:s'),
                        'page_modified_by' => $this->sess_id,
                    );

	            	$this->m_category->edit($data, $id);

                    redirect('backend/posts/categories');
                }
            }

            $data['base'] = 'Group';
            $data['category'] = $this->m_category->get_id_category($id);
            $data['category_parent'] = $this->m_category->get_parent($id);
            $data['mainpage'] = 'backend/post/edit_category';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/posts/categories');
        }
	}

	public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $data = array('page_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_category->delete($data, $id);
            redirect('backend/posts/categories');
        } else {
            redirect('backend/posts/categories');
        }
    }

}