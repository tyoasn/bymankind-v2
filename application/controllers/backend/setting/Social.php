<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Social extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_setting');

        $this->access_id = 104;
	}

    public function reset(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $this->db->where('config_id', 10);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 11);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 12);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 13);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 16);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->session->set_flashdata('success', 'Social Media successfully reseted.');
        redirect('backend/setting/social');
    }

	public function index(){
		$search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Social';

        if ($this->input->post('submit')) {

    		$this->db->where('config_id', 10);
    		$this->db->update('cms_config', array('config_value' => $this->input->post('footer_facebook')));

    		$this->db->where('config_id', 11);
    		$this->db->update('cms_config', array('config_value' => $this->input->post('footer_twitter')));

    		$this->db->where('config_id', 12);
    		$this->db->update('cms_config', array('config_value' => $this->input->post('footer_youtube')));

    		$this->db->where('config_id', 13);
    		$this->db->update('cms_config', array('config_value' => $this->input->post('footer_instagram')));

            $this->db->where('config_id', 16);
            $this->db->update('cms_config', array('config_value' => $this->input->post('footer_linkedin')));

            $this->session->set_flashdata('success', 'Social Media successfully edited.');
            redirect('backend/setting/social');
        }

        $this->db->where('config_id BETWEEN 10 and 16');
        $this->db->order_by("config_id", "asc");
        $photo_query = $this->db->get('cms_config');
        $data['footer'] = $photo_query->result_array();

        $data['mainpage'] = 'backend/setting/social';
		$this->load->view('backend/templates', $data);
	}

}