<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class General extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_setting');
        $this->load->model('m_media');

        $this->access_id = 103;
	}

    public function reset(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $this->db->where('config_id', 1);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 2);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 3);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 4);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->db->where('config_id', 15);
        $this->db->update('cms_config', array('config_value' => ''));

        $this->session->set_flashdata('success', 'General successfully reseted.');
        redirect('backend/setting/general');
    }

	public function index(){
		$search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'General';

        if ($this->input->post('submit')) {
            $format_upload = $this->upload('logo_header_'.time());
            $format_upload2 = $this->upload2('logo_footer_'.time());
        	$format_upload3 = $this->upload3('favicon_'.time());
        	if ($format_upload != "" && $format_upload2 != "" && $format_upload3 != "") {

        		$this->db->where('config_id', 1);
        		$this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

        		$this->db->where('config_id', 2);
        		$this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

        		$this->db->where('config_id', 3);
        		$this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

        		$this->db->where('config_id', 4);
        		$this->db->update('cms_config', array('config_value' => $format_upload));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));


                $this->db->where('config_id', 15);
                $this->db->update('cms_config', array('config_value' => $format_upload3));

            } elseif ($format_upload != "" && $format_upload2 == "" && $format_upload3 == "") {

                $this->db->where('config_id', 1);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

                $this->db->where('config_id', 2);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

                $this->db->where('config_id', 3);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));

                $this->db->where('config_id', 4);
                $this->db->update('cms_config', array('config_value' => $format_upload));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

            } elseif ($format_upload == "" && $format_upload2 != "" && $format_upload3 == "") {

                $this->db->where('config_id', 1);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

                $this->db->where('config_id', 2);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

                $this->db->where('config_id', 3);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));

                $this->db->where('config_id', 15);
                $this->db->update('cms_config', array('config_value' => $format_upload3));
                
            } elseif ($format_upload == "" && $format_upload2 == "" && $format_upload3 != "") {

                $this->db->where('config_id', 1);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

                $this->db->where('config_id', 2);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

                $this->db->where('config_id', 3);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));

                $this->db->where('config_id', 15);
                $this->db->update('cms_config', array('config_value' => $format_upload3));

            } elseif ($format_upload != "" && $format_upload2 != "" && $format_upload3 == "") {

                $this->db->where('config_id', 1);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

                $this->db->where('config_id', 2);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

                $this->db->where('config_id', 3);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

                $this->db->where('config_id', 4);
                $this->db->update('cms_config', array('config_value' => $format_upload));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));

                $this->db->where('config_id', 15);
                $this->db->update('cms_config', array('config_value' => $format_upload3));

            } elseif ($format_upload == "" && $format_upload2 != "" && $format_upload3 != "") {

                $this->db->where('config_id', 1);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

                $this->db->where('config_id', 2);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

                $this->db->where('config_id', 3);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));

                $this->db->where('config_id', 15);
                $this->db->update('cms_config', array('config_value' => $format_upload3));

            } elseif ($format_upload != "" && $format_upload2 == "" && $format_upload3 != "") {

                $this->db->where('config_id', 1);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

                $this->db->where('config_id', 2);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

                $this->db->where('config_id', 3);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));

                $this->db->where('config_id', 4);
                $this->db->update('cms_config', array('config_value' => $format_upload));

                $this->db->where('config_id', 15);
                $this->db->update('cms_config', array('config_value' => $format_upload3));

        	} else {

        		$this->db->where('config_id', 1);
        		$this->db->update('cms_config', array('config_value' => $this->input->post('general_site_title')));

        		$this->db->where('config_id', 2);
        		$this->db->update('cms_config', array('config_value' => $this->input->post('general_keywords')));

        		$this->db->where('config_id', 3);
        		$this->db->update('cms_config', array('config_value' => $this->input->post('general_description')));

                $this->db->where('config_id', 5);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_address')));

                $this->db->where('config_id', 6);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_phone')));

                $this->db->where('config_id', 8);
                $this->db->update('cms_config', array('config_value' => $this->input->post('general_email')));
        	}

            $this->session->set_flashdata('success', 'General successfully edited.');
        	redirect('backend/setting/general');
        }

        $this->db->where('config_id BETWEEN 1 and 16');
        $this->db->order_by("config_id", "asc");
        $photo_query = $this->db->get('cms_config');
        $data['general'] = $photo_query->result_array();

        $data['mainpage'] = 'backend/setting/general';
		$this->load->view('backend/templates', $data);
	}

	/**
     * Upload images
     * @return string
     */
    private function upload($rename) {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '159', 'height' => '106', 'type' => 'small'),
                array('width' => '303', 'height' => '192', 'type' => 'medium'),
                array('width' => '970', 'height' => '455', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '800', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png|gif',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '75%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '75%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'x_axis' => '0',
                    'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload = $mediaId;
        }

        return $format_upload;
    }

    /**
     * Upload images
     * @return string
     */
    private function upload2($rename) {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile2']['name']) && $_FILES['userfile2']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '159', 'height' => '106', 'type' => 'small'),
                array('width' => '303', 'height' => '192', 'type' => 'medium'),
                array('width' => '970', 'height' => '455', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '800', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png|gif',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile2');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '75%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '75%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'x_axis' => '0',
                    'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload = $mediaId;
        }

        return $format_upload;
    }

    /**
     * Upload images
     * @return string
     */
    private function upload3($rename) {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = str_replace('-','_',url_title($rename,'dash',true));
        if (isset($_FILES['userfile3']['name']) && $_FILES['userfile3']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/media/image/';
            //chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '159', 'height' => '106', 'type' => 'small'),
                array('width' => '303', 'height' => '192', 'type' => 'medium'),
                array('width' => '970', 'height' => '455', 'type' => 'large'),
                array('width' => '750', 'height' => '549', 'type' => 'large_mobile'),
                array('width' => '1920', 'height' => '800', 'type' => 'slider'),
                array('width' => '750', 'height' => '1079', 'type' => 'slider_mobile'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'jpg|jpeg|png|gif',
                'max_size' => '2048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload('userfile3');

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => APPPATH . '../asset_admin/assets/uploads/media/image',
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'quality' => '75%'
                );

                $dim = (intval($image_data["image_width"]) / intval($image_data["image_height"])) - ($config_thumb['width'] / $config_thumb['height']);
                $config_thumb['master_dim'] = ($dim > 0)? "height" : "width";

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . $rename . '_small' . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . $rename . '_medium' . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . $rename . '_large' . $image_data['file_ext'];
                        break;
                    case 'large_mobile':
                        $meta_image['large_mobile'] = $base_path . $rename . '_large_mobile' . $image_data['file_ext'];
                        break;
                    case 'slider':
                        $meta_image['slider'] = $base_path . $rename . '_slider' . $image_data['file_ext'];
                        break;
                    case 'slider_mobile':
                        $meta_image['slider_mobile'] = $base_path . $rename . '_slider_mobile' . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $rename . '_'.$value["type"] . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => false,
                    'quality' => '75%',
                    'width' => $value['width'],
                    'height' => $value['height'],
                    'x_axis' => '0',
                    'y_axis' => '0'
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                $test = unlink($base_path . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            //$format_upload = $rename . $image_data['file_ext'];
            $insert_media = array(
                'media_name' => $rename . $image_data['file_ext'],
                'media_type' => 'image',
                'media_directory' => base_url().'asset_admin/assets/uploads/media/image/'
            );

            $mediaId = $this->m_media->add($insert_media);
            $format_upload = $mediaId;
        }

        return $format_upload;
    }

}