<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_user');
	}

	public function index(){
		$data['base'] = 'Setting';

		$user_id = $this->session->userdata('user_id');

		$data['user'] = $this->m_user->get_id_user($user_id);

		$data['mainpage'] = 'backend/setting/user';
		$this->load->view('backend/templates', $data);
	}

	public function profile(){
		$data['base'] = 'Setting';

		$user_id = $this->session->userdata('user_id');

		if ($user_id) {
			if ($this->input->post('submit')) {
				// validation
                $valid = $this->form_validation;
                $valid->set_rules('user_first_name', 'First Name', 'required');
                $valid->set_rules('user_last_name', 'Last Name', 'required');

                if ($valid->run() == false) {
                    // run
                    $this->session->set_flashdata('error', validation_errors());
                    redirect('backend/setting/user#user-profile');
                } else {
                	$format_upload = $this->upload();
                    if ($format_upload != "") {
                    	$data = array(
	                        'user_first_name' => $this->input->post('user_first_name'),
	                        'user_last_name' => $this->input->post('user_last_name'),
	                        'user_phone' => $this->input->post('user_phone'),
                            'user_photo' => $format_upload,
	                        'user_modified_at' => date('Y-m-d H:i:s'),
	                        'user_modified_by' => $this->sess_id
	                    );

                        $this->m_user->edit($data, $user_id);
                        $this->session->set_userdata('user_photo', $format_upload);
                    }
                    else {
                        $data = array(
                            'user_first_name' => $this->input->post('user_first_name'),
                            'user_last_name' => $this->input->post('user_last_name'),
                            'user_phone' => $this->input->post('user_phone'),
                            
                            'user_modified_at' => date('Y-m-d H:i:s'),
                            'user_modified_by' => $this->sess_id
                        );

                        $this->m_user->edit($data, $user_id);
                        
                    }
                    
                    $this->session->set_flashdata('success', 'User berhasil diubah.');
                    redirect('backend/setting/user');
                }
			}
		} else {
			redirect('/');
		}
	}

	public function account(){
		$data['base'] = 'Setting';

		$user_id = $this->session->userdata('user_id');

		if ($user_id) {
			if ($this->input->post('submit')) {
				// validation
                $valid = $this->form_validation;
                $valid->set_rules('user_username', 'Username', 'required');
                $valid->set_rules('user_password', 'Password', 'required');
                $valid->set_rules('user_email', 'Email', 'required|valid_email');
                $valid->set_rules('user_confirm_password', 'Confirm Password', 'required|matches[user_password]');

                if ($valid->run() == false) {
                    // run
                    //var_dump(validation_errors());die;
                    $this->session->set_flashdata('error', validation_errors());
                    redirect('backend/setting/user#user-settings');
                } else {
                	$data = array(
                        'user_username' => $this->input->post('user_username'),
                        'user_email' => $this->input->post('user_email'),
                        'user_modified_at' => date('Y-m-d H:i:s'),
                        'user_modified_by' => $this->sess_id
                    );

                    $password = $this->input->post('user_password');
                    $old_password = $this->input->post('user_old_password');

                    if ($password != $old_password)
                        $data['user_password'] = md5($password);

                    $this->session->set_userdata('user_username', $this->input->post('user_username'));

                    $this->m_user->edit($data, $user_id);
                    $this->session->set_flashdata('success', 'User berhasil diubah.');
                    redirect('backend/setting/user');
                }
			}
		} else {
			redirect('/');
		}
	}

	/**
     * Upload images
     * @return string
     */
    private function upload() {
        $this->load->library('image_lib');
        $format_upload = '';
        $rename = url_title(time());
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {

            $base_path = APPPATH . '../asset_admin/assets/uploads/user/';
            chmod($base_path, 0777);
            $ori_path = $base_path . 'original/';

            $size = array(
                array('width' => '150', 'height' => '150', 'type' => 'small'),
                array('width' => '300', 'height' => '300', 'type' => 'medium'),
                array('width' => '650', 'height' => '650', 'type' => 'large'),
            );

            //UPLOAD ORG IMAGE
            $config = array(
                'upload_path' => $ori_path,
                'allowed_types' => 'gif|jpg|jpeg|png',
                'max_size' => '5048'
            );
            $this->load->library('upload', $config);
            $this->upload->do_upload();

            foreach ($size as $value) {

                $image_data = $this->upload->data();

                //RESIZE IMAGE
                $config_thumb = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => $base_path . $value["type"],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                    'width' => $value['width'],
                    'height' => $value['width']
                );

                $this->image_lib->initialize($config_thumb);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                //CROPING
                switch ($value['type']) {
                    case 'small':
                        $meta_image['small'] = $base_path . 'small' . '/' . $rename . $image_data['file_ext'];
                        break;
                    case 'medium':
                        $meta_image['medium'] = $base_path . 'medium' . '/' . $rename . $image_data['file_ext'];
                        break;
                    case 'large':
                        $meta_image['large'] = $base_path . 'large' . '/' . $rename . $image_data['file_ext'];
                        break;
                }

                $config_crop = array(
                    'image_library' => 'gd2',
                    'source_image' => $base_path . $value["type"] . '/' . $image_data['raw_name'] . $image_data['file_ext'],
                    'new_image' => $base_path . $value["type"] . '/' . $rename . $image_data['file_ext'],
                    'create_thumb' => false,
                    'maintain_ratio' => true,
                );

                $this->image_lib->initialize($config_crop);
                if (!$this->image_lib->crop()) {
                    echo $this->image_lib->display_errors();
                }

                //DELETE RESIZE IMAGE
                unlink($base_path . $value["type"] . '/' . $image_data['raw_name'] . $image_data['file_ext']);
                $this->image_lib->clear();
            }

            rename($image_data['full_path'], $ori_path . $rename . $image_data['file_ext']);
            $format_upload = $rename . $image_data['file_ext'];
        }

        return $format_upload;
    }

}