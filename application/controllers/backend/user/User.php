<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends My_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_user');
        $this->load->model('m_group');
        $this->load->model('m_action');

        $this->access_id = 100;
	}

	public function index() {
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

		$data['base'] = 'User';

		$limit = 10;

        $this->db->where_in('user_is_trash', array('0'));
        $this->db->order_by("user_id", "desc");
        $this->db->limit($limit, $this->uri->segment(4));
        $photo_query = $this->db->get('cms_users');
        $data['users'] = $photo_query->result_array();

        $this->db->where_in('user_is_trash', array('0'));
        $query_total = $this->db->get('cms_users');
        $data['total'] = $query_total->num_rows();

        $this->load->library('pagination');
        $config['base_url'] = site_url('backend/user/user');
        $config['total_rows'] = $data['total'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $config['num_links'] = 2;
        $config['prev_link'] = '&laquo;';
        $config['prev_tag_open'] = '<li><span><span aria-hidden="true">';
        $config['prev_tag_close'] = '</span></span></li>';
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li><span aria-hidden="true">';
        $config['next_tag_close'] = '</span></li>';
        $config['last_link'] = '';
        $config['last_tag_open'] = '';
        $config['last_tag_close'] = '';
        $config['first_link'] = '';
        $config['first_tag_open'] = '';
        $config['first_tag_close'] = '';
        $config['num_tag_open'] = '<li><span>';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="active"><span>';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $this->pagination->initialize($config);
        $data['page_links'] = $this->pagination->create_links();

		$data['mainpage'] = 'backend/user/user';
		$this->load->view('backend/templates', $data);
	}

    public function add(){
        $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
        if(count($add_action) < 1)
            show_404();

        if ($this->input->post('submit')) {
            // validation
            $valid = $this->form_validation;
            $valid->set_rules('user_first_name', 'First Name', 'required');
            $valid->set_rules('user_last_name', 'Last Name', 'required');
            //$valid->set_rules('user_address', 'Address', 'required');
            $valid->set_rules('user_email', 'Email', 'required|valid_email');
            //$valid->set_rules('user_mobile', 'Mobile No', 'required');
            //$valid->set_rules('user_phone', 'Home Phone', 'required');
            $valid->set_rules('group_id', 'Group', 'required');
            $valid->set_rules('user_username', 'Username', 'required');
            $valid->set_rules('user_password', 'Password', 'required');
            $valid->set_rules('user_confirm_password', 'Confirm Password', 'required|matches[user_password]');

            if ($valid->run() == false) {
                // run
                $this->session->set_flashdata('error', validation_errors());
            } else {
                $this->db->where('user_email', $this->input->post('user_email'));
                $cekemail = $this->db->get('cms_users');

                $this->db->where('user_username', $this->input->post('user_username'));
                $cekusername = $this->db->get('cms_users');

                if($cekusername->num_rows() > 0){
                    $this->session->set_flashdata('error', 'A user with this username already exist.');
                    redirect('backend/user/user');
                }

                // if ($cekemail->num_rows() > 0) {
                //     $this->session->set_flashdata('error', 'Email yang Anda masukkan telah terdaftar.');
                //     redirect('backend/user/user');
                // } else {
                    if ($this->input->post('user_password') == $this->input->post('user_confirm_password')) {

                        $user_activation_code = date("YmdHis") . $this->generateString(5);

                        $data = array(
                            'user_first_name' => $this->input->post('user_first_name'),
                            'user_last_name' => $this->input->post('user_last_name'),
                            'user_address' => $this->input->post('user_address'),
                            'user_email' => $this->input->post('user_email'),
                            'user_mobile' => $this->input->post('user_mobile'),
                            'user_phone' => $this->input->post('user_phone'),
                            'group_id' => $this->input->post('group_id'),
                            'user_username' => $this->input->post('user_username'),
                            'user_password' => md5($this->input->post('user_password')),
                            'user_activation_code' => $user_activation_code,
                            'user_created_at' => date('Y-m-d H:i:s'),
                            'user_created_by' => $this->sess_id,
                            'user_status' => 0,
                            'user_is_trash' => 0
                        );

                        $this->m_user->add($data);

                        $config = unserialize(mail);

                        $this->load->library('email', $config);
                        $this->email->set_newline("\r\n");

                        $this->email->from('no-reply@kemenpar.go.id', 'kemenpar.go.id');
                        $this->email->to($this->input->post('user_email'));
                        $this->email->subject('Activation User kemenpar.go.id');
                        $this->email->message('Klik link berikut atau copy link dan paste ke browser Anda untuk aktivasi akun Kemenpar 
                        ' . site_url('auth/auth/activation/' . $user_activation_code));

                        if ($this->email->send()) {
                            $this->session->set_flashdata('success', 'Your account has been successfully created. Please check your email for the instructions on how to confirm your account.');
                            redirect('backend/user/user');
                        } else {
                            $this->session->set_flashdata('error', 'Server failed to send email. Please make sure your email is correct.');
                            redirect('backend/user/user');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'The confirm password does not match.');
                        redirect('backend/user/user');
                    }
                //}
            }
        }

        $data['base'] = 'User';
        $data['group'] = $this->m_group->get();
        $data['mainpage'] = 'backend/user/add_user';
        $this->load->view('backend/templates', $data);
    }

    public function resend_activation(){
        $user_id = $this->uri->segment(5);

        if($user_id){
            $this->db->where('user_id', $user_id);
            $this->db->where('user_status', 0);
            $this->db->where('user_is_trash', 0);
            $cekid = $this->db->get('cms_users')->row();

            $this->db->where('user_email', $cekid->user_email);
            $this->db->where('user_status', 0);
            $this->db->where('user_is_trash', 0);
            $cekemail = $this->db->get('cms_users');

            if ($cekemail->num_rows() > 0) {
                $user = $cekemail->row();

                $config = unserialize(mail);

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");

                $this->email->from('no-reply@kemenpar.go.id', 'kemenpar.go.id');
                $this->email->to($user->user_email);
                $this->email->subject('Activation User kemenpar.go.id');
                $this->email->message('Klik link berikut atau copy link dan paste ke browser Anda untuk aktivasi akun Kemenpar 
                ' . site_url('auth/auth/activation/' . $user->user_activation_code));

                if ($this->email->send()) {
                    $this->session->set_flashdata('success', 'Activation email has been sen to '.$user->user_email.' .');
                    redirect('backend/user/user');
                } else {
                    $this->session->set_flashdata('error', 'Server failed to send email. Please make sure that email is correct.');
                    redirect('backend/user/user');
                }
            } else {
                $this->session->set_flashdata('error', 'The email address was not recognized.');
                redirect('backend/user/user');
            }
        } else {
            redirect('backend/user/user');
        }
    }

    public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

        $id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                $valid->set_rules('user_first_name', 'First Name', 'required');
                $valid->set_rules('user_last_name', 'Last Name', 'required');
                //$valid->set_rules('user_address', 'Address', 'required');
                $valid->set_rules('user_email', 'Email', 'required');
                //$valid->set_rules('user_mobile', 'Mobile No', 'required');
                //$valid->set_rules('user_phone', 'Home Phone', 'required');
                $valid->set_rules('group_id', 'Group', 'required');
                $valid->set_rules('user_username', 'Username', 'required');
                $valid->set_rules('user_password', 'Password', 'required');
                $valid->set_rules('user_confirm_password', 'Confirm Password', 'required|matches[user_password]');
                $valid->set_rules('user_status', 'Status', 'required');

                if ($valid->run() == false) {
                    // run
                    $this->session->set_flashdata('error', validation_errors());
                } else {
                    $this->db->where('user_email', $this->input->post('user_email'));
                    $cekemail = $this->db->get('cms_users');

                    $this->db->where('user_username', $this->input->post('user_username'));
                    $cekusername = $this->db->get('cms_users');

                    if ($this->input->post('user_username') != $this->input->post('user_old_username')){
                        if($cekusername->num_rows() > 0){
                            $this->session->set_flashdata('error', 'A user with this username already exist.');
                            redirect('backend/user/user');
                        }
                    }

                    // if ($cekemail->num_rows() > 0) {
                    //     $this->session->set_flashdata('error', 'Email yang Anda masukkan telah terdaftar.');
                    //     redirect('backend/user/user');
                    // }

                    $data = array(
                        'user_first_name' => $this->input->post('user_first_name'),
                        'user_last_name' => $this->input->post('user_last_name'),
                        'user_address' => $this->input->post('user_address'),
                        'user_email' => $this->input->post('user_email'),
                        'user_mobile' => $this->input->post('user_mobile'),
                        'user_phone' => $this->input->post('user_phone'),
                        'group_id' => $this->input->post('group_id'),
                        'user_username' => $this->input->post('user_username'),
                        'user_modified_at' => date('Y-m-d H:i:s'),
                        'user_modified_by' => $this->sess_id,
                        'user_status' => $this->input->post('user_status'),
                        'user_is_trash' => 0
                    );

                    $password = $this->input->post('user_password');
                    $old_password = $this->input->post('user_old_password');

                    if ($password != $old_password)
                        $data['user_password'] = md5($password);

                    $this->m_user->edit($data, $id);
                    redirect('backend/user/user');
                }
            }

            $data['base'] = 'User';
            $data['user'] = $this->m_user->get_id_user($id);
            $data['group'] = $this->m_group->get();
            $data['group_detail'] = $this->m_group->get_id_group($data['user']->group_id);
            //var_dump($data['group']);die;
            $data['mainpage'] = 'backend/user/edit_user';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/group/group');
        }
    }

    public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $data = array('user_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_user->delete($data, $id);
            redirect('backend/user/user');
        } else {
            redirect('backend/user/user');
        }
    }

    public function views(){
        $view_action = $this->m_access->get_access_group_action($this->group_id, 5, $this->access_id);
        if(count($view_action) < 1)
            show_404();

        $id = $this->uri->segment(5);
        if ($id) {
            $data['base'] = 'User';
            $data['iniform'] = true;
            $data['user'] = $this->m_user->get_id_user($id);
            $data['mainpage'] = 'backend/user/view_user';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/user/user');
        }
    }

    public function generateString($length) {
        // mulai dengan string kosong
        $v_string = "";

        // definisikan karakter-karakter yang diperbolehkan
        $possible = "0123456789bcdfghjkmnpqrstvwxyz";

        // set up sebuah counter
        $i = 0;

        // tambahkan karakter acak ke $v_string sampai $length tercapai
        while ($i < $length) {
            // ambil sebuah karakter acak dari beberapa
            // kemungkinan yang sudah ditentukan tadi
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);

            // kami tidak ingin karakter ini jika sudah ada pada string
            if (!strstr($v_string, $char)) {
                $v_string .= $char;
                $i++;
            }
        }
        return $v_string;
    }

}