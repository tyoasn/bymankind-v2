<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Career extends My_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('m_career');
        $this->load->model('m_user');
        $this->load->model('m_action');
        $this->load->helper("custom");

        $this->access_id = 13;
    }

    public function index(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Career';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where('career_status', 1);
        $this->db->where_in('career_is_trash', array('0'));
        $this->db->order_by("career_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_career');
        //var_dump($this->db->last_query());die;
        $data['career'] = $photo_query->result_array();

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where('career_status', 1);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total'] = $query_total->num_rows();

        $this->db->where('career_status', 2);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

            $this->db->where('career_status', 3);
            $this->db->where_in('career_is_trash', array('0'));
            $query_total = $this->db->get('cms_career');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total']);

        $data['mainpage'] = 'backend/career/career';
        $this->load->view('backend/templates', $data);
    }

    public function all(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Careerall';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where_in('career_is_trash', array('0'));
        $this->db->order_by("career_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_career');
        //var_dump($this->db->last_query());die;
        $data['career'] = $photo_query->result_array();

        $this->db->where('career_status', 1);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total'] = $query_total->num_rows();

        $this->db->where('career_status', 2);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_draft'] = $query_total->num_rows();

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

            $this->db->where('career_status', 3);
            $this->db->where_in('career_is_trash', array('0'));
            $query_total = $this->db->get('cms_career');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_all']);

        $data['mainpage'] = 'backend/career/career';
        $this->load->view('backend/templates', $data);
    }

    public function main(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Careermain';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where_in('career_is_trash', array('0'));
        $this->db->order_by("career_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_career');
        //var_dump($this->db->last_query());die;
        $data['career'] = $photo_query->result_array();

        $this->db->where('career_status', 1);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total'] = $query_total->num_rows();

        $this->db->where('career_status', 2);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_all'] = $query_total->num_rows();

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

            $this->db->where('career_status', 3);
            $this->db->where_in('career_is_trash', array('0'));
            $query_total = $this->db->get('cms_career');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging'] = paginate_function_post($limit,$page,$data['total_main']);

        $data['mainpage'] = 'backend/career/career';
        $this->load->view('backend/templates', $data);
    }

    public function draft(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Careerdraft';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where('career_status', 2);
        $this->db->where_in('career_is_trash', array('0'));
        $this->db->order_by("career_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_career');
        $data['career'] = $photo_query->result_array();

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where('career_status', 2);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where('career_status', 1);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){

            $this->db->where('career_status', 3);

            $this->db->where_in('career_is_trash', array('0'));
            $query_total = $this->db->get('cms_career');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_draft']);

        $data['mainpage'] = 'backend/career/career';
        $this->load->view('backend/templates', $data);
    }

    public function pending(){
        $search_action = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
        if(count($search_action) < 1)
            show_404();

        $data['base'] = 'Careerpending';

        $title = '';
        if($this->input->get('title') != ""){
            $title = $this->input->get('title');
        }

        $limit = 10;
        $page  = 1;
        if($this->input->get('page'))
            $page = $this->input->get('page');

        //$offset = $this->uri->segment(5);
        $offset = ($page-1)*$limit;

        if($title != ""){
            $this->db->like('career_title', $title);
        }
        $this->db->where('career_status', 3);
        $this->db->where_in('career_is_trash', array('0'));
        $this->db->order_by("career_id", "desc");
        $this->db->limit($limit, $offset);
        $photo_query = $this->db->get('cms_career');
        $data['career'] = $photo_query->result_array();

        $this->db->where('career_status', 2);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_draft'] = $query_total->num_rows();

        $this->db->where('career_status', 1);
        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_all'] = $query_total->num_rows();

        $this->db->where_in('career_is_trash', array('0'));
        $query_total = $this->db->get('cms_career');
        $data['total_main'] = $query_total->num_rows();

        if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3){
            if($title != ""){
                $this->db->like('career_title', $title);
            }
            $this->db->where('career_status', 3);

            $this->db->where_in('career_is_trash', array('0'));
            $query_total = $this->db->get('cms_career');
            $data['total_pending'] = $query_total->num_rows();
        }

        $data['paging']             = paginate_function_post($limit,$page,$data['total_pending']);

        $data['mainpage'] = 'backend/career/career';
        $this->load->view('backend/templates', $data);
    }

    public function add(){
        $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id);
        if(count($add_action) < 1)
            show_404();

        if ($this->input->post('submit')) {
            //var_dump($_POST);die;
            // validation
            $valid = $this->form_validation;
            $valid->set_rules('career_title', 'Title', 'required');
            //$valid->set_rules('post_subtitle', 'Subtitle', 'required');
            //$valid->set_rules('post_content', 'Content', 'required');
            if($this->group_id != 3){
                $valid->set_rules('career_status', 'Status', 'required');
            }
            // $valid->set_rules('post_category_id', 'Categories', 'required');
            //$valid->set_rules('post_taxonomy', 'Tags', 'required');

            if ($valid->run() == false) {
                // run
                //var_dump(validation_errors());die;
            } else {

                $data = array(
                    'career_title' => $this->input->post('career_title'),
                    'career_content' => $this->input->post('career_content'),
                    'career_link' => $this->input->post('career_link'),
                    'career_status' => $this->input->post('career_status'),
                    'career_is_trash' => 0
                );

                $career_id = $this->m_career->add($data);

                $this->session->set_flashdata('success', 'Career successfully created.');
                redirect('backend/career/career/all');
            }
        }

        $data['base'] = 'Content';
        $data['iniform'] = true;
        $data['mainpage'] = 'backend/career/add_career';
        $this->load->view('backend/templates', $data);
    }

    public function edit(){
        $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id);
        if(count($edit_action) < 1)
            show_404();

        $id = $this->uri->segment(5);
        if ($id) {
            if ($this->input->post('submit')) {
                // validation
                $valid = $this->form_validation;
                // $valid->set_rules('post_title', 'Title', 'required');
                //$valid->set_rules('post_subtitle', 'Subtitle', 'required');
                //$valid->set_rules('post_content', 'Content', 'required');
                if($this->group_id != 3){
                    $valid->set_rules('career_status', 'Status', 'required');
                }
                //$valid->set_rules('post_taxonomy', 'Tags', 'required');

                if ($valid->run() == false) {
                    // run
                } else {

                    if ($format_upload != "") {
                        $data = array(
                            'career_title' => $this->input->post('career_title'),
                            'career_content' => $this->input->post('career_content'),
                            'career_link' => $this->input->post('career_link'),
                            'career_status' => $this->input->post('career_status'),
                            'career_is_trash' => 0
                        );

                        $this->m_career->edit($data, $id);
                    } else {
                        $data = array(
                            'career_title' => $this->input->post('career_title'),
                            'career_content' => $this->input->post('career_content'),
                            'career_link' => $this->input->post('career_link'),
                            'career_status' => $this->input->post('career_status'),
                            'career_is_trash' => 0
                        );

                        $this->m_career->edit($data, $id);
                    }

                    redirect('backend/career/career/all');
                }
            }

            $data['base'] = 'Group';
            $data['iniform'] = true;

            $data['career'] = $this->m_career->get_id_post($id);
            $data['mainpage'] = 'backend/career/edit_career';
            $this->load->view('backend/templates', $data);
        } else {
            redirect('backend/career/career/all');
        }
    }


    public function delete(){
        $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id);
        if(count($delete_action) < 1)
            show_404();

        if ($this->uri->segment(5)) {
            $data = array('career_is_trash' => 1);
            $id = $this->uri->segment(5);
            $this->m_career->delete($data, $id);
            redirect('backend/career/career/all');
        } else {
            redirect('backend/career/career/all');
        }
    }

   

}