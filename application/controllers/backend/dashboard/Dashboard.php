<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends My_Controller {

	public function __construct() {
		parent::__construct();
		$this->access_id = 1;
	}

	public function index() {
		$search = $this->m_access->get_access_group_action($this->group_id, 1, $this->access_id);
		if(count($search) < 1)
			show_404();

		$data['base'] = 'Dashboard';

		$data['posts'] = $this->db->where('post_status', 1)->where('post_is_trash', 0)->get('cms_posts')->num_rows();
		$data['contents'] = $this->db->where('post_status', 1)->where('post_is_trash', 0)->get('cms_contents')->num_rows();
		$data['members'] = $this->db->where('team_status', 1)->where('team_is_trash', 0)->get('cms_team')->num_rows();
		$data['division'] = $this->db->where('division_status', 1)->where('division_is_trash', 0)->get('cms_division')->num_rows();
		$data['career'] = $this->db->where('career_status', 1)->where('career_is_trash', 0)->get('cms_career')->num_rows();

        $this->db->where('media_created_by', $this->sess_id);
        $query = $this->db->get('cms_media');
        $data['media'] = $query->num_rows();

        $this->db->where_in('user_is_trash', array('0'));
        $query_total = $this->db->get('cms_users');
        $data['users'] = $query_total->num_rows();

		$data['mainpage'] = 'backend/dashboard/dashboard';
		$this->load->view('backend/templates', $data);
    }

}