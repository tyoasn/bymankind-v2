<div class="sidebar" data-color="blue-mankind">
   <!-- SideBar Wrapper -->
   <div class="sidebar-wrapper">
      <div class="user">
         <div class="photo">
            <?php if($this->session->userdata('user_photo') != NULL): ?>
              <img class="img-circle" src="<?php echo base_url(); ?>asset_admin/assets/uploads/user/small/<?php echo $this->session->userdata('user_photo'); ?>" alt="User Image">
            <?php else: ?>
              <img class="img-circle" src="<?php echo base_url(); ?>asset_admin/img/logobymankind.png" alt="User Image">
            <?php endif; ?>

         </div>
         <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
            <?php echo $this->session->userdata('user_username'); ?>
            <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
               <ul class="nav">
                  <li><a href="<?php echo base_url(); ?>backend/setting/user">Profile</a></li>
                  <!-- <li><a href="#">Password</a></li> -->
                  <li><a href="<?php echo base_url(); ?>auth/auth/logout">Sign Out</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Menu Section -->

      <?php foreach($this->access as $menu): ?>
         <?php $alias = strpos($menu->access_alias, '>'); ?>
         <?php if($alias !== false): /* ada */ ?>
            <ul class="nav">
               <li <?php if($menu->access_group == $this->uri->segment(2)){ echo 'class="active"'; } ?>>
                  <a data-toggle="collapse" href="#<?php echo $menu->access_id; ?>">
                     <i class="<?php echo $menu->access_icon; ?>"></i>
                     <p><?php echo explode('>', $menu->access_alias)[0]; ?>
                        <b class="caret"></b>
                     </p>
                  </a>
                  <div class="collapse" id="<?php echo $menu->access_id; ?>">
                     <?php
                          $access_exists = $this->m_access->get_access_exists(explode('>', $menu->access_alias)[0], $this->group_id, 1, '');
                      ?>
                     <ul class="nav">
                           <?php foreach($access_exists as $k): ?>
                              <?php
                                  //$ex = explode('>', $k->access_alias)[1];
                                  echo '<li><a href="'.base_url().'backend/'.$k->access_group.'/'.$k->access_directory.'">'.explode('>', $k->access_alias)[1].'</a></li>';
                              ?>
                          <?php endforeach; ?>
                     </ul>
                  </div>
               </li>
            </ul>
         <?php else: /* tidak ada */ ?>
            <ul class="nav">
               <li <?php if($menu->access_group == $this->uri->segment(2)){ echo 'class="active"'; } ?>>
                  <a href="<?php echo base_url().'backend/'.$menu->access_group.'/'.$menu->access_directory; ?>">
                     <i class="<?php echo $menu->access_icon; ?>"></i>
                     <p><?php echo $menu->access_alias; ?></p>
                  </a>
               </li>
            </ul>
         <?php endif; ?>
      <?php endforeach; ?>
   </div>
</div>