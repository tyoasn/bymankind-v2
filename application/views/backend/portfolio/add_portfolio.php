<div class="container-fluid">
   <div class="header">
      <h3 class="title">Add New Portfolio</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Title 
                        <star>*</star>
                        <?php echo form_error('portfolio_title','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="portfolio_title" type="text" placeholder="Enter title" autocomplete="off" required>
                     <input type="hidden" name="id" id="post-id" value="0">
                  </div>
                  <div class="form-group">
                     <label>
                        Link 
                        <star>*</star>
                        <?php echo form_error('portfolio_link','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="portfolio_link" type="text" placeholder="Enter link" autocomplete="off" required>
                     <input type="hidden" name="id" id="post-id" value="0">
                  </div>
                  <div class="form-group">
                     <label>
                        Business Challenge 
                        <star>*</star>
                        <?php echo form_error('portfolio_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="portfolio_content" id="post-content1" rows="4" placeholder="Enter content"></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Our Creative Solution
                        <star>*</star>
                        <?php echo form_error('portfolio_content_2','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="portfolio_content_2" id="post-content2" rows="4" placeholder="Enter content"></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Our Services
                        <star>*</star>
                        <?php echo form_error('portfolio_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="portfolio_content_3" id="post-content" rows="4" placeholder="Enter content"></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Image Description 1
                        <star>*</star>
                        <?php echo form_error('portfolio_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="portfolio_content_4" id="post-content4" rows="4" placeholder="Enter content"></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Image Description 2
                        <star>*</star>
                        <?php echo form_error('portfolio_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="portfolio_content_5" id="post-content5" rows="4" placeholder="Enter content"></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Image Description 3
                        <star>*</star>
                        <?php echo form_error('portfolio_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="portfolio_content_6" id="post-content6" rows="4" placeholder="Enter content"></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Image Description 4
                        <star>*</star>
                        <?php echo form_error('portfolio_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="portfolio_content_7" id="post-content7" rows="4" placeholder="Enter content"></textarea>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
              <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>Image 1</label><br/>
                     <img src="http://via.placeholder.com/1920x1080" width="200">
                  </div>
                  <div class="form-group">
                     <label>
                        File 
                        <star>*</star>
                     </label>
                     <input type="file" class="form-control" class="form-control" name="userfile" id="userfile" required="true">
                      <span style="color:#ab3737;"> * Maximum size 2MB </span>
                       <br>
                       <span style="color:#ab3737;"> * Image must high resolution </span>
                       <br>
                       <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                  </div>
               </div>
            </div>
            </div>
            <div class="col-md-4">
              <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>Image 2</label><br/>
                     <img src="http://via.placeholder.com/1920x1080" width="200">
                  </div>
                  <div class="form-group">
                     <label>
                        File 
                        <star>*</star>
                     </label>
                     <input type="file" class="form-control" class="form-control" name="userfile2" id="userfile2" required="true">
                      <span style="color:#ab3737;"> * Maximum size 2MB </span>
                       <br>
                       <span style="color:#ab3737;"> * Image must high resolution </span>
                       <br>
                       <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                  </div>
               </div>
            </div>
            </div>
            <div class="col-md-4">
              <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>Image 3</label><br/>
                     <img src="http://via.placeholder.com/1920x1080" width="200">
                  </div>
                  <div class="form-group">
                     <label>
                        File 
                        <star>*</star>
                     </label>
                     <input type="file" class="form-control" class="form-control" name="userfile3" id="userfile3" required="true">
                      <span style="color:#ab3737;"> * Maximum size 2MB </span>
                       <br>
                       <span style="color:#ab3737;"> * Image must high resolution </span>
                       <br>
                       <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                  </div>
               </div>
            </div>
            </div>
            <div class="col-md-4">
              <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>Image 4</label><br/>
                     <img src="http://via.placeholder.com/1920x1080" width="200">
                  </div>
                  <div class="form-group">
                     <label>
                        File 
                        <star>*</star>
                     </label>
                     <input type="file" class="form-control" class="form-control" name="userfile4" id="userfile4" required="true">
                      <span style="color:#ab3737;"> * Maximum size 2MB </span>
                       <br>
                       <span style="color:#ab3737;"> * Image must high resolution </span>
                       <br>
                       <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                  </div>
               </div>
            </div>
            </div>
            <div class="col-md-4">
              <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>Image 5</label><br/>
                     <img src="http://via.placeholder.com/1920x1080" width="200">
                  </div>
                  <div class="form-group">
                     <label>
                        File 
                        <star>*</star>
                     </label>
                     <input type="file" class="form-control" class="form-control" name="userfile5" id="userfile5" required="true">
                      <span style="color:#ab3737;"> * Maximum size 2MB </span>
                       <br>
                       <span style="color:#ab3737;"> * Image must high resolution </span>
                       <br>
                       <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                  </div>
               </div>
            </div>
            </div>
            <div class="col-md-4">
              <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>Image 6</label><br/>
                     <img src="http://via.placeholder.com/1920x1080" width="200">
                  </div>
                  <div class="form-group">
                     <label>
                        File 
                        <star>*</star>
                     </label>
                     <input type="file" class="form-control" class="form-control" name="userfile6" id="userfile6" required="true">
                      <span style="color:#ab3737;"> * Maximum size 2MB </span>
                       <br>
                       <span style="color:#ab3737;"> * Image must high resolution </span>
                       <br>
                       <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                  </div>
               </div>
            </div>
            </div>
         </div>
         <!--  end col-md-6  -->
         <div class="col-md-3">
            <div class="card">
               <div class="content">
                  <?php if($this->group_id != 3): ?>
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="portfolio_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2">Draft</option>
                           <option value="1">Publish</option>
                        </select>
                     </div>
                  <?php else: ?>
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="portfolio_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2">Draft</option>
                           <option value="3">For Review</option>
                        </select>
                     </div>
                  <?php endif; ?>
                  <div class="form-group">
                        <label class="control-label">Publish Date <?php echo form_error('portfolio_publish_date','<span style="color:red;">','</span>'); ?></label>
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input class="form-control datetimepicker" name="portfolio_publish_date" id="publishDate" type="text" placeholder="Select Date" autocomplete="off" required>
                        </div>
                        <div id="ket_waktu">
                            <label class="control-label">Jam</label>
                            <select name="jam" class="form-control">
                                <?php for ($i = 0; $i <= 23; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>"><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                            <label class="control-label">Menit</label>
                            <select name="menit" class="form-control">
                                <?php for ($i = 0; $i <= 59; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>"><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                  <div class="form-group" align="right">
                     <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
   <!--  end col-md-6  -->
   </div>
   </form>
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<?php $this->load->view('backend/post/ckeditor.php'); ?>

<script type="text/javascript">
    $(function () {
        $('.summernote').summernote({
            height: 250,
            minHeight: 250, // set minimum height of editor
            maxHeight: 500, // set maximum height of editor
            focus: true
        });

        /*$('form').on('submit', function (e) {
         e.preventDefault();
         alert($('.summernote').summernote('code'));
         alert($('.summernote').val());
         });*/
    });

    jQuery(document).ready(function(){
        jQuery("#ket_waktu").css("display","none");
    });

    $('#publishDate').datetimepicker({
      format: "YYYY-MM-DD",
    }).on('dp.change', function(e){
      jQuery("#ket_waktu").css("display","inline");
    });

    jQuery(document).ready(function () {
       $("#is_thumbnail").click(function () {
          $('#userfile').attr("disabled", $(this).is(":checked"));
       });
    });
</script>