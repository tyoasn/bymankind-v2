<div class="container-fluid">
<div class="header">
 <h3 class="title">Site Settings
 </h3>
</div>
<div class="row">
    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    <div class="col-md-9">
        <div class="card">
            <h3 class="card-title"></h3>
            <div class="content">
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[0]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[0]['config_value'] != ""){
                                $a = $general[0]['config_value'];
                            } else {
                                $a = " ";
                            }
                        ?>
                        <input class="form-control" name="general_site_title" value="<?php echo $a; ?>" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[1]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[1]['config_value'] != ""){
                                $b = $general[1]['config_value'];
                            } else {
                                $b = " ";
                            }
                        ?>
                        <input class="form-control" name="general_keywords" value="<?php echo $b; ?>" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[2]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[2]['config_value'] != ""){
                                $c = $general[2]['config_value'];
                            } else {
                                $c = " ";
                            }
                        ?>
                        <textarea class="form-control" name="general_description"><?php echo $c; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[4]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[4]['config_value'] != ""){
                                $d = $general[4]['config_value'];
                            } else {
                                $d = " ";
                            }
                        ?>
                            <textarea class="form-control" name="general_address"><?php echo $d; ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[5]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[5]['config_value'] != ""){
                                $e = $general[5]['config_value'];
                            } else {
                                $e = " ";
                            }
                        ?>
                        <input class="form-control" name="general_phone" value="<?php echo $e; ?>" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[7]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[7]['config_value'] != ""){
                                $f = $general[7]['config_value'];
                            } else {
                                $f = " ";
                            }
                        ?>
                        <input class="form-control" name="general_email" value="<?php echo $f; ?>" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[3]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[3]['config_value']){
                                $media = $this->m_media->get_media_id($general[3]['config_value']);
                                $thumbpath = pathinfo($media->media_name);
                                $image = base_url().'asset_admin/assets/uploads/media/image/'.$thumbpath['filename'].'_small.'.$thumbpath['extension'];
                                echo '<img src="'.$image.'" alt=""/>';
                            } else {
                                $image = 'https://via.placeholder.com/80x80';
                                echo '<img src="'.$image.'" alt=""/>';
                                //echo '<span>Foto tidak ditemukan!</span>';
                            }
                        ?>
                        <input class="form-control" name="userfile" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $general[14]['config_key']; ?></label>
                    <div class="col-md-10">
                        <?php
                            if($general[14]['config_value']){
                                $media = $this->m_media->get_media_id($general[14]['config_value']);
                                $thumbpath = pathinfo($media->media_name);
                                $image = base_url().'asset_admin/assets/uploads/media/image/'.$thumbpath['filename'].'_small.'.$thumbpath['extension'];
                                echo '<img src="'.$image.'" alt=""/>';
                            } else {
                                $image = 'https://via.placeholder.com/50x50';
                                echo '<img src="'.$image.'" alt=""/>';
                                //echo '<span>Foto tidak ditemukan!</span>';
                            }
                        ?>
                        <input class="form-control" name="userfile2" type="file">
                    </div>
                </div>
            </div>
            <div class="content">
                <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                <input type="button" value="Reset" onclick="window.location='<?php echo base_url(); ?>backend/setting/general/reset';return false;" class="btn btn-info btn-fill btn-wd">
            </div>
        </div>
    </div>
    </form>
    <div class="clearix"></div>
</div>
</div>