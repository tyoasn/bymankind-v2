<div class="container-fluid">
<div class="header">
 <h3 class="title">Social Media
 </h3>
</div>
<br>
<div class="row">
    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
    <div class="col-md-9">
        <div class="card">
            <div class="content">
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $footer[0]['config_key']; ?></label>
                    <div class="col-md-10">
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                            <input class="form-control" type="text" name="footer_facebook" value="<?php echo $footer[0]['config_value']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $footer[1]['config_key']; ?></label>
                    <div class="col-md-10">
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                            <input class="form-control" type="text" name="footer_twitter" value="<?php echo $footer[1]['config_value']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $footer[2]['config_key']; ?></label>
                    <div class="col-md-10">
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-youtube"></i></span>
                            <input class="form-control" type="text" name="footer_youtube" value="<?php echo $footer[2]['config_value']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $footer[3]['config_key']; ?></label>
                    <div class="col-md-10">
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                            <input class="form-control" type="text" name="footer_instagram" value="<?php echo $footer[3]['config_value']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2"><?php echo $footer[6]['config_key']; ?></label>
                    <div class="col-md-10">
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                            <input class="form-control" type="text" name="footer_linkedin" value="<?php echo $footer[6]['config_value']; ?>">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="content">
                <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                <input type="button" value="Reset" onclick="window.location='<?php echo base_url(); ?>backend/setting/social/reset';return false;" class="btn btn-info btn-fill btn-wd">
            </div>
        </div>
    </div>
    </form>
    <div class="clearix"></div>

</div>

<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/bootstrap-datepicker.min.js"></script>
