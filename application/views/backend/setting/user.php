<div class="container-fluid">
   <div class="row">
      <div class="col-md-8">
         <div class="card">
            <div class="header">
               <h4 class="title">Profile</h4>
            </div>
            <div class="content">
               <form method="POST" action="<?php echo base_url(); ?>backend/setting/user/profile" enctype="multipart/form-data">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>First Name</label>
                           <input class="form-control" type="text" name="user_first_name" value="<?php echo $user->user_first_name; ?>" required>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Last Name</label>
                           <input class="form-control" type="text" name="user_last_name" value="<?php echo $user->user_last_name; ?>" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Phone</label>
                           <input class="form-control" type="text" name="user_phone" value="<?php echo $user->user_phone; ?>">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Address</label>
                           <textarea rows="5" class="form-control" placeholder="Here can be your description" value="Address" name="address">Jakarta</textarea>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Profile Picture</label>
                           <input class="form-control" name="userfile" type="file">
                        </div>
                     </div>
                  </div>
                  <button class="btn btn-info btn-fill pull-right" name="submit" type="submit" value="submit"> Save</button>
                  <div class="clearfix"></div>
               </form>
            </div>
         </div>
      </div>
      <div class="col-md-8">
         <div class="card">
            <div class="header">
               <h4 class="title">Account Setting</h4>
            </div>
            <div class="content">
               <form method="POST" action="<?php echo base_url(); ?>backend/setting/user/account" enctype="multipart/form-data">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Username <?php echo form_error('user_username','<span style="color:red;">','</span>'); ?></label>
                           <input class="form-control" type="text" name="user_username" value="<?php echo $user->user_username; ?>" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Email <?php echo form_error('user_email','<span style="color:red;">','</span>'); ?></label>
                           <input class="form-control" type="text" name="user_email" value="<?php echo $user->user_email; ?>" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Password <?php echo form_error('user_password','<span style="color:red;">','</span>'); ?></label>
                           <input class="form-control" type="password" name="user_password" value="<?php echo $user->user_password; ?>" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <label>Confirm Password <?php echo form_error('user_confirm_password','<span style="color:red;">','</span>'); ?></label>
                           <input class="form-control" type="password" name="user_confirm_password" value="<?php echo $user->user_password; ?>" required>
                        </div>
                     </div>
                  </div>
                  <input class="form-control" name="user_old_password" type="hidden" value="<?php echo $user->user_password; ?>" required>
                  <button class="btn btn-info btn-fill pull-right" name="submit" type="submit" value="submit"> Save</button>
                  <div class="clearfix"></div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>