<div class="container-fluid">
   <div class="header">
      <h3 class="title">Edit Post</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Title 
                        <star>*</star>
                        <?php echo form_error('post_title','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="post_title" type="text" value="<?php echo $post->post_title; ?>" placeholder="Enter title" autocomplete="off" required>

                     <input type="hidden" name="id" id="post-id" value="<?php echo $post->post_id; ?>">
                  </div>
                  <div class="form-group">
                     <label>
                        Subtitle 
                        <star>*</star>
                        <?php echo form_error('post_subtitle','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="post_subtitle" value="<?php echo $post->post_subtitle; ?>" type="text" placeholder="Enter subtitle" autocomplete="off">
                  </div>
                  <div class="form-group">
                     <label>
                        Content 
                        <star>*</star>
                        <?php echo form_error('post_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="post_content" id="post-content" rows="4" placeholder="Enter content"><?php echo $post->post_content; ?></textarea>
                  </div>
                  <div class="category">
                     <star>*</star>
                     Mandatory
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
         <div class="col-md-3">
            <div class="card">
               <div class="content">
                  <?php if($this->group_id != 3): ?>
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="post_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2" <?php echo isset($post->post_status) && $post->post_status == 2 ? 'selected="selected"' : '';?>>Draft</option>
                           <option value="1" <?php echo isset($post->post_status) && $post->post_status == 1 ? 'selected="selected"' : '';?>>Publish</option>
                        </select>
                     </div>
                  <?php else: ?>
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="post_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2" <?php echo isset($post->post_status) && $post->post_status == 2 ? 'selected="selected"' : '';?>>Draft</option>
                           <option value="3" <?php echo isset($post->post_status) && $post->post_status == 3 ? 'selected="selected"' : '';?>>For Review</option>
                        </select>
                     </div>
                  <?php endif; ?>

                  <?php
                  if (isset($post->post_publish_date)) {
                      $temp_date = explode(' ', $post->post_publish_date);
                      $tgl = $temp_date[0];

                      $temp_jam = explode(':', $temp_date[1]);
                      $jam = $temp_jam[0];
                      $menit = $temp_jam[1];

                      if ($temp_date[0] == '0000-00-00') {
                          $tgl = '';
                      }
                  }
                  ?>

                  <div class="form-group">
                        <label class="control-label">Publish Date <?php echo form_error('post_publish_date','<span style="color:red;">','</span>'); ?></label>
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input class="form-control datetimepicker" name="post_publish_date" id="publishDate" type="text" placeholder="Select Date" value="<?php echo $tgl; ?>" autocomplete="off" required>
                        </div>
                        <div id="<?php echo $tgl == "" ? 'ket_waktu' : 'ket' ?>">
                            <label class="control-label">Jam</label>
                            <select name="jam" class="form-control">
                                <?php for ($i = 0; $i <= 23; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>" <?php echo $jam == $i ? 'selected="selected"' : '' ?>><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                            <label class="control-label">Menit</label>
                            <select name="menit" class="form-control">
                                <?php for ($i = 0; $i <= 59; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>" <?php echo $menit == $i ? 'selected="selected"' : '' ?>><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                  <div class="form-group" align="right">
                     <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
         <div class="col-md-3">

            <div class="card">
               <div class="content">
                  <!-- <div class="form-group">
                     <label>Headline</label><br/>
                     <input type="checkbox" data-toggle="switch" name="post_headline" disabled>
                  </div> -->
                  <div class="form-group">
                     <label>
                        Category 
                        <star>*</star>
                        <?php echo form_error('post_category_id','<span style="color:red;">','</span>'); ?>
                     </label>
                     <select class="form-control selectpicker" data-live-search="true" name="post_category_id" id="select" required>
                        <?php foreach($category as $item): ?>
                             <option value="<?php echo $item->page_id; ?>" <?php echo isset($post->post_category_id) && $post->post_category_id == $item->page_id ? 'selected="selected"' : '';?>><?php echo $item->page_name; ?></option>
                         <?php endforeach; ?>
                     </select>
                  </div>
                  <div class="form-group">
                     <label>
                        Tags 
                        <star>*</star>
                        <?php echo form_error('post_taxonomy','<span style="color:red;">','</span>'); ?>
                     </label>
                     <br/>
                     <input class="form-control tagfield" name="post_taxonomy" type="text" placeholder="Enter title" data-role="tagsinput" value="<?php echo $tags; ?>">
                  </div>
                  <div class="form-group">
                     <label>No Thumbnail</label>
                     <input type="checkbox" <?php if($post->post_is_thumbnail == 0): ?> checked <?php endif; ?> name="post_is_thumbnail" id="is_thumbnail" value="1">
                  </div>
                  <!-- <div class="form-group">
                     <label class="control-label">Berita Umum</label>
                     <input type="checkbox" <?php if($post->post_is_editor_choice == 1): ?> checked <?php endif; ?> name="post_is_editor_choice" value="1">
                 </div> -->
                  <div class="form-group">
                     <label>Featured Image</label><br/>
                     <?php
                          if($post->post_thumbnail){
                              $media = $this->m_media->get_media_id($post->post_thumbnail);
                                          $thumbpath = pathinfo($media->media_name);
                              $image = base_url().'asset_admin/assets/uploads/media/image/'.$thumbpath['filename'].'_small.'.$thumbpath['extension'];
                              echo '<img src="'.$image.'" width="200" alt=""/>';
                          } else {
                              $image = '';
                              echo '<span>Foto tidak ditemukan!</span>';
                          }
                      ?>
                  </div>
                  <div class="form-group">
                     <label>
                        File 
                        <star>*</star>
                     </label>
                     <input class="form-control" name="userfile" id="userfile" type="file" <?php if($post->post_is_thumbnail == 0): ?> disabled="disabled" <?php endif; ?>>
                      <span style="color:#ab3737;"> * Maximum size 2MB </span>
                       <br>
                       <span style="color:#ab3737;"> * Image must high resolution </span>
                       <br>
                       <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                  </div>
               </div>
            </div> <!-- end card -->
         </div> <!--  end col-md-6  -->
   <!--  end col-md-6  -->
   </div>
   </form>
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<?php $this->load->view('backend/post/ckeditor.php'); ?>

<script type="text/javascript">
    $(function () {
        $('.summernote').summernote({
            height: 250,
            minHeight: 250, // set minimum height of editor
            maxHeight: 500, // set maximum height of editor
            focus: true
        });

        /*$('form').on('submit', function (e) {
         e.preventDefault();
         alert($('.summernote').summernote('code'));
         alert($('.summernote').val());
         });*/
    });

    $('#publishDate').datetimepicker({
      format: "YYYY-MM-DD",
    });

    jQuery(document).ready(function(){
        jQuery("#ket_waktu").css("display","none");
        jQuery("#publishDate").change(function(){
            jQuery("#ket_waktu").css("display","inline");
        });
    });

    jQuery(document).ready(function () {
       $("#is_thumbnail").click(function () {
          $('#userfile').attr("disabled", $(this).is(":checked"));
       });
    });
</script>