<script type="text/javascript">
	$(function() {
            var postid = $('#post-id').val();

            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.height = 300;
            CKEDITOR.config.filebrowserBrowseUrl = "<?php echo base_url().'backend/media/media/mediabrowse'; ?>?id=" + postid + "&show=current";
            CKEDITOR.config.filebrowserUploadUrl = "<?php echo base_url().'backend/media/media/mediaupload'; ?>?id=" + postid;
            CKEDITOR.config.filebrowserWindowWidth  = 900;
            CKEDITOR.config.filebrowserWindowHeight = 500;
            CKEDITOR.config.extraPlugins = 'oembed,justify';
            CKEDITOR.replace('post-content', CKEDITOR.config);
        });
</script>