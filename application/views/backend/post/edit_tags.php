<div class="container-fluid">
   <div class="header">
      <h3 class="title">Edit Tags</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                            <label class="control-label">Name</label>
                            <input name="taxonomy_name" class="form-control" type="text" placeholder="Enter name" value="<?php echo $tag->taxonomy_name; ?>" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <textarea name="taxonomy_description" class="form-control" rows="4" placeholder="Enter description"><?php echo $tag->taxonomy_description; ?></textarea>
                        </div>
               </div>
               <div class="content">
                    <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>