<input type="text" name="url" id="url" readonly hidden value="#">
   <div class="container-fluid">
      <div class="header">
         <h3 class="title">All Posts 
         	<?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
	        <?php if(count($add_action) > 0): ?>
	        <?php $button_add = $this->m_action->get_id_action(2); ?>
          <div class="pull-right">
         	<a href="<?php echo base_url(); ?>backend/posts/posts/add" class="btn btn-info btn-fill"><?php echo $button_add->action_alias; ?> New</a>
          </div>
         	<?php endif; ?>
         </h3>
      </div>
      <br>
	  <br>
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="content">
                  <ul role="tablist" class="nav nav-tabs">
                     <li role="presentation" class="<?php if($base == "Postall"): ?> active <?php endif; ?>">
                        <a href="<?php echo base_url(); ?>backend/posts/posts/all" class="text-danger">All <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_all; ?></button></a>
                     </li>
                     <?php if($this->group_id != 3 && $this->group_id != 4): ?>
                     <li role="tab" class="<?php if($base == "Postmain"): ?> active <?php endif; ?>">
                        <a href="<?php echo base_url(); ?>backend/posts/posts/main" class="text-danger">Mine <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_main; ?></button></a>
                     </li>
                 	<?php endif; ?>
                     <li role="tab" class="<?php if($base == "Post"): ?> active <?php endif; ?>">
                        <a href="<?php echo base_url(); ?>backend/posts/posts" class="text-danger">Published <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total; ?></button></a>
                     </li>
                     <li role="tab" class="<?php if($base == "Postdraft"): ?> active <?php endif; ?>">
                        <a href="<?php echo base_url(); ?>backend/posts/posts/draft" class="text-danger">Draft <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_draft; ?></button></a>
                     </li>
                     <?php if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3): ?>
                     <li role="tab" class="<?php if($base == "Postpending"): ?> active <?php endif; ?>">
                        <a href="<?php echo base_url(); ?>backend/posts/posts/pending" class="text-danger">Pending <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_pending; ?></button></a>
                     </li>
                     <?php endif; ?>
                  </ul>
                  <div class="tab-content">
                     <div id="all-data" class="tab-pane active">
                          <form action="">
			                     <div class="pull-left search">
	                           	<input class="form-control" type="text" name="title" placeholder="Search" autocomplete="off">
	                         </div>
			                     </form>
                           <div class="columns columns-right pull-right">
                              <a href="<?php echo current_url(); ?>" class="btn btn-default" title="Refresh"><i class="glyphicon fa fa-refresh"></i></a>

                              <!-- <div class="keep-open btn-group" title="Columns">
                                 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="glyphicon fa fa-columns"></i> <span class="caret"></span></button>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><label><input type="checkbox" data-field="no" value="0" checked="checked"> #</label></li>
                                    <li><label><input type="checkbox" data-field="name" value="1" checked="checked"> Name</label></li>
                                    <li><label><input type="checkbox" data-field="title" value="2" checked="checked"> Title</label></li>
                                    <li><label><input type="checkbox" data-field="status" value="3" checked="checked"> Status</label></li>
                                    <li><label><input type="checkbox" data-field="creatdat" value="4" checked="checked"> Created at</label></li>
                                    <li><label><input type="checkbox" data-field="createdby" value="5" checked="checked"> Created by</label></li>
                                 </ul>
                              </div> -->
                           </div>
                            <div class="content table-responsive table-full-width">
                           <table class="table table-hover table-striped">
                              <thead>
                                 <th>Thumb</th>
                                 <th>Title</th>
                                 <th>Author</th>
                                 <th>Date</th>
                                 <th>Status</th>
                                 <th>Actions</th>
                              </thead>
                              <tbody>

                              	<?php if (count($post) > 0) :?>
                            		<?php foreach($post as $item): ?>

                            			<tr>
                            				<td>
                            					<?php
			                                    if($item['post_is_thumbnail'] == 1){
			                                        if($item['post_thumbnail']){
			                                            $media = $this->m_media->get_media_id($item['post_thumbnail']);
			                                            $thumbpath = pathinfo($media->media_name);
			                                            $image = base_url().'asset_admin/assets/uploads/media/image/'.$thumbpath['filename'].'_small.'.$thumbpath['extension'];
			                                        } else {
			                                            $image = 'https://via.placeholder.com/300x200';
			                                        }
			                                    } else {
			                                        $image = '';
			                                    }
			                                    ?>

			                                    <div class="img-container">
	                                             <img src="<?php echo $image; ?>" height="50" width="50">
	                                           </div>
                            				</td>
                            				<td><?php echo $item['post_title']; ?></td>
                            				<td>
			                                    <?php
			                                        $user = $this->m_user->get_id_user($item['post_created_by']);
			                                        echo $user->user_username;
			                                    ?>
			                                </td>
			                                <td><?php echo $item['post_publish_date']; ?></td>
			                                <td>
			                                    <?php
			                                    if($item['post_status'] == 1){
			                                        echo 'Publish';
			                                    } elseif ($item['post_status'] == 2) {
			                                        echo 'Draft';
			                                    } elseif ($item['post_status'] == 3) {
			                                        echo 'For Review';
			                                    }
			                                    ?>
			                                </td>
			                                <td class="td-actions">
			                                <?php $view_action = $this->m_access->get_access_group_action($this->group_id, 5, $this->access_id); ?>
                                       		<?php if(count($view_action) > 0): ?>
                                            <?php $button_views = $this->m_action->get_id_action(5); ?>
                                              <?php
	                                                if($item['post_status'] == 2 || $item['post_publish_date'] >= date('Y-m-d H:i:s')){
	                                                    $link = base_url().'post/preview/'.$item['post_slug'];
	                                                } else {
	                                                    $link = base_url().'post/'.$item['post_slug'];
	                                                }
	                                            ?>
	                                          <a href="<?php echo $link; ?>" >
	                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_views->action_alias; ?>" class="btn btn-warning btn-simple btn-icon "><i class="fa fa-eye"></i>
	                                             </button>
	                                          </a>
	                                        <?php endif; ?>

	                                        <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                                        	<?php if(count($edit_action) > 0): ?>
                                        		<?php $button_edit = $this->m_action->get_id_action(3); ?>
                                            	<?php if($this->group_id == 3): ?>
                                            		<?php if($item['post_status'] == 2): ?>
                                            			<a href="<?php echo base_url(); ?>backend/posts/posts/edit/<?php echo $item['post_id']; ?>" >
			                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
			                                             </button>
			                                          </a>
                                            		<?php endif; ?>
                                            	<?php else: ?>
                                            		<a href="<?php echo base_url(); ?>backend/posts/posts/edit/<?php echo $item['post_id']; ?>" >
			                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
			                                             </button>
			                                          </a>
                                            	<?php endif; ?>
	                                        <?php endif; ?>

	                                        <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                        	<?php if(count($delete_action) > 0): ?>
                                        	  <?php $button_delete = $this->m_action->get_id_action(4); ?>
	                                          <a href="<?php echo base_url(); ?>backend/posts/posts/delete/<?php echo $item['post_id']; ?>" id="delete" >
	                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
	                                             </button>
	                                          </a>
	                                         <?php endif; ?>
	                                       </td>
                            			</tr>

                            		<?php endforeach; ?>
                            	<?php endif; ?>
                                 <!--  <tr>
                                    <td colspan="8" align="center"><i>No Record Found</i></td>
                                 </tr> -->
                                 <tr>
                                    <td colspan="8" align="center">
                                       <?php echo $paging; ?>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--  end card  -->
         </div>
         <!--  end col-md-12  -->
      </div>
   </div>