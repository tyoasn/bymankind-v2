<?php if($tags): ?>
    <?php $arr = array(); ?>
    <?php foreach($tags as $tag): ?>
        <?php $arr[] = array('id' => $tag->taxonomy_id, 'name' => $tag->taxonomy_name); ?>
    <?php endforeach; ?>
    <?php echo json_encode($arr,true); ?>
<?php endif; ?>