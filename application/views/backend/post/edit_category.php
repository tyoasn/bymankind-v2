<div class="container-fluid">
   <div class="header">
      <h3 class="title">Edit Category</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                            <label class="control-label">Name</label>
                            <input name="page_name" class="form-control" type="text" placeholder="Enter name" value="<?php echo $category->page_name; ?>" autocomplete="off" required>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Parent</label>
                            <select class="form-control selectpicker" data-live-search="true" name="page_parent_id" name="page_parent_id" id="select" required>
                                <option value="0">None</option>
                                <?php foreach($category_parent as $item): ?>
                                    <option value="<?php echo $item->page_id; ?>" <?php echo isset($category->page_parent_id) && $category->page_parent_id == $item->page_id ? 'selected="selected"' : '';?>><?php echo $item->page_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <textarea name="page_description" class="form-control" rows="4" placeholder="Enter description"><?php echo $category->page_description; ?></textarea>
                        </div>
               </div>
               <div class="content">
                    <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>