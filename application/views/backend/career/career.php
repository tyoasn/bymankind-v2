<input type="text" name="url" id="url" readonly hidden value="#">
<div class="container-fluid">
   <div class="header">
      <h3 class="title">All Careers
         <?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
         <?php if(count($add_action) > 0): ?>
         <?php $button_add = $this->m_action->get_id_action(2); ?>
         <div class="pull-right">
         <a href="<?php echo base_url(); ?>backend/career/career/add" class="btn btn-info btn-fill"><?php echo $button_add->action_alias; ?> New</a>
         </div>
         <?php endif; ?></h3>
   </div>
   <br>
   <br>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <div class="content">
               <ul role="tablist" class="nav nav-tabs">
                  <li role="presentation" class="<?php if($base == "Careerall"): ?> active <?php endif; ?>">
                     <a href="<?php echo base_url(); ?>backend/career/career/all" class="text-danger">All <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_all; ?></button></a>
                  </li>
                  <?php if($this->group_id != 3 && $this->group_id != 4): ?>
                  <li role="tab" class="<?php if($base == "Careermain"): ?> active <?php endif; ?>">
                     <a href="<?php echo base_url(); ?>backend/career/career/main" class="text-danger">Mine <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_main; ?></button></a>
                  </li>
                  <?php endif; ?>
                  <li role="tab" class="<?php if($base == "Career"): ?> active <?php endif; ?>">
                     <a href="<?php echo base_url(); ?>backend/career/career" class="text-danger">Published <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total; ?></button></a>
                  </li>
                  <li role="tab" class="<?php if($base == "Careerdraft"): ?> active <?php endif; ?>">
                     <a href="<?php echo base_url(); ?>backend/career/career/draft" class="text-danger">Draft <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_draft; ?></button></a>
                  </li>
                  <?php if($this->group_id == 1 || $this->group_id == 2 || $this->group_id == 3): ?>
                  <li role="tab" class="<?php if($base == "Careerpending"): ?> active <?php endif; ?>">
                     <a href="<?php echo base_url(); ?>backend/career/career/pending" class="text-danger">Pending <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_pending; ?></button></a>
                  </li>
                  <?php endif; ?>
               </ul>
               <div class="tab-content">
                  <div id="all-data" class="tab-pane active">
                    <form action="">
                              <div class="pull-left search">
                                 <input class="form-control" type="text" name="title" placeholder="Search" autocomplete="off">
                              </div>
                           </form>
                           <div class="columns columns-right pull-right">
                              <a href="<?php echo current_url(); ?>" class="btn btn-default" title="Refresh"><i class="glyphicon fa fa-refresh"></i></a>
                              <!-- <div class="keep-open btn-group" title="Columns">
                                 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="glyphicon fa fa-columns"></i> <span class="caret"></span></button>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><label><input type="checkbox" data-field="no" value="0" checked="checked"> #</label></li>
                                    <li><label><input type="checkbox" data-field="name" value="1" checked="checked"> Name</label></li>
                                    <li><label><input type="checkbox" data-field="title" value="2" checked="checked"> Title</label></li>
                                    <li><label><input type="checkbox" data-field="status" value="3" checked="checked"> Status</label></li>
                                    <li><label><input type="checkbox" data-field="creatdat" value="4" checked="checked"> Created at</label></li>
                                    <li><label><input type="checkbox" data-field="createdby" value="5" checked="checked"> Created by</label></li>
                                 </ul>
                                 </div> -->
                           </div>
                     <div class="content table-responsive table-full-width">
                        <table class="table table-hover table-striped">
                           <thead>
                              <th>Title</th>
                              <th>Status</th>
                              <th>Link</th>
                              <th>Actions</th>
                           </thead>
                           <tbody>
                              <?php if (count($career) > 0) :?>
                              <?php foreach($career as $item): ?>
                              <tr>
                                 <td><?php echo $item['career_title']; ?></td>
                                 <td>
                                    <?php
                                       if($item['career_status'] == 1){
                                           echo 'Published';
                                       } elseif ($item['career_status'] == 2) {
                                           echo 'Draft';
                                       } elseif ($item['career_status'] == 3) {
                                           echo 'For Review';
                                       }
                                       ?>
                                 </td>
                                 <td>
                                    <?php echo $item['career_link']; ?>
                                 </td>
                                 <td class="td-actions">
                                    <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                                    <?php if(count($edit_action) > 0): ?>
                                    <?php $button_edit = $this->m_action->get_id_action(3); ?>
                                    <?php if($this->group_id == 3): ?>
                                    <?php if($item['career_status'] == 2): ?>
                                    <a href="<?php echo base_url(); ?>backend/career/career/edit/<?php echo $item['career_id']; ?>" >
                                    <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
                                    </button>
                                    </a>
                                    <?php endif; ?>
                                    <?php else: ?>
                                    <a href="<?php echo base_url(); ?>backend/career/career/edit/<?php echo $item['career_id']; ?>" >
                                    <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
                                    </button>
                                    </a>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                    <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                       <?php if(count($delete_action) > 0): ?>
                                         <?php $button_delete = $this->m_action->get_id_action(4); ?>
                                         <a href="<?php echo base_url(); ?>backend/career/career/delete/<?php echo $item['career_id']; ?>" id="delete" >
                                            <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
                                            </button>
                                         </a>
                                        <?php endif; ?>
                                 </td>
                              </tr>
                              <?php endforeach; ?>
                              <?php endif; ?>
                              <!--  <tr>
                                 <td colspan="8" align="center"><i>No Record Found</i></td>
                                 </tr> -->
                              <tr>
                                 <td colspan="8" align="center">
                                    <?php echo $paging; ?>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--  end card  -->
      </div>
      <!--  end col-md-12  -->
   </div>
</div>