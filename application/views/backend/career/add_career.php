<div class="container-fluid">
   <div class="header">
      <h3 class="title">Add New Career</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Title 
                        <star>*</star>
                        <?php echo form_error('career_title','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="career_title" type="text" placeholder="Enter title" autocomplete="off" required>
                     <input type="hidden" name="id" id="content-id" value="0">
                  </div>
                  <div class="form-group">
                     <label>
                        Content 
                        <star>*</star>
                        <?php echo form_error('career_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="career_content" id="post-content" rows="4" placeholder="Enter content"></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Link
                        <star>*</star>
                        <?php echo form_error('career_link','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="career_link" type="text" placeholder="Enter title" autocomplete="off" required>
                     <input type="hidden" name="id" id="content-id" value="0">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
         <div class="col-md-3">
            <div class="card">
               <div class="content">
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="career_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2">Draft</option>
                           <option value="1">Publish</option>
                        </select>
                     </div>
                  <div class="form-group" align="right">
                     <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<?php $this->load->view('backend/content/ckeditor.php'); ?>
<script type="text/javascript">

  $(function () {
        $('.summernote').summernote({
            height: 250,
            minHeight: 250, // set minimum height of editor
            maxHeight: 500, // set maximum height of editor
            focus: true
        });

        /*$('form').on('submit', function (e) {
         e.preventDefault();
         alert($('.summernote').summernote('code'));
         alert($('.summernote').val());
         });*/
    });

</script>
