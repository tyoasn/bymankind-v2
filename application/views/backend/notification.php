<?php if($this->session->flashdata('success')): ?>
	<script type="text/javascript">
		$.notify({
			title: "<strong>Congratulations</strong> ",
			message: "<?php echo $this->session->flashdata('success'); ?>"
		},{
			type: 'success'
		});
	</script>
<?php endif; ?>

<?php if($this->session->flashdata('error')): ?>
	<script type="text/javascript">
		$.notify({
			title: "<strong>Sorry</strong> ",
			message: "<?php echo $this->session->flashdata('error'); ?>"
		},{
			type: 'danger'
		});
	</script>
<?php endif; ?>