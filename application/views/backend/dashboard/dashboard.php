<div class="container-fluid">
  <div class="card card-plain">
     <div class="row">
        <div class="col-md-4">
           <div class="card">
              <div class="content text-center">
                 <h2><i class="pe-7s-note" style="color: #ab3737;"></i></h2>
                 <h3><?php echo $contents; ?> Content(s)</h3>
              </div>
           </div>
        </div>
        <div class="col-md-4">
           <div class="card">
              <div class="content text-center">
                 <h2><i class="pe-7s-photo" style="color: #ab3737;"></i></h2>
                 <h3><?php echo $media; ?> Media</h3>
              </div>
           </div>
        </div>
        <div class="col-md-4">
           <div class="card">
              <div class="content text-center">
                 <h2><i class="pe-7s-network" style="color: #ab3737;"></i></h2>
                 <h3><?php echo $division; ?> Division(s)</h3>
              </div>
           </div>
        </div>
        <div class="col-md-4">
           <div class="card">
              <div class="content text-center">
                 <h2><i class="pe-7s-user" style="color: #ab3737;"></i></h2>
                 <h3><?php echo $members; ?> Member(s)</h3>
              </div>
           </div>
        </div>
        <div class="col-md-4">
           <div class="card">
              <div class="content text-center">
                 <h2><i class="pe-7s-id" style="color: #ab3737;"></i></h2>
                 <h3><?php echo $career; ?> Career(s)</h3>
              </div>
           </div>
        </div>
        <div class="col-md-4">
           <div class="card">
              <div class="content text-center">
                 <h2><i class="pe-7s-users" style="color: #ab3737;"></i></h2>
                 <h3><?php echo $users; ?> User(s)</h3>
              </div>
           </div>
        </div>
     </div>
  </div>
</div>