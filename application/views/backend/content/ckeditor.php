<script type="text/javascript">
	$(function() {
            var contentid = $('#content-id').val();

            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.height = 300;
            CKEDITOR.config.filebrowserBrowseUrl = "<?php echo base_url().'backend/media/media/mediabrowseContent'; ?>?id=" + contentid + "&show=current";
            CKEDITOR.config.filebrowserUploadUrl = "<?php echo base_url().'backend/media/media/mediauploadContent'; ?>?id=" + contentid;
            CKEDITOR.config.filebrowserWindowWidth  = 900;
            CKEDITOR.config.filebrowserWindowHeight = 500;
            CKEDITOR.config.extraPlugins = 'oembed,justify';
            CKEDITOR.replace('post-content', CKEDITOR.config);
        });
</script>