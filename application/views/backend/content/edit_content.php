<div class="container-fluid">
   <div class="header">
      <h3 class="title">Edit Content</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Title 
                        <star>*</star>
                        <?php echo form_error('post_title','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="post_title" type="text" value="<?php echo $content->post_title; ?>" placeholder="Enter title" autocomplete="off">
                     <input type="hidden" name="id" id="post-id" value="<?php echo $content->post_id; ?>">
                  </div>
                  <div class="form-group">
                     <label>
                        Content 
                        <star>*</star>
                        <?php echo form_error('post_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="post_content" id="post-content" rows="4" placeholder="Enter content"><?php echo $content->post_content; ?></textarea>
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
         <div class="col-md-3">
            <div class="card">
               <div class="content">
                  <?php if($this->group_id != 3): ?>
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="post_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2" <?php echo isset($content->post_status) && $content->post_status == 2 ? 'selected="selected"' : '';?>>Draft</option>
                           <option value="1" <?php echo isset($content->post_status) && $content->post_status == 1 ? 'selected="selected"' : '';?>>Publish</option>
                        </select>
                     </div>
                  <?php else: ?>
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="post_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2" <?php echo isset($content->content_status) && $post->post_status == 2 ? 'selected="selected"' : '';?>>Draft</option>
                           <option value="3" <?php echo isset($content->post_status) && $post->post_status == 3 ? 'selected="selected"' : '';?>>For Review</option>
                        </select>
                     </div>
                  <?php endif; ?>

                  <?php
                  if (isset($content->post_publish_date)) {
                      $temp_date = explode(' ', $content->post_publish_date);
                      $tgl = $temp_date[0];

                      $temp_jam = explode(':', $temp_date[1]);
                      $jam = $temp_jam[0];
                      $menit = $temp_jam[1];

                      if ($temp_date[0] == '0000-00-00') {
                          $tgl = '';
                      }
                  }
                  ?>

                  <div class="form-group">
                        <label class="control-label">Publish Date <?php echo form_error('content_publish_date','<span style="color:red;">','</span>'); ?></label>
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input class="form-control datetimepicker" name="post_publish_date" id="publishDate" type="text" placeholder="Select Date" value="<?php echo $tgl; ?>" autocomplete="off" required>
                        </div>
                        <div id="<?php echo $tgl == "" ? 'ket_waktu' : 'ket' ?>">
                            <label class="control-label">Jam</label>
                            <select name="jam" class="form-control">
                                <?php for ($i = 0; $i <= 23; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>" <?php echo $jam == $i ? 'selected="selected"' : '' ?>><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                            <label class="control-label">Menit</label>
                            <select name="menit" class="form-control">
                                <?php for ($i = 0; $i <= 59; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>" <?php echo $menit == $i ? 'selected="selected"' : '' ?>><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                  <div class="form-group" align="right">
                     <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
   <!--  end col-md-6  -->
   </div>
   </form>
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<?php $this->load->view('backend/content/ckeditor.php'); ?>

<script type="text/javascript">

  $(function () {
        $('.summernote').summernote({
            height: 250,
            minHeight: 250, // set minimum height of editor
            maxHeight: 500, // set maximum height of editor
            focus: true
        });

        /*$('form').on('submit', function (e) {
         e.preventDefault();
         alert($('.summernote').summernote('code'));
         alert($('.summernote').val());
         });*/
    });

    $('#publishDate').datetimepicker({
      format: "YYYY-MM-DD",
    });

    jQuery(document).ready(function(){
        jQuery("#ket_waktu").css("display","none");
        jQuery("#publishDate").change(function(){
            jQuery("#ket_waktu").css("display","inline");
        });
    });

</script>