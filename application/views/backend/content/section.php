<input type="text" name="url" id="url" value="http://satset.id" readonly hidden>
   <div class="container-fluid">
      <div class="header">
         <h3 class="title">Section</h3>
      </div>
      <br/>
      <div class="row">
         <?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
          <?php if(count($add_action) > 0): ?>
          <?php $button_add = $this->m_action->get_id_action(2); ?>
         <div class="col-md-4">
            <div class="card">
               <div class="header">Add Section</div>
               <div class="content">
                  <form method="POST" action="" id="addcategory">
                     <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" name="page_name" type="text" placeholder="Enter Name" autocomplete="off" required>
                     </div>
                     <div class="form-group">
                        <label>Parent</label>
                        <select class="form-control selectpicker" data-live-search="true" name="page_parent_id" id="select" required>
                            <option value="0">None</option>
                            <?php foreach($sections as $item): ?>
                                <option value="<?php echo $item->page_id; ?>"><?php echo $item->page_name; ?></option>
                            <?php endforeach; ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="page_description" rows="4" placeholder="Enter Description"></textarea>
                     </div>
                     <div class="form-group" align="right">
                        <input type="submit" name="submit" value="Save" class="btn btn-fill btn-info">
                     </div>
                  </form>
               </div>
            </div>
            <!-- end card -->
         </div>
         <?php endif; ?>
         <!--  end col-md-3  -->
         <div class="col-md-8">
            <div class="card">
               <div class="toolbar">
                  <!--        Here you can write extra buttons/actions for the toolbar              -->
               </div>
               <div class="content">
               </div>
               <div class="tab-content">
                  <div id="all-data" class="tab-pane active">
                     <table class="table table-hover table-bordered table-gfg" id="sampleTable">
                        <thead>
                           <th style="display: none;">Id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Slug</th>
                            <th>Parent</th>
                            <th>Action</th>
                        </thead>
                        <tbody>

                           <?php if (count($section) > 0) :?>
                           <?php foreach($section as $item): ?>

                           <tr>
                               <td style="display: none;"><?php echo $item['page_id'] ?></td>
                                <td><?php echo $item['page_name'] ?></td>
                                <td><?php echo $item['page_description'] ?></td>
                                <td><?php echo $item['page_slug'] ?></td>
                                <td>
                                    <?php
                                        if($item['page_parent_id'] == 0){
                                            echo 'None';
                                        } else {
                                            echo $this->m_section->get_id_category($item['page_parent_id'])->page_name;
                                        }
                                    ?>
                                </td>
                               <td class="td-actions">
                                 <?php $view_action = $this->m_access->get_access_group_action($this->group_id, 5, $this->access_id); ?>
                                  <?php if(count($view_action) > 0): ?>
                                          <?php $button_views = $this->m_action->get_id_action(5); ?>
                                 <a href="<?php echo base_url().'section/'.$item['page_slug']; ?>" target="_blank" >
                                    <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_views->action_alias; ?>" class="btn btn-warning btn-simple btn-icon "><i class="fa fa-eye"></i>
                                    </button>
                                 </a>
                                 <?php endif; ?>

                                 <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                                        <?php if(count($edit_action) > 0): ?>
                                            <?php $button_edit = $this->m_action->get_id_action(3); ?>
                                 <a href="<?php echo base_url(); ?>backend/content/section/edit/<?php echo $item['page_id']; ?>" >
                                    <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
                                    </button>
                                 </a>
                                 <?php endif; ?>

                                 <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                        <?php if(count($delete_action) > 0): ?>
                                            <?php $button_delete = $this->m_action->get_id_action(4); ?>
                                 <a href="<?php echo base_url(); ?>backend/content/section/delete/<?php echo $item['page_id']; ?>" id="delete" >
                                    <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
                                    </button>
                                 </a>
                                 <?php endif; ?>
                              </td>
                           </tr>
                           <?php endforeach; ?>
                           <?php endif; ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <!--  end card  -->
         </div>
         <!-- end col-md-9 -->
      </div>
      <!-- end row -->
   </div>

<script src="<?php echo base_url(); ?>asset_admin/js/plugins/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$('#sampleTable').DataTable({
    "order": [[ 0, "desc" ]],
    aLengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
    ],
    //iDisplayLength: -1
});
</script>