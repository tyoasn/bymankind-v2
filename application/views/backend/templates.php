<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <link rel="icon" type="image/png" href="<?php echo base_url(); ?>asset_admin/img/logobymankind.png">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <title>Mankind Dashboard</title>
      <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
      <meta name="viewport" content="width=device-width" />
      <!-- Javascripts-->
      <script src="<?php echo base_url(); ?>asset_admin/js/jquery-2.1.4.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

      <!-- Bootstrap core CSS     -->
      <link href="<?php echo base_url(); ?>asset_admin/css/bootstrap.min.css" rel="stylesheet" />
      <!--  Light Bootstrap Dashboard core CSS    -->
      <link href="<?php echo base_url(); ?>asset_admin/css/main.css" rel="stylesheet"/>
      <!--  CSS for Demo Purpose, don't include it in your project     -->
      <link href="<?php echo base_url(); ?>asset_admin/css/demo.css" rel="stylesheet" />
      <!--     Fonts and icons     -->
      <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
      <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
      <link href="<?php echo base_url(); ?>asset_admin/css/pe-icon-7-stroke.css" rel="stylesheet" />
   </head>
   <body>
        <div class="wrapper">
         <?php $this->load->view('backend/sidebar'); ?>

         <div class="main-panel">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-danger btn-fill btn-round btn-icon">
                            <i class="fa fa-ellipsis-v visible-on-sidebar-regular"></i>
                            <i class="fa fa-navicon visible-on-sidebar-mini"></i>
                        </button>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>" class="btn btn-info btn-fill" target="_blank">Visit Website <i class="fa fa-external-link"></i></a>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- <a class="navbar-brand" href="#">Dashboard</a> -->
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- section page -->
            <div class="content">
               <?php $this->load->view($mainpage); ?>
            </div>
            <!-- Footer Section -->
            <footer class="footer">
               <div class="container-fluid">
                  <nav class="pull-left">
                     <ul>
                        <li>
                           <a href="#">
                           </a>
                        </li>
                        <li>
                           <a href="#">
                           </a>
                        </li>
                        <li>
                           <a href="#">
                           </a>
                        </li>
                        <li>
                           <a href="#">
                           </a>
                        </li>
                     </ul>
                  </nav>
                  <div class="copyright-footer">
                     <p>&copy; 2019 <a href="https://www.bymankind.com">Mankind</a></p>
                  </div>
               </div>
            </footer>
            <!-- Footer Section -->
         </div>
      </div>
   </body>
   <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
   <!-- <script src="<?php echo base_url(); ?>asset_admin/js/jquery.min.js" type="text/javascript"></script> -->
   <script src="<?php echo base_url(); ?>asset_admin/js/jquery-ui.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap.min.js" type="text/javascript"></script>
   <!--  Forms Validations Plugin -->
   <script src="<?php echo base_url(); ?>asset_admin/js/jquery.validate.min.js"></script>
   
   <!--  Select Picker Plugin -->
   <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-selectpicker.js"></script>
   <!--  Checkbox, Radio, Switch and Tags Input Plugins -->
   <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-checkbox-radio-switch-tags.js"></script>
   <!--  Charts Plugin -->
   <script src="<?php echo base_url(); ?>asset_admin/js/chartist.min.js"></script>
   <!--  Notifications Plugin    -->
   <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-notify.js"></script>
   <!-- Sweet Alert 2 plugin -->
   <script src="<?php echo base_url(); ?>asset_admin/js/sweetalert2.js"></script>
   <!-- Vector Map plugin -->
   <script src="<?php echo base_url(); ?>asset_admin/js/jquery-jvectormap.js"></script>
   <!--  Google Maps Plugin    -->
   <!-- Wizard Plugin    -->
   <script src="<?php echo base_url(); ?>asset_admin/js/jquery.bootstrap.wizard.min.js"></script>
   <!--  Bootstrap Table Plugin    -->
   <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-table.js"></script>
   <!--  Plugin for DataTables.net  -->
   <script src="<?php echo base_url(); ?>asset_admin/js/jquery.datatables.js"></script>
   <!--  Full Calendar Plugin    -->
   <script src="<?php echo base_url(); ?>asset_admin/js/fullcalendar.min.js"></script>
   <!-- Light Bootstrap Dashboard Core javascript and methods -->
   <script src="<?php echo base_url(); ?>asset_admin/js/light-bootstrap-dashboard.js"></script>
   <!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
   <script src="<?php echo base_url(); ?>asset_admin/js/demo.js"></script>
   <!-- <script type="text/javascript">
      $(document).ready(function(){
      
            demo.initDashboardPageCharts();
            demo.initVectorMap();
      
            $.notify({
                icon: 'pe-7s-bell',
                message: "<b>Welcome"
      
             },{
                 type: 'success',
                 timer: 4000
             });
      
      });
   </script> -->
   <script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/typeahead.bundle.js"></script>
   <script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/bootstrap-tagsinput.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script>
        $('.selectpicker').selectpicker({
            dropupAuto: false
        });
    </script>
    <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-notify.min.js"></script>
    <?php $this->load->view('backend/notification'); ?>
    <script src="<?php echo base_url(); ?>asset_admin/js/plugins/bootbox.min.js" type="text/javascript"></script>
     <script>
      $(document).on("click", "#delete", function (e) {
          var link = $(this).attr("href"); // "get" the intended link in a var
          e.preventDefault();
          bootbox.confirm({
              title: "Delete?",
              message: "Are you sure you want to delete?",
              buttons: {
                  cancel: {
                      label: 'Cancel'
                  },
                  confirm: {
                      label: 'Confirm',
                      className: 'btn btn-info btn-fill btn-wd'
                  }
              },
              callback: function (result) {
                  //console.log('This was logged in the callback: ' + result);
                  if(result == true){
                      document.location.href = link;  // if result, "set" the document location
                  }
              }
          })
      });
    </script>
</html>