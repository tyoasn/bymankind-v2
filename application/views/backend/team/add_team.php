<div class="container-fluid">
   <div class="header">
      <h3 class="title">Add New Member</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Name 
                        <star>*</star>
                        <?php echo form_error('team_name','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="team_name" type="text" placeholder="Enter name" autocomplete="off" required>
                     <input type="hidden" name="id" id="content-id" value="0">
                  </div>
                  <div class="form-group">
                     <label>
                        Role
                        <star>*</star>
                        <?php echo form_error('team_role','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="team_role" type="text" placeholder="Enter role" autocomplete="off" required>
                     <input type="hidden" name="id" id="content-id" value="0">
                  </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Picture 1</label><br/>
                           <img src="http://via.placeholder.com/500x500" width="200">
                        </div>
                        <div class="form-group">
                           <label>
                              File 
                              <star>*</star>
                           </label>
                           <input type="file" class="form-control" class="form-control" name="userfile" id="userfile" required="true">
                            <span style="color:#ab3737;"> * Maximum size 2MB </span>
                             <br>
                             <span style="color:#ab3737;"> * Image must high resolution </span>
                             <br>
                             <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label>Picture 2</label><br/>
                           <img src="http://via.placeholder.com/500x500" width="200">
                        </div>
                        <div class="form-group">
                           <label>
                              File 
                              <star>*</star>
                           </label>
                           <input type="file" class="form-control" class="form-control" name="userfile2" id="userfile2" >
                            <span style="color:#ab3737;"> * Maximum size 2MB </span>
                             <br>
                             <span style="color:#ab3737;"> * Image must high resolution </span>
                             <br>
                             <span style="color:#ab3737;"> * Image type (JPG/PNG) </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
         <div class="col-md-3">
            <div class="card">
               <div class="content">
                     <div class="form-group">
                        <label>
                           Status 
                           <star>*</star>
                        </label>
                        <select name="team_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                           <option value="2">Draft</option>
                           <option value="1">Publish</option>
                        </select>
                     </div>
                     <div class="form-group">
                     <label>
                        Division 
                        <star>*</star>
                        <?php echo form_error('team_category_id','<span style="color:red;">','</span>'); ?>
                     </label>
                     <select class="form-control selectpicker" data-live-search="true" name="team_category_id" id="select" required>
                        <?php foreach($division as $item): ?>
                             <option value="<?php echo $item->division_id; ?>"><?php echo $item->division_name; ?></option>
                         <?php endforeach; ?>
                     </select>
                  </div>
                  <div class="form-group" align="right">
                     <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<?php $this->load->view('backend/content/ckeditor.php'); ?>
<script type="text/javascript">

  $(function () {
        $('.summernote').summernote({
            height: 250,
            minHeight: 250, // set minimum height of editor
            maxHeight: 500, // set maximum height of editor
            focus: true
        });

        /*$('form').on('submit', function (e) {
         e.preventDefault();
         alert($('.summernote').summernote('code'));
         alert($('.summernote').val());
         });*/
    });

</script>
