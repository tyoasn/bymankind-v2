<div class="container-fluid">

<div class="header">
 <h3 class="title">Access Menu
 </h3>
</div>
<br>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <!-- <div class="card-title-w-btn">
                <h3 class="title"></h3>
                <div>
                    <a class="btn btn-primary btn-flat" href="access-group-form.php" title="Add New"><i class="fa fa-lg fa-plus"></i> Add Group</a>
                    <a class="btn btn-info btn-flat" href="access-groups.php" title="Refresh"><i class="fa fa-lg fa-refresh"></i></a>
                </div>
            </div> -->
            <form action="<?php echo base_url(); ?>backend/group/group/access_post" method="POST">
                <input type="hidden" name="group_id" value="<?php echo $this->uri->segment(5); ?>">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                                <tr height="20px">
                                    <th width="30" rowspan="2" style="text-align: center">No</th>
                                    <th width="400" rowspan="2">Nama Module</th>
                                    <th colspan="6" style="text-align: center">Action</th>
                                </tr>
                                <tr style="font-size:11px; font-weight:normal;">
                                    <?php if (count($action) > 0) :?>
                                        <?php foreach($action as $value): ?>
                                            <?php
                                                if($value->action_name == 'Search'){
                                                    $id = 'select_all_index';
                                                } elseif($value->action_name == 'Add'){
                                                    $id = 'select_all_add';
                                                } elseif ($value->action_name == 'Edit') {
                                                    $id = 'select_all_edit';
                                                } elseif ($value->action_name == 'Delete') {
                                                    $id = 'select_all_delete';
                                                } elseif ($value->action_name == 'View') {
                                                    $id = 'select_all_view';
                                                } elseif ($value->action_name == 'Access') {
                                                    $id = 'select_all_access';
                                                }
                                            ?>
                                            <th width="10px" style="padding:5px;text-align:center;">
                                                <a href="#" class="south ttip_t" style="text-decoration:none;color:#000;" title="<?php echo $value->action_alias; ?>" ><?php echo $value->action_alias; ?></a><br />
                                                <input type="checkbox" id="<?php echo $id; ?>" value="1">
                                            </th>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tr> 
                            </thead>
                            <tbody>
                                <?php if (count($access) > 0) :?>
                                    <?php foreach($access as $item): ?>
                                        <tr>
                                            <td align="center"><?php echo $item->access_id; ?>.</td>
                                            <td><?php echo $item->access_alias; ?></td>
                                            <td align='center'>
                                                <?php
                                                    $check_access_action = $this->m_action->check_access_action($item->access_id,1);
                                                    if($check_access_action->num_rows() > 0):
                                                        $check_access_group_action =  $this->m_action->check_access_group_action($item->access_id,$group_id,1);
                                                ?>
                                                    <input type='checkbox' class='ttip_t chk_index' title='index' <?php echo isset($check_access_group_action) && $check_access_group_action->num_rows() > 0 ? 'checked="checked"' : '';?> name='acc_name[<?php echo $item->access_id; ?>][1]' value='1' style='cursor:pointer;' class='south'>
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </td>
                                            <td align='center'>
                                                <?php
                                                    $check_access_action = $this->m_action->check_access_action($item->access_id,2);
                                                    if($check_access_action->num_rows() > 0):
                                                        $check_access_group_action =  $this->m_action->check_access_group_action($item->access_id,$group_id,2);
                                                ?>
                                                    <input type="checkbox" class="ttip_t chk_add" title="add" <?php echo isset($check_access_group_action) && $check_access_group_action->num_rows() > 0 ? 'checked="checked"' : '';?> name="acc_name[<?php echo $item->access_id; ?>][2]" value="2" style="cursor:pointer;">
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </td>
                                            <td align='center'>
                                                <?php
                                                    $check_access_action = $this->m_action->check_access_action($item->access_id,3);
                                                    if($check_access_action->num_rows() > 0):
                                                        $check_access_group_action =  $this->m_action->check_access_group_action($item->access_id,$group_id,3);
                                                ?>
                                                    <input type='checkbox' class='ttip_t chk_edit' title='edit' <?php echo isset($check_access_group_action) && $check_access_group_action->num_rows() > 0 ? 'checked="checked"' : '';?> name='acc_name[<?php echo $item->access_id; ?>][3]' value='3' style='cursor:pointer;' class='south'>
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </td>
                                            <td align='center'>
                                                <?php
                                                    $check_access_action = $this->m_action->check_access_action($item->access_id,4);
                                                    if($check_access_action->num_rows() > 0):
                                                        $check_access_group_action =  $this->m_action->check_access_group_action($item->access_id,$group_id,4);
                                                ?>
                                                    <input type="checkbox" class="ttip_t chk_delete" title="delete" <?php echo isset($check_access_group_action) && $check_access_group_action->num_rows() > 0 ? 'checked="checked"' : '';?> name="acc_name[<?php echo $item->access_id; ?>][4]" value="4" style="cursor:pointer;">
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </td>
                                            <td align='center'>
                                                <?php
                                                    $check_access_action = $this->m_action->check_access_action($item->access_id,5);
                                                    if($check_access_action->num_rows() > 0):
                                                        $check_access_group_action =  $this->m_action->check_access_group_action($item->access_id,$group_id,5);
                                                ?>
                                                    <input type="checkbox" class="ttip_t chk_view" title="view" <?php echo isset($check_access_group_action) && $check_access_group_action->num_rows() > 0 ? 'checked="checked"' : '';?> name="acc_name[<?php echo $item->access_id; ?>][5]" value="5" style="cursor:pointer;">
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </td>
                                            <td align='center'>
                                                <?php
                                                    $check_access_action = $this->m_action->check_access_action($item->access_id,6);
                                                    if($check_access_action->num_rows() > 0):
                                                        $check_access_group_action =  $this->m_action->check_access_group_action($item->access_id,$group_id,6);
                                                ?>
                                                    <input type="checkbox" class="ttip_t chk_access" title="access" <?php echo isset($check_access_group_action) && $check_access_group_action->num_rows() > 0 ? 'checked="checked"' : '';?> name="acc_name[<?php echo $item->access_id; ?>][6]" value="6" style="cursor:pointer;">
                                                <?php else: ?>
                                                    -
                                                <?php endif; ?>
                                            </td>          
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?> 
                               
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<script>

    $(document).ready(function () {
        $("#select_all_index").change(function () {
            if (this.checked) {
                $(".chk_index").each(function () {
                    this.checked = true;
                })
            } else {
                $(".chk_index").each(function () {
                    this.checked = false;
                })
            }
        });

        $(".chk_index").click(function () {
            if (!$(this).is(":checked")) {
                $("#select_all_index").prop("checked", false);
            } else {
                var flag = 0;
                $(".chk_index").each(function () {
                    if (!this.checked)
                        flag = 1;
                })
                if (flag == 0) {
                    $("#select_all_index").prop("checked", true);
                }
            }
        });

        $("#select_all_add").change(function () {
            if (this.checked) {
                $(".chk_add").each(function () {
                    this.checked = true;
                })
            } else {
                $(".chk_add").each(function () {
                    this.checked = false;
                })
            }
        });

        $(".chk_add").click(function () {
            if (!$(this).is(":checked")) {
                $("#select_all_add").prop("checked", false);
            } else {
                var flag = 0;
                $(".chk_add").each(function () {
                    if (!this.checked)
                        flag = 1;
                })
                if (flag == 0) {
                    $("#select_all_add").prop("checked", true);
                }
            }
        });

        $("#select_all_edit").change(function () {
            if (this.checked) {
                $(".chk_edit").each(function () {
                    this.checked = true;
                })
            } else {
                $(".chk_edit").each(function () {
                    this.checked = false;
                })
            }
        });

        $(".chk_edit").click(function () {
            if (!$(this).is(":checked")) {
                $("#select_all_edit").prop("checked", false);
            } else {
                var flag = 0;
                $(".chk_edit").each(function () {
                    if (!this.checked)
                        flag = 1;
                })
                if (flag == 0) {
                    $("#select_all_edit").prop("checked", true);
                }
            }
        });

        $("#select_all_delete").change(function () {
            if (this.checked) {
                $(".chk_delete").each(function () {
                    this.checked = true;
                })
            } else {
                $(".chk_delete").each(function () {
                    this.checked = false;
                })
            }
        });

        $(".chk_delete").click(function () {
            if (!$(this).is(":checked")) {
                $("#select_all_delete").prop("checked", false);
            } else {
                var flag = 0;
                $(".chk_delete").each(function () {
                    if (!this.checked)
                        flag = 1;
                })
                if (flag == 0) {
                    $("#select_all_delete").prop("checked", true);
                }
            }
        });

        $("#select_all_view").change(function () {
            if (this.checked) {
                $(".chk_view").each(function () {
                    this.checked = true;
                })
            } else {
                $(".chk_view").each(function () {
                    this.checked = false;
                })
            }
        });

        $(".chk_view").click(function () {
            if (!$(this).is(":checked")) {
                $("#select_all_view").prop("checked", false);
            } else {
                var flag = 0;
                $(".chk_view").each(function () {
                    if (!this.checked)
                        flag = 1;
                })
                if (flag == 0) {
                    $("#select_all_view").prop("checked", true);
                }
            }
        });

        $("#select_all_access").change(function () {
            if (this.checked) {
                $(".chk_access").each(function () {
                    this.checked = true;
                })
            } else {
                $(".chk_access").each(function () {
                    this.checked = false;
                })
            }
        });

        $(".chk_access").click(function () {
            if (!$(this).is(":checked")) {
                $("#select_all_access").prop("checked", false);
            } else {
                var flag = 0;
                $(".chk_access").each(function () {
                    if (!this.checked)
                        flag = 1;
                })
                if (flag == 0) {
                    $("#select_all_access").prop("checked", true);
                }
            }
        });

    });

</script>