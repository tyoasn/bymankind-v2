<input type="text" name="url" id="url" readonly hidden value="#">
   <div class="container-fluid">
      <div class="header">
         <h3 class="title">Groups
         	<?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
	        <?php if(count($add_action) > 0): ?>
	        <?php $button_add = $this->m_action->get_id_action(2); ?>
         	<a href="<?php echo base_url(); ?>backend/group/group/add" class="btn btn-info btn-fill"><?php echo $button_add->action_alias; ?> Group</a>
         	<?php endif; ?>
         </h3>
      </div>
	  <br>
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="content">
                  <div class="tab-content">
                     <div id="all-data" class="tab-pane active">
                        <div class="content table-responsive table-full-width">
                           <table class="table table-hover table-striped">

                           <div class="columns columns-right pull-right">
                              <a href="<?php echo base_url(); ?>backend/group/group" class="btn btn-default" title="Refresh"><i class="glyphicon fa fa-refresh"></i></a>
                           </div>
                              <thead>
                                 <th>#</th>
                                  <th>Name</th>
                                  <th>Description</th>
                                  <th>Status</th>
                                  <th></th>
                              </thead>
                              <tbody>

                              	<?php if (count($groups) > 0) :?>
                            		<?php foreach($groups as $item): ?>

                            			<tr>
                            				<td><?php echo $item['group_id']; ?></td>
                                    <td><?php echo $item['group_name']; ?></td>
                                    <td><?php echo $item['group_description']; ?></td>
                                    <td>
                                        <?php
                                            if($item['group_status'] == 1){
                                                echo 'Active';
                                            } elseif ($item['group_status'] == 0) {
                                                echo 'Nonactive';
                                            }
                                        ?>
                                    </td>
			                                <td class="td-actions">

                                          <a class="color-warning" href="<?php echo base_url(); ?>backend/group/group/access/<?php echo $item['group_id']; ?>"><i class="fa fa-gear fa-lg"></i></a>

	                                        <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                                        	<?php if(count($edit_action) > 0): ?>
                                        		<?php $button_edit = $this->m_action->get_id_action(3); ?>
                                            <a href="<?php echo base_url(); ?>backend/group/group/edit/<?php echo $item['group_id']; ?>" >
                                               <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
                                               </button>
                                            </a>
	                                        <?php endif; ?>

	                                        <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                        	<?php if(count($delete_action) > 0): ?>
                                        	  <?php $button_delete = $this->m_action->get_id_action(4); ?>
	                                          <a href="<?php echo base_url(); ?>backend/group/group/delete/<?php echo $item['group_id']; ?>" id="delete" >
	                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
	                                             </button>
	                                          </a>
	                                         <?php endif; ?>
	                                       </td>
                            			</tr>

                            		<?php endforeach; ?>
                            	<?php endif; ?>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--  end card  -->
         </div>
         <!--  end col-md-12  -->
      </div>
   </div>