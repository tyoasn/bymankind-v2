<div class="container-fluid">
   <div class="header">
      <h3 class="title">Add New Group</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Name 
                     </label>
                     <input name="group_name" class="form-control" type="text" placeholder="Enter name" required>
                  </div>
                  <div class="form-group">
                     <label>
                        Description 
                     </label>
                     <textarea name="group_description" class="form-control" rows="4" placeholder="Enter description" required></textarea>
                  </div>
                  <div class="form-group">
                     <label>
                        Status 
                     </label>
                     <select name="group_status" class="form-control" id="select">
                          <option value="1">Active</option>
                          <option value="0">Nonactive</option>
                      </select>
                  </div>
                  <div class="form-group">
                    <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
    $(function () {
        $('.summernote').summernote({
            height: 250,
            minHeight: 250, // set minimum height of editor
            maxHeight: 500, // set maximum height of editor
            focus: true
        });

        /*$('form').on('submit', function (e) {
         e.preventDefault();
         alert($('.summernote').summernote('code'));
         alert($('.summernote').val());
         });*/
    });

    $('#publishDate').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
        todayHighlight: true,
        orientation: "bottom auto"
    });
</script>