<div class="container-fluid">
   <div class="header">
      <h3 class="title">Add New Media</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                        <label class="control-label">File</label>
                        <input class="form-control" name="userfile" id="userfile" type="file" required>
                        <span style="color:#ab3737;"> * Maximum size 300MB </span>
                        <br>
                        <span style="color:#ab3737;"> * File type (JPG / PNG / PDF / DOC / DOCX / XLS / XLSX / PPT / PPTX / RAR / ZIP / MP3 / MP4) </span>
                    </div>
               </div>
               <div class="content">
                  <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
              </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>