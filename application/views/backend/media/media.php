<input type="text" name="url" id="url" readonly hidden value="#">
   <div class="container-fluid">
      <div class="header">
         <h3 class="title">All Media 
         	<?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
	        <?php if(count($add_action) > 0): ?>
	        <?php $button_add = $this->m_action->get_id_action(2); ?>
          <div class="pull-right">
         	<a href="<?php echo base_url(); ?>backend/media/media/add" class="btn btn-info btn-fill"><?php echo $button_add->action_alias; ?> New</a>
          </div>
         	<?php endif; ?>
         </h3>
      </div>
	  <br>
      <div class="row">
         <div class="col-md-12">
            <div class="card">
               <div class="content">
                  <div class="tab-content">
                     <div id="all-data" class="tab-pane active">
                        <form action="">
			                     <div class="pull-left search">
	                           	<input type="text" name="title" placeholder="Enter title" class="form-control input-post" autocomplete="off">
	                         </div>
      			               </form>
                           <div class="columns columns-right pull-right">
                              <a href="<?php echo current_url(); ?>" class="btn btn-default" title="Refresh"><i class="glyphicon fa fa-refresh"></i></a>
                           </div>
                            <div class="content table-responsive table-full-width">
                              <table class="table table-hover table-striped">
                              <thead>
                                <th>Thumb</th>
                                <th>File</th>
                                <th>Type</th>
                                <th>Action</th>
                              </thead>
                              <tbody>
                              	<?php if (count($media) > 0) :?>
                            		<?php foreach($media as $item): ?>
                            			<tr>
                                    <td>
                                        <?php
                                            if($item['media_type'] == 'image'){
                                                $thumbpath = pathinfo($item['media_name']);
                                                $thumb = $item['media_directory'].$thumbpath['filename'].'_small.'.$thumbpath['extension'];
                                            } else {
                                                $thumb = base_url().'assets/img/file.png';
                                            }
                                        ?>
                                        <div class="img-container">
                                            <img src="<?php echo $thumb; ?>" height="50" width="50">
                                        </div>
                                    </td>
                                    <td>
                                        <?php
                                            if($item['media_type'] == 'image'){
                                                if (strpos($item['media_name'], 'media_') !== false) {
                                                    $thumbpath = pathinfo($item['media_name']);
                                                    //$link = $item['media_directory'].$thumbpath['filename'].'_large.'.$thumbpath['extension'];
                                                    $link = $item['media_directory'].$thumbpath['filename'].'.'.$thumbpath['extension'];
                                                } else {
                                                    $thumbpath = pathinfo($item['media_name']);
                                                    //$link = $item['media_directory'].$thumbpath['filename'].'_large.'.$thumbpath['extension'];
                                                    $link = $item['media_directory'].'original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                                                }
                                            } else {
                                                $link = $item['media_directory'].$item['media_name'];
                                            }
                                        ?>
                                        <a href="<?php echo $link; ?>" target="_blank"><?php echo $item['media_name']; ?></a>
                                    </td>
                                    <td><?php echo $item['media_type']; ?></td>
                                    <td class="td-actions">
                                     <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                            <?php if(count($delete_action) > 0): ?>
                                                <?php $button_delete = $this->m_action->get_id_action(4); ?>
                                     <a href="<?php echo base_url(); ?>backend/media/media/mediadelete/<?php echo $item['media_id']; ?>" id="delete" >
                                        <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
                                        </button>
                                     </a>
                                     <?php endif; ?>
                                  </td>
                            			</tr>
                            		<?php endforeach; ?>
                            	<?php endif; ?>
                                 <!--  <tr>
                                    <td colspan="8" align="center"><i>No Record Found</i></td>
                                 </tr> -->
                                 <tr>
                                    <td colspan="8" align="center">
                                       <?php echo $paging; ?>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--  end card  -->
         </div>
         <!--  end col-md-12  -->
      </div>
   </div>