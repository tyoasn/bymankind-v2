<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>
            Browse
        </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo base_url(); ?>asset_admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo base_url(); ?>asset_admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo base_url(); ?>asset_admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url(); ?>asset_admin/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
        
        <!-- <link href="{{ asset('assets/back/css/magnific-popup.css') }}" rel="stylesheet" type="text/css" /> -->

        <style type="text/css">
            .attachment img {
                width: 150px;
            }
            .media-box {
                background: #fff;
            }
            .nomediayet {
                padding: 2rem 0 0.5rem;
            }
            .image-container {
                display: inline-block; 
                text-align: center;
            }
            .attachment {
                display: block;
            }
        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="skin-black">
        
        <div class="wrapper">
            <section class="content">
                <div class="row">
                    <ul class="nav nav-tabs nav-justified">
                        <li role="presentation" <?php if($show == 'all'): ?> class="active" <?php endif; ?>>
                            <a href="<?php echo base_url().'backend/media/media/mediabrowseCareer'; ?>?id=<?php echo $careerid; ?>&CKEditor=post-content&CKEditorFuncNum=1&langCode=en&show=all">
                                All media items (<?php echo $allcount; ?>)
                            </a>
                        </li>
                        <li role="presentation" <?php if($show == 'current'): ?> class="active" <?php endif; ?>>
                            <a href="<?php echo base_url().'backend/media/media/mediabrowseCareer'; ?>?id=<?php echo $careerid; ?>&CKEditor=post-content&CKEditorFuncNum=1&langCode=en&show=current">
                                Uploaded to this page (<?php echo $currentcount; ?>)
                            </a>
                        </li>
                    </ul>

                    <div class="col-md-12 media-box">
                        <?php if(count($media) == 0): ?>
                            <div class="nomediayet">
                                <div class="callout callout-info">
                                    <p>There is no media yet.</p>
                                </div>
                            </div>
                        <?php else: ?>
                            <div>
                            <?php foreach ($media as $key => $value): ?>
                                <?php $pathinfo = pathinfo($value->media_name); ?>
                                <div class="image-container">
                                    <a href="<?php echo base_url().'asset_admin/assets/uploads/media/image/'.$value->media_name; ?>" class="attachment">
                                        <img src="<?php echo base_url().'asset_admin/assets/uploads/media/image/'.$value->media_name; ?>" alt="" class="img-thumbnail margin">
                                    </a>

                                    <?php
                                        $params = ['mediaid' => $value->media_id];
                                        $params = array_merge($params, $_GET);
                                    ?>
                                    <?php /* ?><button id="media-caption-<?php echo $value->media_id; ?>" class="media-caption btn btn-success btn-xs" data-toggle="modal" data-target="#captionModal" data-url="<?php echo base_url().'backend/media/media/mediacaption/'.$params['mediaid']; ?>" data-caption="<?php echo $value->caption; ?>" data-id="<?php echo $value->media_id; ?>">Edit Caption</button><?php */ ?>
                                    <a class="media-delete btn btn-danger btn-xs" href="<?php echo base_url().'backend/media/media/mediadelete/'.$params['mediaid']; ?>">Delete</a>

                                    <br>
                                    <br>
                                </div>
                            <?php endforeach; ?>
                            </div>

                            <?php echo $paging; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </section>
        </div>

        <div class="modal fade" id="captionModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="image-caption">Image Caption</label>
                            <textarea id="image-caption" class="form-control" rows="2" name="caption"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger" id="save-caption">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>asset_admin/js/moment-with-locales.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>asset_admin/js/app.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>asset_admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        
        <!-- <script src="{{ asset('assets/back/js/jquery.magnific-popup.min.js') }}" type="text/javascript"></script> -->

        <script type="text/javascript">
            $('.attachment').on('click', function(e){
                e.preventDefault();
                window.opener.CKEDITOR.tools.callFunction(<?php echo $callback; ?>,$(this).attr('href'));
                window.close();
            });

            $('.media-delete').on('click', function(e){
                return confirm('Are you sure to delete this image?');
            });

            $('.media-caption').on('click', function(e){
                var $this = $(this);

                $('#image-caption').val($this.data('caption'));
                $('#save-caption').data('url', $this.data('url'));
                $('#save-caption').data('id', $this.data('id'));
            });

            $('#save-caption').on('click', function(e){
                var $this = $(this),
                    imageCaption = $('#image-caption').val();

                $.post( $this.data('url'), { caption: $('#image-caption').val() }).done(function( data ) {
                    $('#media-caption-' + $this.data('id')).data('caption', imageCaption);
                    $('#captionModal').modal('hide');
                });
            });
        </script>

    </body>
</html>