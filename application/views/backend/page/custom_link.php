<div class="container-fluid">
   <div class="header">
      <h3 class="title">Custom Link 
         <?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
           <?php if(count($add_action) > 0): ?>
           <?php $button_add = $this->m_action->get_id_action(2); ?>
         <a href="<?php echo base_url(); ?>backend/pages/custom_link/add" class="btn btn-info btn-fill">Add New</a>
         <?php endif; ?>
      </h3>
   </div>
   <br>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <!-- Here you can write extra buttons/actions for the toolbar -->
            <div class="content">
               <div class="tab-content">
                  <div id="all-data" class="tab-pane active">
                     <table class="table table-hover table-bordered table-gfg" id="sampleTable">

                        <div class="columns columns-right pull-right">
                           <a href="<?php echo base_url(); ?>backend/pages/custom_link" class="btn btn-default" name="refresh" title="Refresh"><i class="glyphicon fa fa-refresh"></i></a>
                        </div>
                        <thead>
                           <th>Name</th>
                            <th>Link</th>
                            <th>Author</th>
                            <th>Date</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                           <?php if (count($custom_link) > 0) :?>
                                <?php foreach($custom_link as $item): ?>
                                    <tr>
                                       <td><?php echo $item['page_name']; ?></td>
                                        <td><a href="<?php echo $item['page_url']; ?>" target="_blank"><?php echo $item['page_url']; ?></a></td>
                                        <td>
                                            <?php
                                                $user = $this->m_user->get_id_user($item['page_created_by']);
                                                echo $user->user_username;
                                            ?>
                                        </td>
                                        <td><?php echo $item['page_created_at']; ?></td>
                                       <td class="td-actions">

                                          <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                                                 <?php if(count($edit_action) > 0): ?>
                                                     <?php $button_edit = $this->m_action->get_id_action(3); ?>
                                          <a href="<?php echo base_url(); ?>backend/pages/custom_link/edit/<?php echo $item['page_id']; ?>" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>

                                          <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                                 <?php if(count($delete_action) > 0): ?>
                                                     <?php $button_delete = $this->m_action->get_id_action(4); ?>
                                          <a href="<?php echo base_url(); ?>backend/pages/custom_link/delete/<?php echo $item['page_id']; ?>" id="delete" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>
                                       </td>
                                    </tr>
                                 <?php endforeach; ?>
                            <?php endif; ?>
                           </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!--  end card  -->
      </div>
      <!--  end col-md-12  -->
   </div>
   <!-- end row -->
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/plugins/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$('#sampleTable').DataTable({
    "order": [[ 3, "desc" ]],
    aLengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
    ],
});
</script>