<div class="container-fluid">
   <div class="header">
      <h3 class="title">Edit Page</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Name 
                        <star>*</star>
                        <?php echo form_error('page_name','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="page_name" type="text" placeholder="Enter name" value="<?php echo $page->page_name; ?>" autocomplete="off" required>

                     <input type="hidden" name="id" id="page-id" value="0">
                  </div>
                  <div class="form-group">
                     <label>
                        Description 
                        <star>*</star>
                        <?php echo form_error('page_description','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="page_description" type="text" placeholder="Enter Description" value="<?php echo $page->page_description; ?>" autocomplete="off">
                  </div>
                  <div class="form-group">
                     <label>
                        Title 
                        <star>*</star>
                        <?php echo form_error('page_title','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="page_title" type="text" placeholder="Enter title" value="<?php echo $page->page_title; ?>" autocomplete="off" required>
                  </div>
                  <div class="form-group">
                     <label>
                        Subtitle 
                        <star>*</star>
                        <?php echo form_error('page_subtitle','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="page_subtitle" type="text" placeholder="Enter subtitle" value="<?php echo $page->page_subtitle; ?>" autocomplete="off" required>
                  </div>
                  <div class="form-group">
                     <label>
                        Content 
                        <star>*</star>
                        <?php echo form_error('page_content','<span style="color:red;">','</span>'); ?>
                     </label>
                     <textarea class="form-control" name="page_content" id="post-content" rows="4" placeholder="Enter content"><?php echo $page->page_content; ?></textarea>
                  </div>
                  <div class="category">
                     <star>*</star>
                     Mandatory
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <!--  end col-md-6  -->
         <div class="col-md-3">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                    <label>
                       Status 
                       <star>*</star>
                    </label>
                    <select name="page_status" class="form-control" data-title="Single Select" data-style="btn-default btn-block" data-menu-style="dropdown-blue" required="true">
                       <option value="2" <?php echo isset($page->page_status) && $page->page_status == 2 ? 'selected="selected"' : '';?>>Draft</option>
                       <option value="1" <?php echo isset($page->page_status) && $page->page_status == 1 ? 'selected="selected"' : '';?>>Publish</option>
                    </select>
                 </div>

                 <?php
                  if (isset($page->page_publishdate)) {
                      $temp_date = explode(' ', $page->page_publishdate);
                      $tgl = $temp_date[0];

                      $temp_jam = explode(':', $temp_date[1]);
                      $jam = $temp_jam[0];
                      $menit = $temp_jam[1];

                      if ($temp_date[0] == '0000-00-00') {
                          $tgl = '';
                      }
                  }
                  ?>

                  <div class="form-group">
                        <label class="control-label">Publish Date <star>*</star> <?php echo form_error('page_publishdate','<span style="color:red;">','</span>'); ?></label>
                        <div class="input-group"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input class="form-control datetimepicker" name="page_publishdate" id="publishDate" type="text" placeholder="Select Date" value="<?php echo $tgl; ?>" autocomplete="off" required>
                        </div>
                        <div id="<?php echo $tgl == "" ? 'ket_waktu' : 'ket' ?>">
                            <label class="control-label">Jam</label>
                            <select name="jam" class="form-control">
                                <?php for ($i = 0; $i <= 23; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>" <?php echo $jam == $i ? 'selected="selected"' : '' ?>><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                            <label class="control-label">Menit</label>
                            <select name="menit" class="form-control">
                                <?php for ($i = 0; $i <= 59; $i++) { ?>
                                    <option value="<?php echo $i < 10 ? '0' . $i : $i; ?>" <?php echo $menit == $i ? 'selected="selected"' : '' ?>><?php echo $i < 10 ? '0' . $i : $i; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                  <div class="form-group" align="right">
                     <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/moment.min.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url(); ?>asset_admin/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

<?php $this->load->view('backend/page/ckeditor.php'); ?>

<script type="text/javascript">
    $(function () {
        $('.summernote').summernote({
            height: 250,
            minHeight: 250, // set minimum height of editor
            maxHeight: 500, // set maximum height of editor
            focus: true
        });

        /*$('form').on('submit', function (e) {
         e.preventDefault();
         alert($('.summernote').summernote('code'));
         alert($('.summernote').val());
         });*/
    });

    $('#publishDate').datetimepicker({
      format: "YYYY-MM-DD",
    });

    jQuery(document).ready(function(){
        jQuery("#ket_waktu").css("display","none");
        jQuery("#publishDate").change(function(){
            jQuery("#ket_waktu").css("display","inline");
        });
    });
</script>