<script type="text/javascript">
	$(function() {
            var pageid = $('#page-id').val();

            CKEDITOR.config.skin = 'bootstrapck';
            CKEDITOR.config.height = 300;
            CKEDITOR.config.filebrowserBrowseUrl = "<?php echo base_url().'backend/media/media/mediabrowsePage'; ?>?id=" + pageid + "&show=current";
            CKEDITOR.config.filebrowserUploadUrl = "<?php echo base_url().'backend/media/media/mediauploadPage'; ?>?id=" + pageid;
            CKEDITOR.config.filebrowserWindowWidth  = 900;
            CKEDITOR.config.filebrowserWindowHeight = 500;
            CKEDITOR.config.extraPlugins = 'oembed,justify';
            CKEDITOR.replace('post-content', CKEDITOR.config);
        });
</script>