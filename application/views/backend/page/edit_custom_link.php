<div class="container-fluid">
   <div class="header">
      <h3 class="title">Add New Page</h3>
   </div>
   <br>
   <form  id="cms" method="POST" action="" enctype="multipart/form-data">
      <div class="row">
         <div class="col-md-9">
            <div class="card">
               <div class="content">
                  <div class="form-group">
                     <label>
                        Name 
                        <star>*</star>
                        <?php echo form_error('page_name','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="page_name" type="text" placeholder="Enter name" value="<?php echo $custom_link->page_name; ?>" autocomplete="off" required>

                     <input type="hidden" name="id" id="page-id" value="0">
                  </div>
                  <div class="form-group">
                     <label>
                        Description 
                        <star>*</star>
                        <?php echo form_error('page_description','<span style="color:red;">','</span>'); ?>
                     </label>
                     <input class="form-control" name="page_url" type="text" placeholder="Enter URL" value="<?php echo $custom_link->page_url; ?>" autocomplete="off">
                  </div>
                  <div class="form-group">
                      <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill btn-wd">
                  </div>
                  <div class="category">
                     <star>*</star>
                     Mandatory
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
   <!--  end col-md-6  -->
   </div>
   </form>
</div>