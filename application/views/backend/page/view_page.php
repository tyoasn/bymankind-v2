<div class="page-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> Page Detail</h1>
    </div>
    <div>
        <ul class="breadcrumb">
            <li><i class="fa fa-home fa-lg"></i></li>
            <li><a href="<?php echo base_url(); ?>backend/page/page">Pages</a></li>
            <li><a href="<?php echo base_url(); ?>backend/page/page/views/<?php echo $page->page_id; ?>">Page Detail</a></li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-title-w-btn">
                <h3 class="title"></h3>
                <div>
                    <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                    <?php if(count($edit_action) > 0): ?>
                        <?php $button_edit = $this->m_action->get_id_action(3); ?>
                    <a class="btn btn-warning btn-flat" href="<?php echo base_url(); ?>backend/page/page/edit/<?php echo $page->page_id; ?>" title="<?php echo $button_edit->action_alias; ?>"><i class="fa fa-lg <?php echo $button_edit->action_icon; ?>"></i> <?php echo $button_edit->action_alias; ?> Page</a>
                    <?php endif; ?>
                    <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                    <?php if(count($delete_action) > 0): ?>
                        <?php $button_delete = $this->m_action->get_id_action(4); ?>
                    <a class="btn btn-danger btn-flat" href="<?php echo base_url(); ?>backend/page/page/delete/<?php echo $page->page_id; ?>" title="<?php echo $button_delete->action_alias; ?>"><i class="fa fa-lg <?php echo $button_delete->action_icon; ?>"></i></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="card-body">
                <h3 class="title">
                    <strong>
                        <?php echo $page->page_title; ?>
                    </strong>
                </h3>
                <h5>
                    <div class="publish-date label label-success">
                        <?php echo $page->page_created_at; ?> // <?php echo $this->m_user->get_id_user($page->page_created_by)->user_username; ?>
                    </div>
                </h5>
                
                <div>
                    <img src="<?php echo base_url(); ?>asset_admin/assets/uploads/page/original/<?php echo $page->page_photo; ?>" height="100px" style="margin-top: 20px; margin-bottom: 20px;" alt=""/>
                </div>
                
                <p>
                    <?php echo $page->page_content; ?>
                </p>
            </div>
        </div>
    </div>
</div>