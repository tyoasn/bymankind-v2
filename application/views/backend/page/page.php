<div class="container-fluid">
   <div class="header">
      <h3 class="title">All Pages 
         <?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
           <?php if(count($add_action) > 0): ?>
           <?php $button_add = $this->m_action->get_id_action(2); ?>
         <a href="<?php echo base_url(); ?>backend/pages/pages/add" class="btn btn-info btn-fill">Add New</a>
         <?php endif; ?>
      </h3>
   </div>
   <br>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <!-- Here you can write extra buttons/actions for the toolbar -->
            <div class="content">
               <ul role="tablist" class="nav nav-tabs">
                  <li role="presentation" <?php if($base == "Pageall"): ?> class="active" <?php endif; ?>>
                     <a href="<?php echo base_url(); ?>backend/pages/pages/all" class="text-danger">All <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_all; ?></button></a>
                  </li>
                  <li <?php if($base == "Pagemain"): ?> class="active" <?php endif; ?>>
                     <a href="<?php echo base_url(); ?>backend/pages/pages/main" class="text-danger">Mine <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_main; ?></button></a>
                  </li>
                  <li <?php if($base == "Page"): ?> class="active" <?php endif; ?>>
                     <a href="<?php echo base_url(); ?>backend/pages/pages" class="text-danger">Published <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total; ?></button></a>
                  </li>
                  <li <?php if($base == "Pagedraft"): ?> class="active" <?php endif; ?>>
                     <a href="<?php echo base_url(); ?>backend/pages/pages/draft" class="text-danger">Draft <button class="btn btn-round btn-xs btn-fill btn-danger"><?php echo $total_draft; ?></button></a>
                  </li>
               </ul>
               <div class="tab-content">
                  <div id="all-data" class="tab-pane active">
                     <table class="table table-hover table-bordered table-gfg" id="sampleTable">

                        <!-- <form action="">
                           <div class="pull-left search">
                              <input class="form-control" type="text" placeholder="Search">
                           </div>
                        </form> -->

                        <div class="columns columns-right pull-right">
                           <a href="<?php echo base_url(); ?>backend/pages/pages" class="btn btn-default" name="refresh" title="Refresh"><i class="glyphicon fa fa-refresh"></i></a>
                           <!-- <div class="keep-open btn-group" title="Columns">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="glyphicon fa fa-columns"></i> <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                 <li><label><input type="checkbox" data-field="no" value="0" checked="checked"> #</label></li>
                                 <li><label><input type="checkbox" data-field="name" value="1" checked="checked"> Name</label></li>
                                 <li><label><input type="checkbox" data-field="title" value="2" checked="checked"> Title</label></li>
                                 <li><label><input type="checkbox" data-field="status" value="3" checked="checked"> Status</label></li>
                                 <li><label><input type="checkbox" data-field="creatdat" value="4" checked="checked"> Created at</label></li>
                                 <li><label><input type="checkbox" data-field="createdby" value="5" checked="checked"> Created by</label></li>
                              </ul>
                           </div> -->
                        </div>
                        <thead>
                           <th>Name</th>
                             <th>Title</th>
                             <th>Parent</th>
                             <th>Author</th>
                             <th>Date</th>
                             <th>Action</th>
                        </thead>
                        <tbody>
                           <?php if (count($page) > 0) :?>
                                <?php foreach($page as $item): ?>
                                    <tr>
                                       <td><?php echo $item['page_name']; ?></td>
                                       <td><?php echo $item['page_title']; ?></td>
                                       <td>
                                           <?php
                                               if($item['page_parent_id'] == 0){
                                                   echo 'None';
                                               } else {
                                                   echo $this->m_page->get_id_page($item['page_parent_id'])->page_name;
                                               }
                                           ?>
                                       </td>
                                       <td>
                                           <?php
                                               $user = $this->m_user->get_id_user($item['page_created_by']);
                                               echo $user->user_username;
                                           ?>
                                       </td>
                                       <td><?php echo $item['page_publishdate']; ?></td>
                                       <td class="td-actions">
                                          <?php $view_action = $this->m_access->get_access_group_action($this->group_id, 5, $this->access_id); ?>
                                           <?php if(count($view_action) > 0): ?>
                                           <?php $button_views = $this->m_action->get_id_action(5); ?>
                                           <?php
                                                 if($item['page_status'] == 2 || $item['page_publishdate'] >= date('Y-m-d H:i:s')){
                                                     $link = base_url().'page/preview/'.$item['page_slug'];
                                                 } else {
                                                     $link = base_url().'page/'.$item['page_slug'];
                                                 }
                                             ?>
                                          <a href="<?php echo $link; ?>" target="_blank" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_views->action_alias; ?>" class="btn btn-warning btn-simple btn-icon "><i class="fa fa-eye"></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>

                                          <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                                                 <?php if(count($edit_action) > 0): ?>
                                                     <?php $button_edit = $this->m_action->get_id_action(3); ?>
                                          <a href="<?php echo base_url(); ?>backend/pages/pages/edit/<?php echo $item['page_id']; ?>" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>

                                          <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                                 <?php if(count($delete_action) > 0): ?>
                                                     <?php $button_delete = $this->m_action->get_id_action(4); ?>
                                          <a href="<?php echo base_url(); ?>backend/pages/pages/delete/<?php echo $item['page_id']; ?>" id="delete" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>
                                       </td>
                                    </tr>
                                 <?php endforeach; ?>
                            <?php endif; ?>
                           </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!--  end card  -->
      </div>
      <!--  end col-md-12  -->
   </div>
   <!-- end row -->
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/plugins/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$('#sampleTable').DataTable({
    "order": [[ 4, "desc" ]],
    aLengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
    ],
});
</script>