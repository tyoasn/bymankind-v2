<div class="container-fluid">
   <div class="header">
      <h3 class="title">All Users
         <?php $add_action = $this->m_access->get_access_group_action($this->group_id, 2, $this->access_id); ?>
           <?php if(count($add_action) > 0): ?>
           <?php $button_add = $this->m_action->get_id_action(2); ?>
           <div class="pull-right">
         <a href="<?php echo base_url(); ?>backend/user/user/add" class="btn btn-info btn-fill">Add User</a>
       </div>
         <?php endif; ?>
      </h3>
   </div>
   <br>
   <div class="row">
      <div class="col-md-12">
         <div class="card">
            <!-- Here you can write extra buttons/actions for the toolbar -->
            <div class="content">
               <div class="tab-content">
                  <div id="all-data" class="tab-pane active">
                     <table class="table table-hover table-bordered table-gfg" id="sampleTable">

                        <div class="columns columns-right pull-right">
                           <a href="<?php echo base_url(); ?>backend/user/user" class="btn btn-default" name="refresh" title="Refresh"><i class="glyphicon fa fa-refresh"></i></a>
                        </div>
                        <thead>
                           <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Group</th>
                            <th>Status</th>
                            <th></th>
                        </thead>
                        <tbody>
                           <?php if (count($users) > 0) :?>
                                <?php foreach($users as $item): ?>
                                    <tr>
                                       <td><?php echo $item['user_id']; ?></td>
                                        <td><?php echo $item['user_first_name']; ?></td>
                                        <td><?php echo $item['user_last_name']; ?></td>
                                        <td><?php echo $item['user_username']; ?></td>
                                        <td><?php echo $item['user_email']; ?></td>
                                        <td>
                                            <?php
                                                $groups = $this->m_group->get_id_group($item['group_id']);
                                                echo $groups->group_name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if($item['user_status'] == 1){
                                                    echo "Active";
                                                } elseif ($item['user_status'] == 0) {
                                                    echo "Nonaktif";
                                                }
                                            ?>
                                        </td>
                                       <td class="td-actions">

                                          <?php if($item['user_status'] == 0): ?>
                                              <a class="color-success" data-toggle="tooltip" data-placement="top" title="Resend" href="<?php echo base_url(); ?>backend/user/user/resend_activation/<?php echo $item['user_id']; ?>"><i class="fa fa-send fa-lg"></i></a>
                                          <?php endif; ?>

                                          <?php $view_action = $this->m_access->get_access_group_action($this->group_id, 5, $this->access_id); ?>
                                           <?php if(count($view_action) > 0): ?>
                                           <?php $button_views = $this->m_action->get_id_action(5); ?>
                                          <a href="<?php echo base_url(); ?>backend/user/user/views/<?php echo $item['user_id']; ?>" target="_blank" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_views->action_alias; ?>" class="btn btn-warning btn-simple btn-icon "><i class="fa fa-eye"></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>

                                          <?php $edit_action = $this->m_access->get_access_group_action($this->group_id, 3, $this->access_id); ?>
                                                 <?php if(count($edit_action) > 0): ?>
                                                     <?php $button_edit = $this->m_action->get_id_action(3); ?>
                                          <a href="<?php echo base_url(); ?>backend/user/user/edit/<?php echo $item['user_id']; ?>" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_edit->action_alias; ?>" class="btn btn-success btn-simple btn-icon "><i class="fa fa-pencil-square-o"></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>

                                          <?php $delete_action = $this->m_access->get_access_group_action($this->group_id, 4, $this->access_id); ?>
                                                 <?php if(count($delete_action) > 0): ?>
                                                     <?php $button_delete = $this->m_action->get_id_action(4); ?>
                                          <a href="<?php echo base_url(); ?>backend/user/user/delete/<?php echo $item['user_id']; ?>" id="delete" >
                                             <button type="button" rel="tooltip" data-placement="left" title="<?php echo $button_delete->action_alias; ?>" class="btn btn-danger btn-simple btn-icon "><i class="fa fa-close "></i>
                                             </button>
                                          </a>
                                          <?php endif; ?>
                                       </td>
                                    </tr>
                                 <?php endforeach; ?>
                            <?php endif; ?>
                           </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <!--  end card  -->
      </div>
      <!--  end col-md-12  -->
   </div>
   <!-- end row -->
</div>

<script src="<?php echo base_url(); ?>asset_admin/js/plugins/bootbox.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
$('#sampleTable').DataTable({
    "order": [[ 0, "desc" ]],
    aLengthMenu: [
        [10, 25, 50, 100, -1],
        [10, 25, 50, 100, "All"]
    ],
});
</script>