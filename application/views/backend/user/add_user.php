<div class="container-fluid">
   <div class="header">
      <h3 class="title">Add New User</h3>
   </div>
   <br>
   <div class="row">
      <form method="POST" action="#" class="form-horizontal">
         <div class="col-md-8">
            <div class="card">
               <div class="header">Personal Information</div>
               <div class="content">
                  <div class="row">
                     <div class="col-md-6" style="padding: 0 30px 0 30px;">
                        <div class="form-group">
                           <label>First Name</label>
                           <input class="form-control" name="user_first_name" type="text" placeholder="John" autocomplete="off" required>
                        </div>
                     </div>
                     <div class="col-md-6" style="padding: 0 30px 0 30px;">
                        <div class="form-group">
                           <label>Last Name</label>
                           <input class="form-control" name="user_last_name" type="text" placeholder="Doe" autocomplete="off" required>  
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px;">
                        <div class="form-group">
                           <label>Address</label>
                           <textarea class="form-control" name="user_address" rows="4" placeholder="Enter address"></textarea>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px;">
                        <div class="form-group">
                           <label>Mobile No</label>
                           <input class="form-control" name="user_mobile" type="tel" placeholder="" autocomplete="off">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px;">
                        <div class="form-group">
                           <label>Home Phone</label>
                           <input class="form-control" name="user_phone" type="tel" placeholder="Enter name" autocomplete="off">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- end card -->
         </div>
         <div class="col-md-8">
            <div class="card">
               <div class="header">Account Information</div>
               <div class="content">
                  <div class="row">
                     <div class="col-md-6" style="padding: 0 30px 0 30px">
                        <div class="form-group">
                           <label>Email</label>
                           <input class="form-control" name="user_email" type="email" placeholder="john.doe@example.com" autocomplete="off" required>
                        </div>
                     </div>
                     <div class="col-md-6" style="padding: 0 30px 0 30px">
                        <div class="form-group">
                           <label>Username</label>
                           <input id="username" class="form-control" name="user_username" type="text" placeholder="johndoe" autocomplete="off" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px">
                        <div class="form-group">
                           <label>New Password</label>
                           <input id="password" class="form-control" name="user_password" type="password" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px">
                        <div class="form-group">
                           <label>Confirm Password</label>
                           <input id="confirm_password" class="form-control" name="user_confirm_password" type="password" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px">
                        <div class="form-group">
                           <label>Group</label>
                           <select class="form-control" name="group_id" id="select" required>
                               <?php foreach($group as $item): ?>
                                   <option value="<?php echo $item->group_id; ?>"><?php echo $item->group_name; ?></option>
                               <?php endforeach; ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px">
                        <div class="form-group">
                           <label>Language</label>
                           <select class="form-control" name="language_id" id="select2" required>
                               <?php foreach($language as $item): ?>
                                   <option value="<?php echo $item->language_id; ?>"><?php echo $item->language_name; ?></option>
                               <?php endforeach; ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-12" style="padding: 0 30px 0 30px">
                        <div class="form-group">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <input type="submit" name="submit" value="Save" class="btn btn-info btn-fill">
            <!-- end card -->
         </div>
      </form>
   </div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/plugins/bootstrap-datepicker.min.js"></script>

<script src="<?php echo base_url(); ?>asset_admin/js/plugins/bootstrap-show-password.js" type="text/javascript"></script>

<script>
    $(function () {
        $('#password').password().on('show.bs.password', function (e) {
            $('#eventLog').text('On show event');
            $('#methods').prop('checked', true);
        }).on('hide.bs.password', function (e) {
            $('#eventLog').text('On hide event');
            $('#methods').prop('checked', false);
        });
        $('#methods').click(function () {
            $('#password').password('toggle');
        });
    });
    
    $(function () {
        $('#confirm_password').password().on('show.bs.password', function (e) {
            $('#eventLog').text('On show event');
            $('#methods').prop('checked', true);
        }).on('hide.bs.password', function (e) {
            $('#eventLog').text('On hide event');
            $('#methods').prop('checked', false);
        });
        $('#methods').click(function () {
            $('#confirm_password').password('toggle');
        });
    });
</script>