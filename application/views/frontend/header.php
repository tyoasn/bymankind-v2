<!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="robots" content="index, follow" >
      <?php
            if($this->keywords->config_value != ""){
              $keywords = $this->keywords->config_value;
            } else {
              $keywords = 'agency, creative, digital, mankind';
            }
      ?>
      <meta name="keywords" content="<?php echo $keywords; ?>">
      <?php
            if($this->description->config_value != ""){
              $description = $this->description->config_value;
            } else {
              $description = 'Creative Agency From Jakarta';
            }
      ?>
      <meta name="description" content="<?php echo $description; ?>">
      <meta name="author" content="">
      <?php
        if($this->favicon->config_value != ""){
          $media = $this->m_media->get_media_id($this->favicon->config_value);
          $thumbpath = pathinfo($media->media_name);
          $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
        } else {
          $image = base_url().'assets/img/logobymankind.png';
        }
      ?>
      <link rel="shortcut icon" href="<?php echo $image; ?>">
      <!-- CSS -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
      <link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/style.css">
      <link rel='stylesheet' href="<?php echo base_url(); ?>assets/css/main.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/owl.theme.default.min.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">
      <link href="<?php echo base_url(); ?>assets/css/ionicons.min.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/css/magnific-popup.css" rel="stylesheet">
      <link href='<?php echo base_url(); ?>assets/css/simplelightbox.min.css' rel='stylesheet' type='text/css'>
      <!-- fonts -->
      <link rel="stylesheet" href="https://use.typekit.net/pcd4nar.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome/css/font-awesome.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/icons-fonts.css" >
      <!-- REVOLUTION STYLE SHEETS -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/settings.css">
      <?php
            if($this->title->config_value != ""){
              $title = $this->title->config_value;
            } else {
              $title = 'Mankind';
            }
      ?>
      <title><?php echo $title; ?></title>