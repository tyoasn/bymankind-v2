<section id="section-title">
         <div class="container">
            <div class="row">
               <div class="col-md-12 wow fadeIn">
                  <div class="title-page text-center">
                     <img src="<?php echo base_url(); ?>assets/img/doit/doitlikeus.png" class="img-title5">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <?php foreach($portfolio as $item): ?>
            <div class="col-md-6">
                  <div class="port-box wow fadeInLeft">
                     <a href="<?php echo base_url(); ?>portfolio/<?php echo $item['portfolio_slug']; ?>">
                        <?php
                        if($item['portfolio_is_thumbnail'] == 1){
                            if($item['portfolio_thumbnail']){
                                $media = $this->m_media->get_media_id($item['portfolio_thumbnail']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                        <img src="<?php echo $image; ?>" class="img-responsive" alt="portofolio">
                     </a>
                     <a href="<?php echo base_url(); ?>portfolio/<?php echo $item['portfolio_slug']; ?>">
                        <div class="title-portfolio"><?php echo $item['portfolio_title']; ?></div>
                     </a>
                  </div>
               </div>
            <?php endforeach;?> 
         </div>
<!--             <div class="col-md-6">
               <div class="port-box wow fadeInRight">
                  <a href="<?php echo base_url(); ?>portfolio"><img src="<?php echo base_url(); ?>assets/img/doit/thumbnailsupermusic.jpg" class="img-responsive" alt="portofolio"></a>
                  <a href="<?php echo base_url(); ?>portfolio"><div class="title-portfolio">Supermusic</div></a>
               </div>
               <div class="port-box wow fadeInRight">
                  <a href="<?php echo base_url(); ?>portfolio"><img src="<?php echo base_url(); ?>assets/img/doit/thumbnailsupermusictv.jpg" class="img-responsive" alt="portofolio"></a>
                  <a href="<?php echo base_url(); ?>portfolio"><div class="title-portfolio">Supermusic TV</div></a>
               </div>
               <div class="port-box wow fadeInRight">
                  <a href="<?php echo base_url(); ?>portfolio"><img src="<?php echo base_url(); ?>assets/img/doit/thumbnailhodgepodge.jpg" class="img-responsive" alt="portofolio"></a>
                  <a href="<?php echo base_url(); ?>portfolio"><div class="title-portfolio">Hodgepodge Superfest</div></a>
               </div>
               <div class="port-box wow fadeInRight">
                  <a href="<?php echo base_url(); ?>portfolio"><img src="<?php echo base_url(); ?>assets/img/doit/thumbnailgreenfg.jpg" class="img-responsive" alt="portofolio"></a>
                  <a href="<?php echo base_url(); ?>portfolio"><div class="title-portfolio">Green For Good</div></a>
               </div>
               <div class="port-box wow fadeInRight">
                  <a href="<?php echo base_url(); ?>portfolio"><img src="<?php echo base_url(); ?>assets/img/doit/thumbnailcgc.jpg" class="img-responsive" alt="portofolio"></a>
                  <a href="<?php echo base_url(); ?>portfolio"><div class="title-portfolio">Alcatel Lucent</div></a>
               </div>
            </div> -->
      </section>