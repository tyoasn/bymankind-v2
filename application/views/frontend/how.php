<section id="section-title">
         <div class="container">
            <div class="row">
               <div class="col-md-12 wow fadeIn">
                  <div class="title-page text-center">
                     <img src="<?php echo base_url(); ?>assets/img/how/howmankind.png" class="img-title" alt="how mankind">
                  </div>
               </div>
               <div class="col-md-6 col-how-title wow fadeIn">
                  <img src="<?php echo base_url(); ?>assets/img/how/wecollabphoto.png" class="img-wecollab" alt="collab">
               </div>
               <div class="col-md-6 col-how-title">
                  <div class="how-title2">
                     <img src="<?php echo base_url(); ?>assets/img/how/wecollab.png" class="img-wecollab2" alt="collab 2">
                     <?php
                           if($this->services_1->post_content != ""){
                             $services_1 = $this->services_1->post_content;
                           } else {
                             $services_1 = 'lorem ipsum';
                           }
                     ?>
                     <p>
                       <?php echo $services_1; ?>
                     </p>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-red">OUR</span><br><span class="font-red">SERVICES</span></h1>
               </div>
            </div>
            <div class="row">
            <div class="col-sm-4 grid1 wow fadeInLeft">
               <img src="<?php echo base_url(); ?>assets/img/how/digimarketing.png" class="img-service img-responsive" alt="digital marketing">
               <div class="txt-services">
                  <span class="line1"></span>
                  <h2>DIGITAL MARKETING</h2>
                  <?php
                           if($this->services_2->post_content != ""){
                             $services_2 = $this->services_2->post_content;
                           } else {
                             $services_2 = 'lorem ipsum';
                           }
                     ?>
                  <p><?php echo $services_2 ?></p>
               </div>
            </div>
            <div class="col-sm-4 grid2 wow fadeInUp">
               <img src="<?php echo base_url(); ?>assets/img/how/mobiledev.png" class="img-service img-responsive" alt="mobile development">
               <div class="txt-services2">
                  <span class="line2"></span>
                  <h2>MOBILE DEVELOPMENT</h2>
                  <?php
                           if($this->services_3->post_content != ""){
                             $services_3 = $this->services_3->post_content;
                           } else {
                             $services_3 = 'lorem ipsum';
                           }
                     ?>
                  <p><?php echo $services_3 ?></p>
               </div>
            </div>
            <div class="col-sm-4 wow fadeInRight">
               <img src="<?php echo base_url(); ?>assets/img/how/websitedev.png" class="img-service img-responsive" alt="website development">
               <div class="txt-services3">
                  <span class="line3"></span>
                  <h2>WEBSITE DEVELOPMENT</h2>
                  <?php
                           if($this->services_4->post_content != ""){
                             $services_4 = $this->services_4->post_content;
                           } else {
                             $services_4 = 'lorem ipsum';
                           }
                     ?>
                  <p><?php echo $services_4 ?></p>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-sm-4 grid1 wow fadeInLeft">
               <img src="<?php echo base_url(); ?>assets/img/how/aservice.png" class="img-service img-responsive" alt="advertising">
               <div class="txt-services">
                  <span class="line1"></span>
                  <h2>ADVERTISING</h2>
                  <?php
                           if($this->services_5->post_content != ""){
                             $services_5 = $this->services_5->post_content;
                           } else {
                             $services_5 = 'lorem ipsum';
                           }
                     ?>
                  <p><?php echo $services_5 ?></p>
               </div>
            </div>
            <div class="col-sm-4 grid2 wow fadeInUp">
               <img src="<?php echo base_url(); ?>assets/img/how/socmed.png" class="img-service img-responsive" alt="social media">
               <div class="txt-services2">
                  <span class="line2"></span>
                  <h2>SOCIAL MEDIA</h2>
                  <?php
                           if($this->services_6->post_content != ""){
                             $services_6 = $this->services_6->post_content;
                           } else {
                             $services_6 = 'lorem ipsum';
                           }
                     ?>
                  <p><?php echo $services_6 ?></p>
               </div>
            </div>
            <div class="col-sm-4 wow fadeInRight">
               <img src="<?php echo base_url(); ?>assets/img/how/branding.png" class="img-service img-responsive" alt="branding">
               <div class="txt-services3">
                  <span class="line3"></span>
                  <h2>BRANDING</h2>
                  <?php
                           if($this->services_7->post_content != ""){
                             $services_7 = $this->services_7->post_content;
                           } else {
                             $services_7 = 'lorem ipsum';
                           }
                     ?>
                  <p><?php echo $services_7 ?></p>
               </div>
            </div>
         </div>
         </div>
      </section>