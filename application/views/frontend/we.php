<section id="section-title">
         <div class="container">
            <div class="row">
               <div class="col-md-12 wow fadeIn">
                  <div class="title-page text-center">
                     <img src="<?php echo base_url(); ?>assets/img/we/wearehere.png" class="img-title5" alt="we are mankind">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="section-division">
         <div class="container">
            <div class="col-md-12 text-center wow fadeIn">
              <!--  <a id="btn-close">&times;</a> -->
               <a id="btn-frasa"></a>
               <a id="btn-studio"></a>
               <a id="btn-techno"></a>
               <p class="desc-frasa1">
                  Great things in businesss are never done by one person;
                  <br>
               <span class="font-white">they're done by a team of people.</span>
               </p>
               <?php
                           if($this->team_1->post_content != ""){
                             $team_1 = $this->team_1->post_content;
                           } else {
                             $team_1 = 'lorem ipsum';
                           }
                     ?>
               <p class="desc-frasa2">
                  <?php echo $team_1; ?>
               </p>
               <p class="desc-studio1">
                  Great things in businesss are never done by one person;
                  <br>
               <span class="font-white">they're done by a team of people.</span>
               </p>
               <?php
                           if($this->team_2->post_content != ""){
                             $team_2 = $this->team_2->post_content;
                           } else {
                             $team_2 = 'lorem ipsum';
                           }
                     ?>
               <p class="desc-studio2">
                  <?php echo $team_2; ?>
               </p>
               <p class="desc-techno1">
                  Great things in businesss are never done by one person;
                  <br>
               <span class="font-white">they're done by a team of people.</span>
               </p>
               <?php
                           if($this->team_3->post_content != ""){
                             $team_3 = $this->team_3->post_content;
                           } else {
                             $team_3 = 'lorem ipsum';
                           }
                     ?>
               <p class="desc-techno2">
                  <?php echo $team_3; ?>
               </p>
               <div id="div-all">
                  <img src="<?php echo base_url(); ?>assets/img/we/mankind-all-div.png" class="img-div" alt="mankind division">
               </div>
               <div id="div-frasa">
                  <img src="<?php echo base_url(); ?>assets/img/we/mankindfrasa.png" class="img-div" alt="mankind frasa">
               </div>
               <div id="div-studio">
                  <img src="<?php echo base_url(); ?>assets/img/we/mankindstudio.png" class="img-div" alt="mankind studio">
               </div>
               <div id="div-techno">
                  <img src="<?php echo base_url(); ?>assets/img/we/mankindtechno2.png" class="img-div" alt="mankind technology">
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-red">OUR</span><br><span class="font-red">TEAM</span></h1>
               </div>
            </div>
         </div>
         <div class="container col-client">
            <div class="row">

               <?php foreach($team1 as $item): ?>
               <div class="col-md-3 box-1">
                  <div class="frame-img">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image; ?>" class="img-person" alt="mankind member">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail_2']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail_2']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image2 = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image2 = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image2 = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image2; ?>" class="img-person" alt="mankind member">
                  </div>
                  <div class="txt-team"><?php echo $item['team_name']; ?></div>
                  <p class="txt-role"><?php echo $item['team_role']; ?></p>
<!--              <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                  <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-instagram"></i></a> -->
               </div>
               <?php endforeach;?> 
               
               
            </div>
         </div>
         <div class="container col-client">
            <div class="row">
               <div class="col-md-12 wow fadeInUp">
                  <div class="title-division">Mankind Frasa</div>
               </div>
               <?php foreach($team2 as $item): ?> 
               <div class="col-md-3 box-1">
                  <div class="frame-img">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image; ?>" class="img-person" alt="mankind member">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail_2']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail_2']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image2 = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image2 = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image2 = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image2; ?>" class="img-person" alt="mankind member">
                  </div>
                  <div class="txt-team"><?php echo $item['team_name']; ?></div>
                  <p class="txt-role"><?php echo $item['team_role']; ?></p>
<!--              <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                  <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-instagram"></i></a> -->
               </div>
               <?php endforeach;?> 
            </div>
         </div>
         <div class="container col-client">
            <div class="row">
               <div class="col-md-12 wow fadeInUp">
                  <div class="title-division">Mankind Studio</div>
               </div>
               <?php foreach($team3 as $item): ?> 
               <div class="col-md-3 box-1">
                  <div class="frame-img">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image; ?>" class="img-person" alt="mankind member">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail_2']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail_2']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image2 = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image2 = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image2 = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image2; ?>" class="img-person" alt="mankind member">
                  </div>
                  <div class="txt-team"><?php echo $item['team_name']; ?></div>
                  <p class="txt-role"><?php echo $item['team_role']; ?></p>
<!--              <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                  <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-instagram"></i></a> -->
               </div>
               <?php endforeach;?> 
            </div>
         </div>
         <div class="container col-client">
            <div class="row">
               <div class="col-md-12 wow fadeInUp">
                  <div class="title-division">Mankind Technology</div>
               </div>
               <?php foreach($team4 as $item): ?> 
               <div class="col-md-3 box-1">
                  <div class="frame-img">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image; ?>" class="img-person" alt="mankind member">
                     <?php
                        if($item['team_is_thumbnail'] == 1){
                            if($item['team_thumbnail_2']){
                                $media = $this->m_media->get_media_id($item['team_thumbnail_2']);
                                $thumbpath = pathinfo($media->media_name);
                                 $image2 = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                            } else {
                                $image2 = 'https://via.placeholder.com/300x300';
                            }
                        } else {
                            $image2 = 'https://via.placeholder.com/300x300';
                        }
                        ?>
                     <img src="<?php echo $image2; ?>" class="img-person" alt="mankind member">
                  </div>
                  <div class="txt-team"><?php echo $item['team_name']; ?></div>
                  <p class="txt-role"><?php echo $item['team_role']; ?></p>
<!--              <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                  <a class="socmed-menu2" href="#" target="_blank"><i class="fa fa-instagram"></i></a> -->
               </div>
               <?php endforeach;?> 
            </div>
         </div>
      </section>