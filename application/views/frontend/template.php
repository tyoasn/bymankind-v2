<!doctype html>
<html lang="en">
   <head>
      <?php $this->load->view('frontend/header'); ?>
   </head>
   <body>
      <?php $this->load->view('frontend/menu'); ?>
      <?php $this->load->view($mainpage); ?>
      <?php $this->load->view($footer); ?>
      <script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.min.js"></script>
      <?php $this->load->view('frontend/modal'); ?>
   </body>
</html>