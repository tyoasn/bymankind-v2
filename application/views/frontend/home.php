<section id="slider-home">
         <div id="rev_slider_26_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="bymankind" style="padding:0px;">
            <span class="overlay-bg"></span>
            <!-- START REVOLUTION SLIDER 5.4.7.2 fullscreen mode -->
            <div id="rev_slider_26_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.7.2">
               <ul>
                  <!-- SLIDE  -->
                  <li data-index="rs-101" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="-70"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                     <!-- LAYERS -->
                     <!-- LAYER NR. 1 -->
                     <a class="thisis-block" href="<?php echo base_url(); ?>thisis" target="_self">
                     <div class="tp-caption tp-resizeme" 
                        id="thisis2" 
                        data-x="['center','center','center','center']" data-hoffset="['-2','-61','-51','-30']" 
                        data-y="['top','top','top','top']" data-voffset="['100','82','128','163']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions=''
                        data-responsive_offset="on" 
                        data-frames='[{"delay":"bytrigger","speed":500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"frame":"999","to":"x:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="keep"><img src="<?php echo base_url(); ?>assets/img/banner/thisisred.png" alt="this is" data-ww="['1165px','800px','585px','351px']" data-hh="['233px','160px','117px','70px']" width="2732" height="546" data-no-retina> </div>
                     <!-- LAYER NR. 2 -->
                     <div class="tp-caption tp-resizeme" 
                        href="<?php echo base_url(); ?>thisis" target="_self" id="thisis1" 
                        data-x="['center','center','center','center']" data-hoffset="['-2','-61','-51','-30']" 
                        data-y="['top','top','top','top']" data-voffset="['100','82','128','163']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions='[{"event":"mouseenter","action":"stoplayer","layer":"thisis1","delay":"0"},{"event":"mouseleave","action":"startlayer","layer":"thisis1","delay":"0"},{"event":"mouseleave","action":"stoplayer","layer":"thisis2","delay":"0"},{"event":"mouseenter","action":"startlayer","layer":"thisis2","delay":"0"}]'
                        data-responsive_offset="on" 
                        data-frames='[{"delay":9,"speed":500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"x:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="reset"><img src="<?php echo base_url(); ?>assets/img/banner/thisisblack.png" alt="this is" data-ww="['1165px','800px','585px','351px']" data-hh="['233px','160px','117px','70px']" width="2732" height="546" data-no-retina> </div>
                     </a>

                     <!-- LAYER NR. 3 -->
                     <a class="how-block" href="<?php echo base_url(); ?>how" target="_self">
                     <div class="tp-caption   tp-resizeme" 
                        id="how2" 
                        data-x="['center','center','center','center']" data-hoffset="['-200','-150','-118','-102']" 
                        data-y="['top','top','top','top']" data-voffset="['375','277','274','260']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions=''
                        data-responsive_offset="on" 
                        data-frames='[{"delay":"bytrigger","speed":500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"frame":"999","to":"x:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="keep"><img src="<?php echo base_url(); ?>assets/img/banner/howblue.png" alt="how" data-ww="['585px','414px','296px','202px']" data-hh="['218px','154px','110px','74px']" width="1465" height="546" data-no-retina> </div>
                     <!-- LAYER NR. 4 -->
                     <div class="tp-caption   tp-resizeme" 
                        id="how1" 
                        data-x="['center','center','center','center']" data-hoffset="['-200','-150','-118','-102']" 
                        data-y="['top','top','top','top']" data-voffset="['375','277','274','260']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions='[{"event":"mouseenter","action":"stoplayer","layer":"how1","delay":"0"},{"event":"mouseleave","action":"startlayer","layer":"how1","delay":"0"},{"event":"mouseenter","action":"startlayer","layer":"how2","delay":"0"},{"event":"mouseleave","action":"stoplayer","layer":"how2","delay":"0"}]'
                        data-responsive_offset="on" 
                        data-frames='[{"delay":500,"speed":500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"x:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="reset"><img src="<?php echo base_url(); ?>assets/img/banner/howblack.png" alt="how" data-ww="['585px','414px','296px','202px']" data-hh="['218px','154px','110px','74px']" width="1465" height="546" data-no-retina> </div>
                     </a>
                     <!-- LAYER NR. 5 -->
                     <a class="we-block" href="<?php echo base_url(); ?>we" target="_self">
                     <div class="tp-caption   tp-resizeme" 
                        id="we2" 
                        data-x="['center','center','center','center']" data-hoffset="['372','226','-175','-142']" 
                        data-y="['top','top','top','top']" data-voffset="['375','274','412','365']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions=''
                        data-responsive_offset="on" 
                        data-frames='[{"delay":"bytrigger","speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="keep"><img src="<?php echo base_url(); ?>assets/img/banner/wered.png" alt="we" data-ww="['329px','231px','195px','116px']" data-hh="['222px','156px','132px','80px']" width="809" height="546" data-no-retina> </div>
                     <!-- LAYER NR. 6 -->
                     <div class="tp-caption   tp-resizeme" 
                        id="we1" 
                        data-x="['center','center','center','center']" data-hoffset="['372','226','-175','-142']" 
                        data-y="['top','top','top','top']" data-voffset="['375','275','412','365']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions='[{"event":"mouseenter","action":"stoplayer","layer":"we1","delay":"0"},{"event":"mouseleave","action":"startlayer","layer":"we1","delay":"0"},{"event":"mouseenter","action":"startlayer","layer":"we2","delay":"0"},{"event":"mouseleave","action":"stoplayer","layer":"we2","delay":"0"}]'
                        data-responsive_offset="on" 
                        data-frames='[{"delay":1000,"speed":500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"x:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="reset"><img src="<?php echo base_url(); ?>assets/img/banner/weblack.png" alt="we" data-ww="['329px','231px','195px','116px']" data-hh="['222px','156px','132px','80px']" width="809" height="546" data-no-retina> </div>
                     </a>
                     <!-- LAYER NR. 7 -->
                     <a class="doit-block" href="<?php echo base_url(); ?>doit" target="_self">
                     <div class="tp-caption   tp-resizeme" 
                        id="doit2" 
                        data-x="['center','center','center','center']" data-hoffset="['0','-23','-32','-56']" 
                        data-y="['top','top','top','top']" data-voffset="['635','455','574','473']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions=''
                        data-responsive_offset="on" 
                        data-frames='[{"delay":"bytrigger","speed":500,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"bytrigger","speed":500,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="keep"><img src="<?php echo base_url(); ?>assets/img/banner/doitblue.png" alt="do it" data-ww="['1077px','693px','499px','301px']" data-hh="['250px','161px','116px','70px']" width="2424" height="562" data-no-retina> </div>
                     <!-- LAYER NR. 8 -->
                     <div class="tp-caption   tp-resizeme" 
                        id="doit1" 
                        data-x="['center','center','center','center']" data-hoffset="['0','-23','-32','-56']" 
                        data-y="['top','top','top','top']" data-voffset="['635','455','574','473']" 
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="image" 
                        data-actions='[{"event":"mouseenter","action":"stoplayer","layer":"doit1","delay":"0"},{"event":"mouseleave","action":"startlayer","layer":"doit1","delay":"0"},{"event":"mouseenter","action":"startlayer","layer":"doit2","delay":" 0"},{"event":"mouseleave","action":"stoplayer","layer":"doit2","delay":"0"}]'
                        data-responsive_offset="on" 
                        data-frames='[{"delay":1500,"speed":500,"frame":"0","from":"x:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"x:50px;opacity:0;","ease":"Power3.easeInOut"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        data-lasttriggerstate="reset"><img src="<?php echo base_url(); ?>assets/img/banner/doitblack.png" alt="do it" data-ww="['1077px','693px','499px','301px']" data-hh="['250px','161px','116px','70px']" width="2424" height="562" data-no-retina> </div>
                     </a>
                     <!-- LAYER NR. 10 -->
                     <a class="tp-caption rev-btn" id="slide-101-layer-16"
                        href="<?php echo base_url(); ?>contact"
                        data-x="['left','left','left','center']" data-hoffset="['50','31','26','0']" 
                        data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','25','22','70']" 
                        data-width="none"
                        data-height="none"
                        data-fontsize="['24','20','20','20']"
                        data-responsive="on"
                        data-whitespace="nowrap"
                        data-type="button" 
                        data-responsive_offset="on" 
                        data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,228,98);bg:rgba(2,2,2,0.8);bc:rgb(255,228,98);bw:2 2 2 2;"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[32,22,22,12]"
                        data-paddingright="[35,35,35,35]"
                        data-paddingbottom="[32,22,22,12]"
                        data-paddingleft="[35,35,35,35]"
                        style="z-index: 17; white-space: nowrap; font-size: 16px; line-height: 17px; font-weight: 500; color: #222; letter-spacing: 0px;font-family:aktiv-grotesk;background-color:rgba(0,0,0,0);border-color:rgb(2,2,2);border-style:solid;border-width:4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Contact Us</a>
                     <!-- LAYER NR. 11 -->
                     <a class="tp-caption rev-btn " 
                        href="<?php echo base_url(); ?>career" target="_self" id="slide-101-layer-15" 
                        data-x="['right','right','right','center']" data-hoffset="['50','31','26','0']" 
                        data-y="['bottom','bottom','bottom','bottom']" data-voffset="['50','25','22','0']" 
                        data-width="none"
                        data-height="none"
                        data-fontsize="['24','20','20','20']"
                        data-responsive="on"
                        data-whitespace="nowrap"
                        data-type="button" 
                        data-actions=''
                        data-responsive_offset="on" 
                        data-frames='[{"delay":2000,"speed":1000,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,228,98);bg:rgba(2,2,2,0.8);bc:rgb(255,228,98);bw:2 2 2 2;"}]'
                        data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[32,22,22,12]"
                        data-paddingright="[35,35,35,35]"
                        data-paddingbottom="[32,22,22,12]"
                        data-paddingleft="[35,35,35,35]"
                        style="z-index: 18; white-space: nowrap; font-size: 16px; line-height: 17px; font-weight: 500; color: #222; letter-spacing: 0px;font-family:aktiv-grotesk;background-color:rgba(0,0,0,0);border-color:rgba(2,2,2,1);border-style:solid;border-width:4px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;text-decoration: none;">Join Our Team</a>
                  </li>
               </ul>
               <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
            </div>
         </div>
         <!-- END REVOLUTION SLIDER -->
      </section>