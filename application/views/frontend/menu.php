<!-- menu -->
      <!-- navigation -->
      <nav class="navbar fixed-top">
         <span class="menu-btn" style="font-size:30px;cursor:pointer" onclick="openNav()"><img src="<?php echo base_url(); ?>assets/img/menu-white/burger.png" alt="menu"></span>
         <div id="mySidenav" class="sidenav dropdown">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
            <img src="<?php echo base_url(); ?>assets/img/menu-white/mankindwhitetypo.png" class="logo-sidemenu" alt="mankind">
            <div class="menu-left">
               <a class="dropdown-item <?php if($base == 'home'): ?> active <?php endif; ?>" href="<?php echo base_url(); ?>">Home</a>
               <a class="dropdown-item <?php if($base == 'thisis'): ?> active <?php endif; ?>" href="<?php echo base_url(); ?>thisis">About</a>
               <a class="dropdown-item <?php if($base == 'how'): ?> active <?php endif; ?>" href="<?php echo base_url(); ?>how">Services</a>
               <a class="dropdown-item <?php if($base == 'we'): ?> active <?php endif; ?>" href="<?php echo base_url(); ?>we">The Team</a>
               <a class="dropdown-item <?php if($base == 'doit'): ?> active <?php endif; ?>" href="<?php echo base_url(); ?>doit">Works</a>
               <a class="dropdown-item <?php if($base == 'career'): ?> active <?php endif; ?>" href="<?php echo base_url(); ?>career">Career</a>
               <a class="dropdown-item <?php if($base == 'contact'): ?> active <?php endif; ?>" href="<?php echo base_url(); ?>contact">Contact Us</a>
            </div>
            <div class="menu-left2">
               <div class="col-md-6 footer-menu">
               <div class="font-raleway" id="footer-heading">Find Us!</div>
                  <ul class="links-list a-text-cont a-text-main-cont font-raleway" id="footer-link">
                     <li>
                        <p class="txt-footer2">Kindo Square C9
                           <br>
                           Jl. Duren Tiga Raya No. 101.
                           <br>
                           Duren Tiga, Pancoran, Jakarta Selatan, DKI
                           Jakarta 12760
                           <br>
                           Phone : (+62 21) 2753 2278
                           <br>
                           Fax : (+62 21) 7919 7510
                        </p>
                     </li>
                  </ul>
               </div>
               <div class="col-md-6 widget">
               <?php
                     if($this->linkedin->config_value != ""){
                       $linkedin = $this->linkedin->config_value;
                     } else {
                       $linkedin = 'Linkedin';
                     }
               ?>
               <?php
                     if($this->instagram->config_value != ""){
                       $instagram = $this->instagram->config_value;
                     } else {
                       $instagram = 'Instagram';
                     }
               ?>
                  <a class="socmed-menu2 socmed3" href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                  <a class="socmed-menu2 socmed3" href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
               </div>
            </div>
         </div>
         <?php
           if($this->logo->config_value != ""){
             $media = $this->m_media->get_media_id($this->logo->config_value);
             $thumbpath = pathinfo($media->media_name);
             $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
           } else {
             $image = base_url().'assets/img/logobymankind.png';
           }
         ?>
         <a class="navbar-brand mx-auto" href="<?php echo base_url(); ?>"><img src="<?php echo $image; ?>" class="logo-img" alt="logo mankind"></a>
         <div class="overlay2"></div>
      </nav>
      <!-- end navigation -->
      <!-- end menu -->