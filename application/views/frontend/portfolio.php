<section id="section-title">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="title-page text-center">
                     <img src="<?php echo base_url(); ?>assets/img/doit/doitlikeus.png" class="img-title5">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <div class="mankind-title">
               <h1 class="font-title"><?php echo $detail->portfolio_title; ?></h1>
            </div>
         </div>
         <div class="container">
            <div class="banner-portfolio">
               <?php if($detail->portfolio_is_thumbnail == 1): ?>
                                    <?php
                                      if($detail->portfolio_thumbnail){
                                        $media = $this->m_media->get_media_id($detail->portfolio_thumbnail);
                                       $thumbpath = pathinfo($media->media_name);
                                       $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                                      } else {
                                        $image = 'https://via.placeholder.com/1920x1080';
                                      }
                                   ?>
               <img src="<?php echo $image; ?>" class="img-responsive" alt="invasion">
               <?php endif; ?>
            </div>
         </div>
         <div class="container">
            <div class="col-md-8">
               <div class="txt-port">
                  <h2>Business Challenge</h2>
                     <?php echo $detail->portfolio_content; ?>
               </div>
               <div class="txt-port">
                  <h2>Our Creative Solution</h2>
                     <?php echo $detail->portfolio_content_2; ?>
               </div>
               <div class="txt-port">
                  <a href="<?php echo $detail->portfolio_link; ?>" class="btn-submit">Visit their website</a>
               </div>
            </div>
            <div class="col-md-4">
               <div class="txt-port">
                  <h2>Our Services</h2>
                  <?php echo $detail->portfolio_content_3; ?>
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold2">
         <div class="container">
            <div class="col-md-12 text-center div-kv">
               <?php if($detail->portfolio_is_thumbnail == 1): ?>
                                    <?php
                                      if($detail->portfolio_thumbnail_2){
                                        $media = $this->m_media->get_media_id($detail->portfolio_thumbnail_2);
                                       $thumbpath = pathinfo($media->media_name);
                                       $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                                      } else {
                                        $image = 'https://via.placeholder.com/1920x1080';
                                      }
                                   ?>
               <img src="<?php echo $image; ?>" class="img-kv" alt="invasion">
               <?php endif; ?>
               <p><?php echo $detail->portfolio_content_4; ?></p>
            </div>
            <div class="col-md-12 text-center div-kv">
               <?php if($detail->portfolio_is_thumbnail == 1): ?>
                                    <?php
                                      if($detail->portfolio_thumbnail_3){
                                        $media = $this->m_media->get_media_id($detail->portfolio_thumbnail_3);
                                       $thumbpath = pathinfo($media->media_name);
                                       $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                                      } else {
                                        $image = 'https://via.placeholder.com/1920x1080';
                                      }
                                   ?>
               <img src="<?php echo $image; ?>" class="img-kv" alt="invasion">
               <?php endif; ?>
               <p><?php echo $detail->portfolio_content_5; ?></p>
            </div>
            <div class="col-md-12 text-center div-kv">
               <?php if($detail->portfolio_is_thumbnail == 1): ?>
                                    <?php
                                      if($detail->portfolio_thumbnail_4){
                                        $media = $this->m_media->get_media_id($detail->portfolio_thumbnail_4);
                                       $thumbpath = pathinfo($media->media_name);
                                       $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                                      } else {
                                        $image = 'https://via.placeholder.com/1920x1080';
                                      }
                                   ?>
               <img src="<?php echo $image; ?>" class="img-kv" alt="invasion">
               <?php endif; ?>
               <p><?php echo $detail->portfolio_content_6; ?></p>
            </div>
            <div class="col-md-12 text-center div-kv">
               <div class="col-md-6">
                  <?php if($detail->portfolio_is_thumbnail == 1): ?>
                                    <?php
                                      if($detail->portfolio_thumbnail_5){
                                        $media = $this->m_media->get_media_id($detail->portfolio_thumbnail_5);
                                       $thumbpath = pathinfo($media->media_name);
                                       $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                                      } else {
                                        $image = 'https://via.placeholder.com/1920x1080';
                                      }
                                   ?>
                  <img src="<?php echo $image; ?>" class="img-kv" alt="invasion">
                  <?php endif; ?>
               </div>
               <div class="col-md-6">
                  <?php if($detail->portfolio_is_thumbnail == 1): ?>
                                    <?php
                                      if($detail->portfolio_thumbnail_6){
                                        $media = $this->m_media->get_media_id($detail->portfolio_thumbnail_6);
                                       $thumbpath = pathinfo($media->media_name);
                                       $image = base_url().'asset_admin/assets/uploads/media/image/original/'.$thumbpath['filename'].'.'.$thumbpath['extension'];
                                      } else {
                                        $image = 'https://via.placeholder.com/1920x1080';
                                      }
                                   ?>
                  <img src="<?php echo $image; ?>" class="img-kv" alt="invasion">
                  <?php endif; ?>
               </div>
               <p><?php echo $detail->portfolio_content_7; ?></p>
            </div>
         </div>
      </section>