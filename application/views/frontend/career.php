<section id="section-title">
         <div class="container">
            <div class="row">
               <div class="col-md-12 wow fadeIn">
                  <div class="title-page text-center">
                     <img src="<?php echo base_url(); ?>assets/img/career/wearehiring.png" class="img-title4" alt="we are hiring">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="about">
         <div class="page-section">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 text-center">
                     <?php
                           if($this->career_1->post_content != ""){
                             $career_1 = $this->career_1->post_content;
                           } else {
                             $career_1 = 'lorem ipsum';
                           }
                     ?>
                     <div class="fes2-title-40 title-section3">
                        <?php echo $career_1; ?>
                     </div>
                  </div>
               </div>   
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <div class="col-md-12 text-center">
               <h2 class="title-career-page">
                 Current Openings 
               </h2>
               <?php
                           if($this->career_2->post_content != ""){
                             $career_2 = $this->career_2->post_content;
                           } else {
                             $career_2 = 'lorem ipsum';
                           }
                     ?>
               <p class="txt-career-page">
                  <?php echo $career_2; ?>
               </p>
            </div>
         </div>
      <div class="container">
      <div class="col-md-12">

<?php foreach($career as $item): ?>
         <div class="panel-group" id="faqAccordion">
            <div class="panel panel-default wow fadeIn">
               <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#faqAccordion" data-target="#<?php echo $item['career_id']; ?>">
                  <h1 class="role-title"><?php echo $item['career_title']; ?></h1>
                  <a class="btn-submit inline-btn">Job Details</a>
               </div>
               <div id="<?php echo $item['career_id']; ?>" class="panel-collapse collapse" style="height: 0px;">
                  <div class="panel-body">
                     <p>
                        <?php echo $item['career_content']; ?>
                     </p>
                     <div class="divider-career">
                     <a class="btn-submit" href="<?php echo $item['career_link']; ?>">Apply</a>
                  </div>
                  </div>
               </div>
            </div>
         </div>
<?php endforeach;?>
      </div>
      </div>
      </section>