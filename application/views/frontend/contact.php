<section id="section-title">
         <div class="container">
            <div class="col-md-12 wow fadeIn">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-red">GET IN TOUCH</span><br><span class="font-red">WITH US</span></h1>
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <div class="col-md-4 wow fadeIn">
               <img src="<?php echo base_url(); ?>assets/img/contact/photocontact.png" class="img-responsive" alt="mail box">
            </div>
            <div class="col-md-8 form-div">
               <form method="POST">
                  <div class="form-contact">
                     <div class="col-md-6">
                        <div class="form-group">
                         <label for="inputFirstname" class="contact-detail">Name</label>
                         <input type="text" name="name" class="form-control input2" id="name" required="">
                         <?php echo form_error('name', '<span style="color: red;">', '</span>'); ?>
                       </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                         <label for="name" class="contact-detail">Company</label>
                         <input type="text" name="company" class="form-control input2" id="company" required="">
                         <?php echo form_error('company', '<span style="color: red;">', '</span>'); ?>
                       </div>
                     </div>
                     <div class="col-md-12">
                       <div class="form-group">
                         <label for="email" class="contact-detail">Email</label>
                         <input type="email" name="email" class="form-control input2" id="email" required="">
                         <?php echo form_error('email', '<span style="color: red;">', '</span>'); ?>
                       </div>
                     </div>
                     <div class="col-md-12">
                        <div class="form-group">
                         <label for="phone" class="contact-detail">Tell us about yourself</label>
                           <textarea placeholder="" name="message" class="form-control input3" id="message"></textarea>
                           <?php echo form_error('message', '<span style="color: red;">', '</span>'); ?>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <button type="submit" name="submit" value="Submit" class="btn-submit">Get in touch!</button>
                     </div>
                 </div>
               </form>
            </div>
         </div>
      </section>
      <section id="section-fold-blue">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-white">LET'S JUST MEET</span><br><span class="font-white">IN PERSON</span></h1>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="col-md-9">
               <div id="map">
                  <div data-address="Mankind Jakarta" id="google-map">
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div id="map">
                  <h2 class="txt-footer3">Mankind Jakarta</h2>
                     <p class="txt-footer4">
                        <b>Kindo Square C9</b>
                        <br>
                        Jl. Duren Tiga Raya No. 101.
                        <br>
                        Duren Tiga, Pancoran, Jakarta Selatan, DKI
                        Jakarta 12760
                        <br>
                        Phone : (+62 21) 2753 2278
                        <br>
                        Fax : (+62 21) 7919 7510
                     </p>
               </div>
            </div>
         </div>
      </section>
      <section id="section-last">
         <div class="container">
            <div class="row">
               <div class="col-md-3 col-sm-3 widget" id="footer-cont">
                  <h2 class="title-list">New Business</h2>
                  <ul class="links-list a-text-cont a-text-main-cont font-raleway">
                     <li>
                        <p class="txt-footer"><b>Doni Hendano</b>
                           <br>
                           <a class="link-career-list" href="mailto:doni@bymankind.com">doni@bymankind.com</a>
                           <br>
                           <br>
                           <b>Info Bymankind</b>
                           <br>
                           <a class="link-career-list" href="mailto:info@bymankind.com">info@bymankind.com</a>
                        </p>
                     </li>
                  </ul>
               </div>
               <div class="col-md-3 col-sm-3 widget" id="footer-cont">
                  <h2 class="title-list">Press Inquiries</h2>
                  <ul class="links-list a-text-cont a-text-main-cont font-raleway">
                     <li>
                        <p class="txt-footer"><b>Nisrina</b>
                           <br>
                           <a class="link-career-list" href="mailto:nisrina@bymankind.com">nisrina@bymankind.com</a>
                        </p>
                     </li>
                  </ul>
               </div>
               <div class="col-md-3 col-sm-3 widget" id="footer-cont">
                  <h2 class="title-list">Career</h2>
                  <ul class="links-list a-text-cont a-text-main-cont font-raleway">
                     <li>
                        <p class="txt-footer"><b>Career</b>
                           <br>
                           <a class="link-career-list" href="mailto:career@bymankind.com">career@bymankind.com</a>
                        </p>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>