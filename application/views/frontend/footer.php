<?php if($base != 'home'): ?>
            <footer id="footer-mankind" class="page-section">
               <div class="container">
                  <div class="row">
                     <div class="col-md-4 col-sm-4 widget" id="footer-cont">
                        <div class="font-raleway" id="footer-heading">Main Office</div>
                        <ul class="links-list a-text-cont a-text-main-cont font-raleway" id="footer-link">
                           <li>
                              <p class="txt-footer1">Mankind Jakarta</p>
                              <p class="txt-footer2">Kindo Square C9
                                 <br>
                                 Jl. Duren Tiga Raya No. 101.
                                 <br>
                                 Duren Tiga, Pancoran, Jakarta Selatan, DKI
                                 Jakarta 12760
                                 <br>
                                 Phone : (+62 21) 2753 2278
                                 <br>
                                 Fax : (+62 21) 7919 7510
                              </p>
                           </li>
                        </ul>
                     </div>
                      <div class="col-md-4 col-sm-4 widget" id="footer-cont">
                        <div class="font-raleway" id="footer-heading">Sitemap</div>
                        <ul class="links-list a-text-cont a-text-main-cont font-raleway link-footer" id="footer-link">
                           <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                           <li><a href="<?php echo base_url(); ?>thisis">ABOUT</a></li>
                           <li><a href="<?php echo base_url(); ?>how">SERVICES</a></li>
                           <li><a href="<?php echo base_url(); ?>we">THE TEAM</a></li>
                           <li><a href="<?php echo base_url(); ?>doit">WORKS</a></li>
                        </ul>
                        <ul class="links-list a-text-cont a-text-main-cont font-raleway link-footer" id="footer-link">
                           <li><a href="<?php echo base_url(); ?>career">CAREER</a></li>
                           <li><a href="<?php echo base_url(); ?>contact">CONTACT US</a></li>
                        </ul>
                     </div>
                     <div class="col-md-4 col-sm-4 widget" id="footer-cont">
                        <div class="font-raleway" id="footer-heading">Find Us</div>
                        <?php
                              if($this->linkedin->config_value != ""){
                                $linkedin = $this->linkedin->config_value;
                              } else {
                                $linkedin = 'Linkedin';
                              }
                        ?>
                        <?php
                              if($this->instagram->config_value != ""){
                                $instagram = $this->instagram->config_value;
                              } else {
                                $instagram = 'Instagram';
                              }
                        ?>
                        <a class="socmed-menu2" href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <a class="socmed-menu2" href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                     </div>
                     <div class="col-md-12 text-center">
                        <div class="copyright-txt">COPYRIGHT @ 2016 - 2019 BY MANKIND JAKARTA</div>
                     </div>
                  </div>
               </div>
            </footer>
<?php endif; ?>
      <!-- BACK TO TOP -->
      <p id="back-top">
         <a href="#top" title="Back to Top"><i class="fa fa-angle-double-up" style="font-size:50px"></i></a>
      </p>
      <!-- jQuery  -->
      <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
      <!-- PORTFOLIO SCRIPTS -->
      <script src="<?php echo base_url(); ?>assets/js/isotope.pkgd.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/imagesloaded.pkgd.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/simple-lightbox.js"></script>
      <script src="https://cdn.rawgit.com/nnattawat/flip/master/dist/jquery.flip.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery.nav.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery.stellar.min.js"></script>
      <!-- APPEAR -->    
      <script src="<?php echo base_url(); ?>assets/js/jquery.appear.js"></script>  
      <!-- MAIN SCRIPT -->
      <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
      <!-- GOOLE MAP 
      !!! To setup Google Maps, please, see the documentation !!! --> 
      <script src="https://maps.google.com/maps/api/js?key=AIzaSyByLU1FwXomxyQoPCFIdc_7KPFbGq5DvUw
      "></script>
      <script src="<?php echo base_url(); ?>assets/js/gmap3.min.js"></script>
      <!-- REVOLUTION JS FILES -->
      <script src="<?php echo base_url(); ?>assets/js/revslider.js"></script>
      <script>
         var icon_map = '<?php echo base_url(); ?>assets/img/loc-marker.png';
      </script>