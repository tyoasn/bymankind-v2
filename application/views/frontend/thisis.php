
      <section id="section-title">
         <div class="container">
            <div class="row">
               <div class="col-md-12  wow fadeIn">
                  <div class="title-page text-center">
                     <img src="<?php echo base_url(); ?>assets/img/thisis/thisismankind.png" class="img-title" alt="this is">
                  </div>
                  <div class="title-page text-center">
                     <img src="<?php echo base_url(); ?>assets/img/thisis/mankindis.png" class="img-title4" alt="mankind is">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="about">
         <div class="page-section">
            <div class="container">
               <div class="row">
                  <div class="col-md-12 text-center">
                     <?php
                           if($this->about_1->post_content != ""){
                             $about_1 = $this->about_1->post_content;
                           } else {
                             $about_1 = 'lorem ipsum';
                           }
                     ?>
                     <div class="fes2-title-40 title-section2">
                        <?php echo $about_1; ?>
                     </div>
                  </div>
               </div>   
            </div>
            <div class="container">
                  <div class="col-md-4">
                     <div class="fes2-main-text-cont">
                         <blockquote class="testimonial-3">
                           <p class="font-quote">Great things in businesss are never done by one person;<br><span class="font-black2">they're done by a team of people.</span></p>
                           <br>
                           <p class="fes2-title-40">- Steve Jobs</p>
                        </blockquote>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="fes2-main-text-cont wow fadeInUp">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/photomankindoffice.png" class="" alt="mankind office">
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="fes2-main-text-cont img-inline wow fadeInRight">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/futurerobot.png" class="img-future" alt="future robot">
                        <p class="font-quote2">We want to make people excited<br><span class="font-black2">about the future!</span></p>
                     </div>
                  </div>
            </div>
         </div>
      </section>
      <section id="section-fold-blue">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-white">WHO</span><br><span class="font-white">WE ARE</span></h1>
               </div>
            </div>
            <div class="col-md-5">
               <div class="div-culture">
                  <img src="<?php echo base_url(); ?>assets/img/thisis/culturalfrontier.png" class="img-responsive" alt="cultural frontier">
               </div>
               <?php
                           if($this->about_2->post_content != ""){
                             $about_2 = $this->about_1->post_content;
                           } else {
                             $about_2 = 'lorem ipsum';
                           }
                     ?>
               <div class="txt-section-white">
                  <?php echo $about_2; ?>
               </div>
            </div>
            <div class="col-md-7 wow fadeInRight">
               <div class="div-img-bg text-right">
                  <img src="<?php echo base_url(); ?>assets/img/thisis/photocultural.png" class="img-back" alt="photo cultural">
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-red">THE COLLABORATORS</span><br><span class="font-red">WE'VE WORKED WITH</span></h1>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="col-md-3 panel-artist">
               <div class="tab-content">
                  <div id="tab1" class="tab-pane active">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab1.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jason Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab2" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab2.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jeanette Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab3" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab3.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jocasta Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
               </div>
               <div id="tabs">
                  <ul class="nav nav-tabs nav-stacked nav-tab-artist">
                     <li class="active">
                        <a class="nav-item" data-toggle="tab" href="#tab1">
                           <h2 class="artist-name1">Jason Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab2">
                           <h2 class="artist-name1">Jeanette Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab3">
                           <h2 class="artist-name1">Jocasta Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="col-md-3 panel-artist">
               <div class="tab-content">
                  <div id="tab4" class="tab-pane active">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab1.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jason Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab5" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab2.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jeanette Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab6" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab3.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jocasta Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
               </div>
               <div id="tabs">
                  <ul class="nav nav-tabs nav-stacked nav-tab-artist">
                     <li class="active">
                        <a class="nav-item" data-toggle="tab" href="#tab4">
                           <h2 class="artist-name1">Jason Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab5">
                           <h2 class="artist-name1">Jeanette Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab6">
                           <h2 class="artist-name1">Jocasta Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="col-md-3 panel-artist">
               <div class="tab-content">
                  <div id="tab7" class="tab-pane active">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab1.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jason Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab8" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab2.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jeanette Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab9" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab3.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jocasta Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
               </div>
               <div id="tabs">
                  <ul class="nav nav-tabs nav-stacked nav-tab-artist">
                     <li class="active">
                        <a class="nav-item" data-toggle="tab" href="#tab7">
                           <h2 class="artist-name1">Jason Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab8">
                           <h2 class="artist-name1">Jeanette Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab9">
                           <h2 class="artist-name1">Jocasta Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
            <div class="col-md-3 panel-artist">
               <div class="tab-content">
                  <div id="tab10" class="tab-pane active">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab1.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jason Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab11" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab2.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jeanette Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
                  <div id="tab12" class="tab-pane">
                     <div class="panel-img">
                        <img src="<?php echo base_url(); ?>assets/img/thisis/collab3.png" class="img-collab" alt="mankind collab">
                        <h2 class="artist-name2">Jocasta Doe</h2>
                        <h4 class="artist-title2">Mural Artist</h4>
                        <div class="overlay3"></div>
                     </div>
                  </div>
               </div>
               <div id="tabs">
                  <ul class="nav nav-tabs nav-stacked nav-tab-artist">
                     <li class="active">
                        <a class="nav-item" data-toggle="tab" href="#tab10">
                           <h2 class="artist-name1">Jason Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab11">
                           <h2 class="artist-name1">Jeanette Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                     <li class="">
                        <a class="nav-item" data-toggle="tab" href="#tab12">
                           <h2 class="artist-name1">Jocasta Doe</h2>
                           <h4 class="artist-title1">Mural Artist</h4>
                        </a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-red">OUR</span><br><span class="font-red">CLIENTS</span></h1>
               </div>
            </div>
         </div>
         <div class="container col-client">
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/rbt.png" class="img-client" alt="rbt">
            </div>
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/supersoccer.png" class="img-client" alt="supersoccer">
            </div>
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/smid.png" class="img-client" alt="supermusic">
            </div>
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/alcatel.png" class="img-client" alt="alcatel">
            </div>
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/danamon.png" class="img-client" alt="danamon">
            </div>
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/djalin.png" class="img-client" alt="djalin">
            </div>
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/kemenpar.png" class="img-client" alt="kemenpar">
            </div>
            <div class="col-sm-3 div-logo">
               <img src="<?php echo base_url(); ?>assets/img/thisis/logo/satset.png" class="img-client" alt="satset">
            </div>
         </div>
      </section>
      <section id="section-fold-blue">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-white">TEAM</span><br><span class="font-white">RECOGNITION</span></h1>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="col-md-3 div-logo2">
               <img src="<?php echo base_url(); ?>assets/img/thisis/teamrecog.png" class="img-recog" alt="team recognition">
            </div>
            <div class="col-md-3 div-logo2">
               <img src="<?php echo base_url(); ?>assets/img/thisis/teamrecog.png" class="img-recog" alt="team recognition">
            </div>
            <div class="col-md-3 div-logo2">
               <img src="<?php echo base_url(); ?>assets/img/thisis/teamrecog.png" class="img-recog" alt="team recognition">
            </div>
            <div class="col-md-3 div-logo2">
               <img src="<?php echo base_url(); ?>assets/img/thisis/teamrecog.png" class="img-recog" alt="team recognition">
            </div>
         </div>
      </section>
      <section id="section-fold">
         <div class="last-fold">
         <div class="container">
            <div class="col-md-12">
               <div class="mankind-title">
                  <h1 class="font-title"><span class="font-stroke-red">WHAT IS LIKE TO BE</span><br><span class="font-red">WORKING WITH US</span></h1>
               </div>
            </div>
            <div class="col-md-6">
               <div class="div-personalized text-center">
               <img src="<?php echo base_url(); ?>assets/img/thisis/personalized.png" class="img-personalized" alt="personalized">
            </div>
               <img src="<?php echo base_url(); ?>assets/img/thisis/photopersonalized.png" alt="photo personalized">
            </div>
            <?php
                           if($this->about_3->post_content != ""){
                             $about_3 = $this->about_3->post_content;
                           } else {
                             $about_3 = 'lorem ipsum';
                           }
                     ?>
            <div class="col-md-6 div-txt-personalized">
               <p>
                  <?php echo $about_3; ?>
               </p>
            </div>
         </div>
      </div>
      </section>