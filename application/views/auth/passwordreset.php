<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/img/favicon.png">
    
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>asset_admin/css/main.css">
    <title>Kementerian Pariwisata RI</title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--if lt IE 9
    script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
    script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
    -->
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1></h1>
        <img src="<?php echo base_url(); ?>asset_admin/images/logo-smid-invasion-md.png" width="300px" alt=""/>
      </div>
      <div class="login-box">
        <form class="login-form" action="" method="post">
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Forgot Password</h3>
          <div class="form-group">
                <label class="control-label">PASSWORD</label>
                <input class="form-control" type="password" name="password" placeholder="Password*" required autofocus>
            </div>
            <div class="form-group">
                <label class="control-label">CONFIRM PASSWORD</label>
                <input class="form-control" type="password" name="confirmpassword" placeholder="Confirm Password*" required>
            </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block" name="submit" type="submit" value="submit">CHANGE PASSWORD <i class="fa fa-sign-in fa-lg"></i></button>
          </div>
        </form>
      </div>
    </section>
  </body>
  <script src="<?php echo base_url(); ?>asset_admin/js/jquery-2.1.4.min.js"></script>
  <script src="<?php echo base_url(); ?>asset_admin/js/essential-plugins.js"></script>
  <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url(); ?>asset_admin/js/plugins/pace.min.js"></script>
  <script src="<?php echo base_url(); ?>asset_admin/js/main.js"></script>
</html>