<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="{{URL::asset('/assets/img/favicon.png')}}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Sign in</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>asset_admin/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>asset_admin/css/main.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>asset_admin/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>asset_admin/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Light Bootstrap Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>asset_admin/css/main.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>asset_admin/css/pe-icon-7-stroke.css" rel="stylesheet"/>
</head>
<body> 

<div class="wrapper wrapper-full-page">
    <div class="full-page login-page" data-color="white">
        <div class="content">
            <div class="container">
                <div class="row">
                <div class="col-md-12">
                    <div class="logo-login">
                      <a href="#"><img src="<?php echo base_url(); ?>asset_admin/img/logobymankind.png" class="logo-img2" alt="Logo"></a>
                    </div>
                </div>                
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <?php $attributes = array('class' => 'login-form'); ?>
                        <?php echo form_open('auth/auth/check', $attributes); ?>
                            <div class="card card-login">
                                <div class="header2 text-center">Sign in</div>
                                <div class="content">
                                    <?php if ($this->session->flashdata('error')) : ?>
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <stonrg><?php echo $this->session->flashdata('error'); ?></stonrg>
                                        </div>
                                      <?php endif ?>
                                      <?php if ($this->session->flashdata('success')) : ?>
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <stonrg><?php echo $this->session->flashdata('success'); ?></stonrg>
                                        </div>
                                      <?php endif ?>
                                    <div class="form-group">
                                        <label class="label-login">Username</label>
                                        <input type="text" placeholder="Enter Username" class="form-control" name="username" autocomplete="off" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="label-login">Password</label>
                                        <input type="password" placeholder="Enter Password" class="form-control" name="password" autocomplete="off" required>
                                    </div>                                    
                                    <div class="form-group">
                                        <label class="checkbox label-login">
                                             <input id="checkbox" type="checkbox" data-toggle="checkbox" value="1" name="remember">
                                            Remember Me
                                        </label>    
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button class="btn btn-fill btn-danger btn-wd btn-login" type="submit">Sign in</button>
                                </div>
                            </div> 
                        <?php echo form_close(); ?>
                    </div>                    
                </div>
            </div>
            <div class="copyright-mankind">
                <p>&copy; 2019 <a href="https://www.bymankind.com">Mankind</a></p>
            </div>
        </div>
    </div>                                   
</div>
</body>
    <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
    <script src="<?php echo base_url(); ?>asset_admin/js/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>asset_admin/js/jquery-ui.min.js" type="text/javascript"></script> 
	<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap.min.js" type="text/javascript"></script>
	<!--  Forms Validations Plugin -->
	<script src="<?php echo base_url(); ?>asset_admin/js/jquery.validate.min.js"></script>
	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="<?php echo base_url(); ?>asset_admin/js/moment.min.js"></script>
    <!--  Date Time Picker Plugin is included in this js file -->
    <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-datetimepicker.js"></script>
    <!--  Select Picker Plugin -->
    <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-selectpicker.js"></script>
	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-checkbox-radio-switch-tags.js"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-notify.js"></script>
    <!-- Sweet Alert 2 plugin -->
	<script src="<?php echo base_url(); ?>asset_admin/js/sweetalert2.js"></script>
    <!-- Vector Map plugin -->
	<script src="<?php echo base_url(); ?>asset_admin/js/jquery-jvectormap.js"></script>
	<!-- Wizard Plugin    -->
    <script src="<?php echo base_url(); ?>asset_admin/js/jquery.bootstrap.wizard.min.js"></script>
    <!--  Datatable Plugin    -->
    <script src="<?php echo base_url(); ?>asset_admin/js/bootstrap-table.js"></script>
    <!--  Full Calendar Plugin    -->
    <script src="<?php echo base_url(); ?>asset_admin/js/fullcalendar.min.js"></script>
    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="<?php echo base_url(); ?>asset_admin/js/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/background.js"></script>
    <input type="text" id="msg" value="#" readonly hidden>
    <script type="text/javascript" src="<?php echo base_url(); ?>asset_admin/js/success_n.js"></script>
    <input type="text" id="msg" value="#" readonly hidden>
    <script type="text/javascript" src="#"></script>
    <script>
        $('#checkbox').prop('checked', false).change()
    </script>
</html>