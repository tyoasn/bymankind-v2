<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function hashtagRemove($string){
    $noHashTag = str_replace('#', '', $string);
    $noHashTag = preg_replace('/#([\w-]+)/i', '', $string);
    return $noHashTag;
}

function hashtagShow($string){
    $matches = array();
    preg_match_all('/#\S*\w/i', $string, $matches);
    return explode('#', $matches[0][0])[1];
}

function read_more($title){
    if(strlen($title) > 45){
        // cut string when reach 45 character
        $stringCut = substr($title, 0, 45);
    
        // strrpos -> find the position of the last occurrence of second parameter (' ') inside the string:
        $title = substr($stringCut, 0, strrpos($stringCut, ' ')) . "...";
    }

    return $title;
}

function read_more_title($title){
    if(strlen($title) > 55){
        // cut string when reach 55 character
        $stringCut = substr($title, 0, 55);
    
        // strrpos -> find the position of the last occurrence of second parameter (' ') inside the string:
        $title = substr($stringCut, 0, strrpos($stringCut, ' ')) . "...";
    }

    return $title;
}

function excerpt($content){
    $title = strip_tags($content);
    if(strlen($title) > 260){
        // cut string when reach 260 character
        $stringCut = substr($title, 0, 260);
    
        // strrpos -> find the position of the last occurrence of second parameter (' ') inside the string:
        $title = substr($stringCut, 0, strrpos($stringCut, ' ')) . "...";
    }

    return $title;
}

function dateformat($datetime){
	$time = strtotime($datetime);

    if($_SESSION['lang'] == 2){
        $array_hari = array(1=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
        //Array Bulan
        $array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
    } elseif ($_SESSION['lang'] == 3) {
        $array_hari = array(1=>'Monday','Tuesday','Wednesday','Thursday','Friday', 'Saturday','Sunday');
        //Array Bulan
        $array_bulan = array(1=>'January','February','March', 'April', 'May', 'June','July','August','September','October', 'November','December');
    }
    $hari = $array_hari[date('N', $time)];
    //Format Tanggal
    $tanggal = date ('j', $time);
    $bulan = $array_bulan[date('n', $time)];
    //Format Tahun
    $tahun = date('Y', $time);
    //Format jam dan menit
    $jammenit = date('H:i', $time);
    $jammenitdetik = date('H:i:s', $time);
    //Menampilkan hari dan tanggal
    //$date = $hari . ', ' . $tanggal . ' ' . $bulan . ' ' . $tahun;
    //$date = 'Published at '.$tanggal.' '.$bulan.' '.$tahun;
    //$date = $jammenit.' - '.$bulan.' '.$tahun;
    //$date = $tanggal.'/'.$bulan.'/'.$tahun.' '.$jammenitdetik;
    $date = $hari.', '.$tanggal.' '.$bulan.' '.$tahun;

    return $date;
}

function dateformat2($datetime){
    $time = strtotime($datetime);

    if($_SESSION['lang'] == 2){
        $array_hari = array(1=>'Senin','Selasa','Rabu','Kamis','Jumat', 'Sabtu','Minggu');
        //Array Bulan
        $array_bulan = array(1=>'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus','September','Oktober', 'November','Desember');
    } elseif ($_SESSION['lang'] == 3) {
        $array_hari = array(1=>'Monday','Tuesday','Wednesday','Thursday','Friday', 'Saturday','Sunday');
        //Array Bulan
        $array_bulan = array(1=>'January','February','March', 'April', 'May', 'June','July','August','September','October', 'November','December');
    }
    $hari = $array_hari[date('N', $time)];
    //Format Tanggal
    $tanggal = date ('j', $time);
    $bulan = $array_bulan[date('n', $time)];
    //Format Tahun
    $tahun = date('Y', $time);
    //Format jam dan menit
    $jammenit = date('H:i', $time);
    $jammenitdetik = date('H:i:s', $time);
    //Menampilkan hari dan tanggal
    //$date = $hari . ', ' . $tanggal . ' ' . $bulan . ' ' . $tahun;
    //$date = 'Published at '.$tanggal.' '.$bulan.' '.$tahun;
    //$date = $jammenit.' - '.$bulan.' '.$tahun;
    //$date = $tanggal.'/'.$bulan.'/'.$tahun.' '.$jammenitdetik;
    $date = $tanggal.' '.$bulan.' '.$tahun;

    return $date;
}

function waktu_lalu($time)
{
    $timestamp = strtotime($time);
    $selisih = time() - $timestamp ;
 
    $detik = $selisih ;
    $menit = round($selisih / 60 );
    $jam = round($selisih / 3600 );
    $hari = round($selisih / 86400 );
    $minggu = round($selisih / 604800 );
    $bulan = round($selisih / 2419200 );
    $tahun = round($selisih / 29030400 );
 
    if ($detik <= 60) {
        $waktu = $detik.' seconds ago';
    } else if ($menit <= 60) {
        $waktu = $menit.' minutes ago';
    } else if ($jam <= 24) {
        $waktu = $jam.' hours ago';
    } else if ($hari <= 7) {
        $waktu = $hari.' days ago';
    } else if ($minggu <= 4) {
        $waktu = $minggu.' weeks ago';
    } else if ($bulan <= 12) {
        $waktu = $bulan.' months ago';
    } else {
        $waktu = $tahun.' years ago';
    }
    
    return $waktu;
}

function parse_yturl($url) {
    $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
    preg_match($pattern, $url, $matches);

    return (isset($matches[1])) ? $matches[1] : false;
}

function paginate_function($item_per_page, $current_page, $total_records)
{
    unset($_GET['page']);
    $param = http_build_query($_GET);
    $total_pages = ceil($total_records/$item_per_page);
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination text-center pagination-kemenpar">';
        $right_links    = $current_page + 5;
        $previous       = $current_page - 5; //previous link
        $next           = $current_page + 5; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
            $previous_link = $current_page-1;
            //$pagination .= '<h1><a class="item" href="?page=1" title="First">First  </a></h1>'; //first link
            $pagination .= '<li class="page-item"><a class="page-link prev-pag" href="?'.$param.'&page='.$previous_link.'"><i class="fa fa-angle-left"></i></a></li>'; //previous link
            for($i = $previous; $i < $current_page ; $i++){ //create right-hand side links
                if($i < $current_page && $i > 0){
                    $pagination .= '<li class="page-item"><a class="page-link" href="?'.$param.'&page='.$i.'">'.$i.'</a></li>';
                }
            }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="page-item"><a class="page-link active" href="#">'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="page-item"><a class="page-link active" href="#">'.$current_page.'</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="page-item"><a class="page-link active" href="#">'.$current_page.'</a></li>';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li class="page-item"><a class="page-link" href="?'.$param.'&page='.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){
            $next_link = $current_page+1;
            $pagination .= '<li class="page-item"><a class="page-link prev-pag" href="?'.$param.'&page='.$next_link.'"><i class="fa fa-angle-right"></i></a></li>';
        }
        $pagination .= "</ul>";
    }
    return $pagination; //return pagination links
}

function paginate_function_channel($item_per_page, $current_page, $total_records)
{
    $total_pages = ceil($total_records/$item_per_page);
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<div class="col-sm-12 pagination__block"><ul class="pagination">';
        $right_links    = $current_page + 5;
        $previous       = $current_page - 5; //previous link
        $next           = $current_page + 5; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
            $previous_link = $current_page-1;
            //$pagination .= '<h1><a class="item" href="?page=1" title="First">First  </a></h1>'; //first link
            $pagination .= '<li><a href="?page='.$previous_link.'">&laquo;</a></li>'; //previous link
            for($i = $previous; $i < $current_page ; $i++){ //create right-hand side links
                if($i < $current_page && $i > 0){
                    $pagination .= '<li><a href="?page='.$i.'">'.$i.'</a></li>';
                }
            }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="?page='.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){
            $next_link = $current_page+1;
            $pagination .= '<li><a href="?page='.$next_link.'">»</a></li>'; //next link
        }
        $pagination .= "</ul></div>";
    }
    return $pagination; //return pagination links
}

function paginate_function_post($item_per_page, $current_page, $total_records)
{
    unset($_GET['page']);
    $param = http_build_query($_GET);
    $total_pages = ceil($total_records/$item_per_page);
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<div class="pagination__block"><ul class="pagination">';
        $right_links    = $current_page + 5;
        $previous       = $current_page - 5; //previous link
        $next           = $current_page + 5; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
            $previous_link = $current_page-1;
            //$pagination .= '<h1><a class="item" href="?page=1" title="First">First  </a></h1>'; //first link
            $pagination .= '<li><a href="?'.$param.'&page='.$previous_link.'">&laquo;</a></li>'; //previous link
            for($i = $previous; $i < $current_page ; $i++){ //create right-hand side links
                if($i < $current_page && $i > 0){
                    $pagination .= '<li><a href="?'.$param.'&page='.$i.'">'.$i.'</a></li>';
                }
            }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="?'.$param.'&page='.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){
            $next_link = $current_page+1;
            $pagination .= '<li><a href="?'.$param.'&page='.$next_link.'">»</a></li>'; //next link
        }
        $pagination .= "</ul></div>";
    }
    return $pagination; //return pagination links
}

function paginate_function_content($item_per_page, $current_page, $total_records)
{
    unset($_GET['page']);
    $param = http_build_query($_GET);
    $total_pages = ceil($total_records/$item_per_page);
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<div class="pagination__block"><ul class="pagination">';
        $right_links    = $current_page + 5;
        $previous       = $current_page - 5; //previous link
        $next           = $current_page + 5; //next link
        $first_link     = true; //boolean var to decide our first link

        if($current_page > 1){
            $previous_link = $current_page-1;
            //$pagination .= '<h1><a class="item" href="?page=1" title="First">First  </a></h1>'; //first link
            $pagination .= '<li><a href="?'.$param.'&page='.$previous_link.'">&laquo;</a></li>'; //previous link
            for($i = $previous; $i < $current_page ; $i++){ //create right-hand side links
                if($i < $current_page && $i > 0){
                    $pagination .= '<li><a href="?'.$param.'&page='.$i.'">'.$i.'</a></li>';
                }
            }
            $first_link = false; //set first link to false
        }

        if($first_link){ //if current active page is first link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }

        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="?'.$param.'&page='.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){
            $next_link = $current_page+1;
            $pagination .= '<li><a href="?'.$param.'&page='.$next_link.'">»</a></li>'; //next link
        }
        $pagination .= "</ul></div>";
    }
    return $pagination; //return pagination links
}

function array_filter_key_like($id, $array){
    // foreach ($array as $key => $value) {
    //     if (false !== stripos($key, $id.'_')) {
    //         return $key;
    //     }
    // }
    // return false;

    $filtered = array_filter($array, function ($key) use ($id) {
        return strpos($key, $id.'_') === 0;
    }, ARRAY_FILTER_USE_KEY);

    return $filtered;
}

function dd($data = "", $die = TRUE) {
        print_r($data);
        if ($die) {
            die();
        }
    }
