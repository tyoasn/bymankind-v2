<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of My_Controller
 * for load access to all controller
 * @author aris
 */

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('m_access');

		if (!$this->session->userdata('login')) {
            redirect('auth/auth');
        }
        $this->sess_id = $this->session->userdata('user_id');
        $this->group_id = $this->session->userdata('group_id');

        $this->access = $this->m_access->get_access_group_action($this->group_id, 1, '');

        date_default_timezone_set("Asia/Jakarta");
	}

}