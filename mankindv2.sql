-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 29, 2019 at 09:05 AM
-- Server version: 5.5.60-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mankindv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_access`
--

CREATE TABLE `cms_access` (
  `access_id` int(11) UNSIGNED NOT NULL,
  `access_name` varchar(45) DEFAULT NULL,
  `access_alias` text,
  `access_description` text,
  `access_group` varchar(45) DEFAULT NULL,
  `access_directory` varchar(45) DEFAULT NULL,
  `access_order_by` int(11) DEFAULT NULL,
  `access_icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_access`
--

INSERT INTO `cms_access` (`access_id`, `access_name`, `access_alias`, `access_description`, `access_group`, `access_directory`, `access_order_by`, `access_icon`) VALUES
(1, 'Dashboard', 'Dashboard', 'Dashboard', 'dashboard', 'dashboard', 1, 'pe-7s-graph'),
(2, 'Posts', 'Posts > All Posts', 'Posts', 'posts', 'posts', 2, 'pe-7s-news-paper'),
(3, 'Posts', 'Posts > Add New', 'Add Post', 'posts', 'posts/add', 3, 'pe-7s-news-paper'),
(4, 'Posts', 'Posts > Categories', 'Categories', 'posts', 'categories', 4, 'pe-7s-news-paper'),
(5, 'Posts', 'Posts > Tags', 'Tags', 'posts', 'tags', 5, 'pe-7s-news-paper'),
(6, 'Media', 'Media > Library', 'Media', 'media', 'media', 6, 'pe-7s-photo'),
(7, 'Media', 'Media > Add New', 'Media', 'media', 'media/add', 7, 'pe-7s-photo'),
(8, 'Team', 'Team > All Members', 'Team', 'team', 'team', 8, 'pe-7s-network'),
(9, 'Team', 'Team > Add Member', 'Team', 'team', 'team/add', 9, 'pe-7s-network'),
(10, 'Team', 'Team > Division', 'Division', 'team', 'division', 10, 'pe-7s-id'),
(11, 'Content', 'Content > All Contents', 'Content', 'content', 'content', 11, 'pe-7s-note'),
(12, 'Content', 'Content > Add Content', 'Content', 'content', 'content/add', 12, 'pe-7s-note'),
(13, 'Careers', 'Careers > All Careers', 'Career', 'career', 'career', 13, 'pe-7s-id'),
(14, 'Careers', 'Careers > Add Career', 'Career', 'career', 'career/add', 14, 'pe-7s-id'),
(15, 'Portfolio', 'Portfolio > All Portfolios', 'Portfolio', 'portfolio', 'portfolio', 15, 'pe-7s-display1'),
(100, 'Access Management', 'Access Management > User', 'User', 'user', 'user', 100, 'pe-7s-users'),
(101, 'Access Management', 'Access Management > Add New', 'User', 'user', 'user/add', 101, 'pe-7s-users'),
(102, 'Access Management', 'Access Management > Groups', 'Groups', 'group', 'group', 102, 'pe-7s-users'),
(103, 'Settings', 'Settings > General', 'General', 'setting', 'general', 103, 'pe-7s-config'),
(104, 'Settings', 'Settings > Social Media', 'Social Media', 'setting', 'social', 104, 'pe-7s-config');

-- --------------------------------------------------------

--
-- Table structure for table `cms_access_action`
--

CREATE TABLE `cms_access_action` (
  `id` int(11) UNSIGNED NOT NULL,
  `access_id` int(11) UNSIGNED NOT NULL,
  `action_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_access_action`
--

INSERT INTO `cms_access_action` (`id`, `access_id`, `action_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 2, 1),
(8, 2, 2),
(9, 2, 3),
(10, 2, 4),
(11, 2, 5),
(12, 2, 6),
(13, 3, 1),
(14, 3, 2),
(15, 3, 3),
(16, 3, 4),
(17, 3, 5),
(18, 3, 6),
(19, 4, 1),
(20, 4, 2),
(21, 4, 3),
(22, 4, 4),
(23, 4, 5),
(24, 4, 6),
(25, 5, 1),
(26, 5, 2),
(27, 5, 3),
(28, 5, 4),
(29, 5, 5),
(30, 5, 6),
(31, 6, 1),
(32, 6, 2),
(33, 6, 3),
(34, 6, 4),
(35, 6, 5),
(36, 6, 6),
(37, 7, 1),
(38, 7, 2),
(39, 7, 3),
(40, 7, 4),
(41, 7, 5),
(42, 7, 6),
(43, 8, 1),
(44, 8, 2),
(45, 8, 3),
(46, 8, 4),
(47, 8, 5),
(48, 8, 6),
(49, 9, 1),
(50, 9, 2),
(51, 9, 3),
(52, 9, 4),
(53, 9, 5),
(54, 9, 6),
(55, 10, 1),
(56, 10, 2),
(57, 10, 3),
(58, 10, 4),
(59, 10, 5),
(60, 10, 6),
(61, 11, 1),
(62, 11, 2),
(63, 11, 3),
(64, 11, 4),
(65, 11, 5),
(66, 11, 6),
(67, 12, 1),
(68, 12, 2),
(69, 12, 3),
(70, 12, 4),
(71, 12, 5),
(72, 12, 6),
(73, 13, 1),
(74, 13, 2),
(75, 13, 3),
(76, 13, 4),
(77, 13, 5),
(78, 13, 6),
(79, 14, 1),
(80, 14, 2),
(81, 14, 3),
(82, 14, 4),
(83, 14, 5),
(84, 14, 6),
(85, 15, 1),
(86, 15, 2),
(87, 15, 3),
(88, 15, 4),
(89, 15, 5),
(90, 15, 6),
(91, 100, 1),
(92, 100, 2),
(93, 100, 3),
(94, 100, 4),
(95, 100, 5),
(96, 100, 6),
(97, 101, 1),
(98, 101, 2),
(99, 101, 3),
(100, 101, 4),
(101, 101, 5),
(102, 101, 6),
(103, 102, 1),
(104, 102, 2),
(105, 102, 3),
(106, 102, 4),
(107, 102, 5),
(108, 102, 6),
(109, 103, 1),
(110, 103, 2),
(111, 103, 3),
(112, 103, 4),
(113, 103, 5),
(114, 103, 6),
(115, 104, 1),
(116, 104, 2),
(117, 104, 3),
(118, 104, 4),
(119, 104, 5),
(120, 104, 6);

-- --------------------------------------------------------

--
-- Table structure for table `cms_access_group_action`
--

CREATE TABLE `cms_access_group_action` (
  `id` int(11) UNSIGNED NOT NULL,
  `access_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL,
  `action_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_access_group_action`
--

INSERT INTO `cms_access_group_action` (`id`, `access_id`, `group_id`, `action_id`) VALUES
(1, 1, 1, 1),
(2, 1, 1, 2),
(3, 1, 1, 3),
(4, 1, 1, 4),
(5, 1, 1, 5),
(6, 1, 1, 6),
(7, 2, 1, 1),
(8, 2, 1, 2),
(9, 2, 1, 3),
(10, 2, 1, 4),
(11, 2, 1, 5),
(12, 2, 1, 6),
(13, 3, 1, 1),
(14, 3, 1, 2),
(15, 3, 1, 3),
(16, 3, 1, 4),
(17, 3, 1, 5),
(18, 3, 1, 6),
(19, 4, 1, 1),
(20, 4, 1, 2),
(21, 4, 1, 3),
(22, 4, 1, 4),
(23, 4, 1, 5),
(24, 4, 1, 6),
(25, 5, 1, 1),
(26, 5, 1, 2),
(27, 5, 1, 3),
(28, 5, 1, 4),
(29, 5, 1, 5),
(30, 5, 1, 6),
(31, 6, 1, 1),
(32, 6, 1, 2),
(33, 6, 1, 3),
(34, 6, 1, 4),
(35, 6, 1, 5),
(36, 6, 1, 6),
(37, 7, 1, 1),
(38, 7, 1, 2),
(39, 7, 1, 3),
(40, 7, 1, 4),
(41, 7, 1, 5),
(42, 7, 1, 6),
(43, 8, 1, 1),
(44, 8, 1, 2),
(45, 8, 1, 3),
(46, 8, 1, 4),
(47, 8, 1, 5),
(48, 8, 1, 6),
(49, 9, 1, 1),
(50, 9, 1, 2),
(51, 9, 1, 3),
(52, 9, 1, 4),
(53, 9, 1, 5),
(54, 9, 1, 6),
(55, 10, 1, 1),
(56, 10, 1, 2),
(57, 10, 1, 3),
(58, 10, 1, 4),
(59, 10, 1, 5),
(60, 10, 1, 6),
(61, 11, 1, 1),
(62, 11, 1, 2),
(63, 11, 1, 3),
(64, 11, 1, 4),
(65, 11, 1, 5),
(66, 11, 1, 6),
(67, 12, 1, 1),
(68, 12, 1, 2),
(69, 12, 1, 3),
(70, 12, 1, 4),
(71, 12, 1, 5),
(72, 12, 1, 6),
(73, 13, 1, 1),
(74, 13, 1, 2),
(75, 13, 1, 3),
(76, 13, 1, 4),
(77, 13, 1, 5),
(78, 13, 1, 6),
(79, 14, 1, 1),
(80, 14, 1, 2),
(81, 14, 1, 3),
(82, 14, 1, 4),
(83, 14, 1, 5),
(84, 14, 1, 6),
(85, 15, 1, 1),
(86, 15, 1, 2),
(87, 15, 1, 3),
(88, 15, 1, 4),
(89, 15, 1, 5),
(90, 15, 1, 6),
(91, 100, 1, 1),
(92, 100, 1, 2),
(93, 100, 1, 3),
(94, 100, 1, 4),
(95, 100, 1, 5),
(96, 100, 1, 6),
(97, 101, 1, 1),
(98, 101, 1, 2),
(99, 101, 1, 3),
(100, 101, 1, 4),
(101, 101, 1, 5),
(102, 101, 1, 6),
(103, 102, 1, 1),
(104, 102, 1, 2),
(105, 102, 1, 3),
(106, 102, 1, 4),
(107, 102, 1, 5),
(108, 102, 1, 6),
(109, 103, 1, 1),
(110, 103, 1, 2),
(111, 103, 1, 3),
(112, 103, 1, 4),
(113, 103, 1, 5),
(114, 103, 1, 6),
(115, 104, 1, 1),
(116, 104, 1, 2),
(117, 104, 1, 3),
(118, 104, 1, 4),
(119, 104, 1, 5),
(120, 104, 1, 6),
(121, 1, 2, 1),
(122, 1, 2, 2),
(123, 1, 2, 3),
(124, 1, 2, 4),
(125, 1, 2, 5),
(126, 1, 2, 6),
(127, 2, 2, 1),
(128, 2, 2, 2),
(129, 2, 2, 3),
(130, 2, 2, 4),
(131, 2, 2, 5),
(132, 2, 2, 6),
(133, 3, 2, 1),
(134, 3, 2, 2),
(135, 3, 2, 3),
(136, 3, 2, 4),
(137, 3, 2, 5),
(138, 3, 2, 6),
(139, 4, 2, 1),
(140, 4, 2, 2),
(141, 4, 2, 3),
(142, 4, 2, 4),
(143, 4, 2, 5),
(144, 4, 2, 6),
(145, 5, 2, 1),
(146, 5, 2, 2),
(147, 5, 2, 3),
(148, 5, 2, 4),
(149, 5, 2, 5),
(150, 5, 2, 6),
(151, 6, 2, 1),
(152, 6, 2, 2),
(153, 6, 2, 3),
(154, 6, 2, 4),
(155, 6, 2, 5),
(156, 6, 2, 6),
(157, 7, 2, 1),
(158, 7, 2, 2),
(159, 7, 2, 3),
(160, 7, 2, 4),
(161, 7, 2, 5),
(162, 7, 2, 6),
(163, 14, 2, 1),
(164, 14, 2, 2),
(165, 14, 2, 3),
(166, 14, 2, 4),
(167, 14, 2, 5),
(168, 14, 2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `cms_action`
--

CREATE TABLE `cms_action` (
  `action_id` int(11) UNSIGNED NOT NULL,
  `action_name` varchar(45) DEFAULT NULL,
  `action_alias` varchar(45) DEFAULT NULL,
  `action_icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_action`
--

INSERT INTO `cms_action` (`action_id`, `action_name`, `action_alias`, `action_icon`) VALUES
(1, 'Search', 'Search', 'Search'),
(2, 'Add', 'Add', 'fa-plus'),
(3, 'Edit', 'Edit', 'fa-edit'),
(4, 'Delete', 'Delete', 'fa-trash'),
(5, 'View', 'View', 'fa-eye'),
(6, 'Access', 'Access', 'Access');

-- --------------------------------------------------------

--
-- Table structure for table `cms_career`
--

CREATE TABLE `cms_career` (
  `career_id` int(11) NOT NULL,
  `career_title` varchar(255) NOT NULL,
  `career_content` text NOT NULL,
  `career_link` varchar(100) NOT NULL,
  `career_status` int(1) NOT NULL,
  `career_is_trash` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_career`
--

INSERT INTO `cms_career` (`career_id`, `career_title`, `career_content`, `career_link`, `career_status`, `career_is_trash`) VALUES
(3, 'Back End Developer', '<h2 style=\"box-sizing: border-box; font-family: aktiv-grotesk; line-height: 32px; color: rgb(50, 51, 54); margin: 27px 0px 18px; font-size: 28px;\">Job Descriptions</h2>\r\n\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">&nbsp;</p>\r\n\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">\r\n	<li style=\"box-sizing: border-box;\">Build scalable and robust APIs and system.</li>\r\n	<li style=\"box-sizing: border-box;\">Build reusable code and libraries for future use.</li>\r\n	<li style=\"box-sizing: border-box;\">Implement security and data protection.</li>\r\n	<li style=\"box-sizing: border-box;\">Identify and fix bootlenecks and bugs on your system.</li>\r\n</ul>\r\n\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">&nbsp;</p>\r\n\r\n<h2 style=\"box-sizing: border-box; font-family: aktiv-grotesk; line-height: 32px; color: rgb(50, 51, 54); margin: 27px 0px 18px; font-size: 28px;\">Requirements</h2>\r\n\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">\r\n	<li style=\"box-sizing: border-box;\">Bachelor Degree in Computer Science, Information technology, or related subjects.</li>\r\n	<li style=\"box-sizing: border-box;\">Minimum 1 year experience / Fresh Graduates may apply.</li>\r\n	<li style=\"box-sizing: border-box;\">Profiency in PHP CodeIgniter, Laravel</li>\r\n	<li style=\"box-sizing: border-box;\">Strong attention to details, analytical, and problem solving skills.</li>\r\n	<li style=\"box-sizing: border-box;\">Experienced in working as a team.</li>\r\n	<li style=\"box-sizing: border-box;\">Good communication and inperpersonal skills.</li>\r\n</ul>\r\n', 'http://test.com', 1, 0),
(4, 'Content Writer', '<h2 style=\"box-sizing: border-box; font-family: aktiv-grotesk; line-height: 32px; color: rgb(50, 51, 54); margin: 27px 0px 18px; font-size: 28px;\">Job Descriptions</h2>\r\n\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">&nbsp;</p>\r\n\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">\r\n	<li style=\"box-sizing: border-box;\">Build scalable and robust APIs and system.</li>\r\n	<li style=\"box-sizing: border-box;\">Build reusable code and libraries for future use.</li>\r\n	<li style=\"box-sizing: border-box;\">Implement security and data protection.</li>\r\n	<li style=\"box-sizing: border-box;\">Identify and fix bootlenecks and bugs on your system.</li>\r\n</ul>\r\n\r\n<p style=\"box-sizing: border-box; margin: 0px 0px 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">&nbsp;</p>\r\n\r\n<h2 style=\"box-sizing: border-box; font-family: aktiv-grotesk; line-height: 32px; color: rgb(50, 51, 54); margin: 27px 0px 18px; font-size: 28px;\">Requirements</h2>\r\n\r\n<ul style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 10px; color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 18px;\">\r\n	<li style=\"box-sizing: border-box;\">Bachelor Degree in Computer Science, Information technology, or related subjects.</li>\r\n	<li style=\"box-sizing: border-box;\">Minimum 1 year experience / Fresh Graduates may apply.</li>\r\n	<li style=\"box-sizing: border-box;\">Profiency in PHP CodeIgniter, Laravel</li>\r\n	<li style=\"box-sizing: border-box;\">Strong attention to details, analytical, and problem solving skills.</li>\r\n	<li style=\"box-sizing: border-box;\">Experienced in working as a team.</li>\r\n	<li style=\"box-sizing: border-box;\">Good communication and inperpersonal skills.</li>\r\n</ul>\r\n', 'http://test.com', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_config`
--

CREATE TABLE `cms_config` (
  `config_id` int(11) UNSIGNED NOT NULL,
  `config_key` text,
  `config_value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_config`
--

INSERT INTO `cms_config` (`config_id`, `config_key`, `config_value`) VALUES
(1, 'Site Title', 'Mankind'),
(2, 'Keywords', 'agency, digital, mankind, jakarta, creative'),
(3, 'Description', 'Creative Agency From Jakarta'),
(4, 'Site Logo', '43'),
(5, 'Address', 'Kindo Square C9 \r\nJl. Duren Tiga Raya No. 101. \r\nDuren Tiga, Pancoran, Jakarta Selatan, DKI Jakarta 12760'),
(6, 'Phone', '(+62 21) 2753 2278'),
(7, 'Fax', ''),
(8, 'Email', ' info@bymankind.com'),
(9, 'Homepage', NULL),
(10, 'Facebook', 'https://www.facebook.com/'),
(11, 'Twitter', 'https://twitter.com/'),
(12, 'Youtube', 'https://www.youtube.com/'),
(13, 'Instagram', 'https://www.instagram.com/'),
(14, 'Site Icon Footer', '44'),
(15, 'Favicon', '46'),
(16, 'Linkedin', 'https://www.linkedin.com/');

-- --------------------------------------------------------

--
-- Table structure for table `cms_contents`
--

CREATE TABLE `cms_contents` (
  `post_id` int(11) NOT NULL,
  `post_author` int(11) NOT NULL,
  `post_title` varchar(100) DEFAULT NULL,
  `post_slug` varchar(100) DEFAULT NULL,
  `post_excerpt` text,
  `post_content` text,
  `post_thumbnail` varchar(100) DEFAULT NULL,
  `post_category_id` int(11) NOT NULL,
  `post_is_thumbnail` int(11) DEFAULT NULL,
  `post_is_editor_choice` int(11) DEFAULT '0',
  `post_views` int(11) DEFAULT '0',
  `post_share` int(11) DEFAULT '0',
  `post_created_at` datetime DEFAULT NULL,
  `post_created_by` int(11) DEFAULT NULL,
  `post_modified_at` datetime DEFAULT NULL,
  `post_modified_by` int(11) DEFAULT NULL,
  `post_publish_date` datetime DEFAULT NULL,
  `post_status` int(1) DEFAULT NULL,
  `post_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_contents`
--

INSERT INTO `cms_contents` (`post_id`, `post_author`, `post_title`, `post_slug`, `post_excerpt`, `post_content`, `post_thumbnail`, `post_category_id`, `post_is_thumbnail`, `post_is_editor_choice`, `post_views`, `post_share`, `post_created_at`, `post_created_by`, `post_modified_at`, `post_modified_by`, `post_publish_date`, `post_status`, `post_is_trash`) VALUES
(3, 1, 'This is mankind', 'this-is-mankind', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 11:41:34', 1, '2019-01-26 13:47:24', 1, '2019-01-26 00:00:00', 1, 0),
(4, 1, 'Who we are', 'who-we-are', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 13:55:42', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(5, 1, 'Personalized collaboration', 'personalized-collaboration', NULL, '<p><span style=\"color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 20px; letter-spacing: 1px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></p>\r\n\r\n<p><span style=\"color: rgb(34, 34, 34); font-family: aktiv-grotesk; font-size: 20px; letter-spacing: 1px;\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></p>\r\n', NULL, 0, NULL, 0, 0, 0, '2019-01-26 13:56:46', 1, '2019-01-27 00:04:13', 1, '2019-01-26 00:00:00', 1, 0),
(6, 1, 'How mankind do it', 'how-mankind-do-it', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 13:58:16', 1, '2019-01-26 14:01:55', 1, '2019-01-26 00:00:00', 1, 0),
(7, 1, 'Digital marketing', 'digital-marketing', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:08:01', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(8, 1, 'Mobile development', 'mobile-development', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:11:50', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(9, 1, 'Website development', 'website-development', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:13:02', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(10, 1, 'Advertising', 'advertising', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:13:24', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(11, 1, 'Social media', 'social-media', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:13:37', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(12, 1, 'Branding', 'branding', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:13:46', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(13, 1, 'Mankind Frasa', 'mankind-frasa', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:29:02', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(14, 1, 'Mankind Studio', 'mankind-studio', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:29:19', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(15, 1, 'Mankind Technology', 'mankind-technology', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:29:39', 1, '2019-01-26 14:31:19', 1, '2019-01-26 00:00:00', 1, 0),
(16, 1, 'Career', 'career', NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:38:17', 1, NULL, NULL, '2019-01-26 00:00:00', 1, 0),
(17, 1, 'Current openings', 'current-openings', NULL, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>\r\n', NULL, 0, NULL, 0, 0, 0, '2019-01-26 14:45:15', 1, '2019-01-27 00:03:53', 1, '2019-01-26 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_content_taxonomy`
--

CREATE TABLE `cms_content_taxonomy` (
  `post_taxonomy_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_division`
--

CREATE TABLE `cms_division` (
  `division_id` int(11) NOT NULL,
  `division_name` varchar(45) DEFAULT NULL,
  `division_description` text,
  `division_title` varchar(100) DEFAULT NULL,
  `division_type_id` int(3) NOT NULL,
  `division_created_at` datetime DEFAULT NULL,
  `division_created_by` int(11) DEFAULT NULL,
  `division_modified_at` datetime DEFAULT NULL,
  `division_modified_by` int(11) DEFAULT NULL,
  `division_publishdate` datetime DEFAULT NULL,
  `division_status` int(1) DEFAULT NULL,
  `division_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_division`
--

INSERT INTO `cms_division` (`division_id`, `division_name`, `division_description`, `division_title`, `division_type_id`, `division_created_at`, `division_created_by`, `division_modified_at`, `division_modified_by`, `division_publishdate`, `division_status`, `division_is_trash`) VALUES
(1, 'Management', 'management', NULL, 2, '2019-01-27 03:30:35', 1, '2019-01-27 13:33:51', 1, '2019-01-27 03:30:35', 1, 0),
(2, 'Frasa', 'frasa', NULL, 2, '2019-01-27 14:38:30', 1, NULL, NULL, '2019-01-27 14:38:30', 1, 0),
(3, 'Studio', 'studio', NULL, 2, '2019-01-27 14:38:39', 1, NULL, NULL, '2019-01-27 14:38:39', 1, 0),
(4, 'Technology', 'technology', NULL, 2, '2019-01-27 14:38:50', 1, NULL, NULL, '2019-01-27 14:38:50', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_groups`
--

CREATE TABLE `cms_groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(45) DEFAULT NULL,
  `group_description` text,
  `group_created_at` datetime DEFAULT NULL,
  `group_created_by` int(11) DEFAULT NULL,
  `group_modified_at` datetime DEFAULT NULL,
  `group_modified_by` int(11) DEFAULT NULL,
  `group_status` int(1) DEFAULT NULL,
  `group_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_groups`
--

INSERT INTO `cms_groups` (`group_id`, `group_name`, `group_description`, `group_created_at`, `group_created_by`, `group_modified_at`, `group_modified_by`, `group_status`, `group_is_trash`) VALUES
(1, 'administrator', 'Administrator', '2017-04-20 00:00:00', 1, NULL, NULL, 1, 0),
(2, 'editor', 'Editor', '2017-04-20 00:00:00', 1, NULL, NULL, 1, 0),
(3, 'contributor', 'Contributor', '2017-04-20 00:00:00', 1, NULL, NULL, 1, 0),
(4, 'author', 'Author', '2017-04-20 00:00:00', 1, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_media`
--

CREATE TABLE `cms_media` (
  `media_id` int(11) NOT NULL,
  `media_name` text NOT NULL,
  `media_type` varchar(20) NOT NULL,
  `media_directory` varchar(200) NOT NULL,
  `media_created_at` datetime DEFAULT '2018-10-25 00:00:00',
  `media_created_by` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_media`
--

INSERT INTO `cms_media` (`media_id`, `media_name`, `media_type`, `media_directory`, `media_created_at`, `media_created_by`) VALUES
(38, 'media_1548229773_logobymankind.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2019-01-23 14:49:34', 1),
(43, 'logo_header_1548233609.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(46, 'favicon_1548234182.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(107, 'team1_1548608950_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(108, 'team2_1548608955_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(109, 'team1_1548608999_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(110, 'team2_1548609004_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(111, 'team1_1548609049_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(112, 'team2_1548609054_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(113, 'team1_1548609098_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(114, 'team2_1548609103_john_doe.png', 'image', 'http://localhost/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(116, 'portfolio_1556528039_portfolio_1.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(117, 'portfolio2_1556528040_portfolio_1.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(118, 'portfolio3_1556528042_portfolio_1.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(119, 'portfolio4_1556528043_portfolio_1.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(120, 'portfolio5_1556528044_portfolio_1.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(121, 'portfolio6_1556528045_portfolio_1.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(122, 'portfolio_1556528127_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(123, 'portfolio2_1556528128_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(124, 'portfolio3_1556528130_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(125, 'portfolio4_1556528131_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(126, 'portfolio5_1556528132_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(127, 'portfolio6_1556528133_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(128, 'portfolio_1556528226_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(129, 'portfolio2_1556528228_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(130, 'portfolio3_1556528229_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(131, 'portfolio4_1556528230_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(132, 'portfolio5_1556528231_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1),
(133, 'portfolio6_1556528232_invasion.jpg', 'image', 'http://gun.mankind.id/bymankind-v2/asset_admin/assets/uploads/media/image/', '2018-10-25 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `page_id` int(11) NOT NULL,
  `page_name` varchar(45) DEFAULT NULL,
  `page_description` text,
  `page_title` varchar(100) DEFAULT NULL,
  `page_subtitle` varchar(100) DEFAULT NULL,
  `page_slug` varchar(45) DEFAULT NULL,
  `page_content` text,
  `page_parent_id` int(11) NOT NULL,
  `page_type_id` int(3) NOT NULL,
  `page_url` varchar(100) DEFAULT NULL,
  `page_created_at` datetime DEFAULT NULL,
  `page_created_by` int(11) DEFAULT NULL,
  `page_modified_at` datetime DEFAULT NULL,
  `page_modified_by` int(11) DEFAULT NULL,
  `page_publishdate` datetime DEFAULT NULL,
  `page_photo` varchar(100) DEFAULT NULL,
  `page_status` int(1) DEFAULT NULL,
  `page_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`page_id`, `page_name`, `page_description`, `page_title`, `page_subtitle`, `page_slug`, `page_content`, `page_parent_id`, `page_type_id`, `page_url`, `page_created_at`, `page_created_by`, `page_modified_at`, `page_modified_by`, `page_publishdate`, `page_photo`, `page_status`, `page_is_trash`) VALUES
(1, 'aaa', 'aaa', NULL, NULL, 'aaa', NULL, 0, 2, NULL, '2019-01-23 16:32:23', 1, NULL, NULL, '2019-01-23 16:32:23', NULL, 1, 1),
(2, 'About-1', '', NULL, NULL, 'about-1', NULL, 0, 2, NULL, '2019-01-24 15:02:38', 1, NULL, NULL, '2019-01-24 15:02:38', NULL, 1, 1),
(3, 'About-1', '', NULL, NULL, 'about-1', NULL, 0, 2, NULL, '2019-01-24 15:03:27', 1, NULL, NULL, '2019-01-24 15:03:27', NULL, 1, 1),
(4, 'About - 1', '', NULL, NULL, 'about-1', NULL, 0, 2, NULL, '2019-01-24 15:04:44', 1, NULL, NULL, '2019-01-24 15:04:44', NULL, 1, 1),
(5, 'About - 1', '', NULL, NULL, 'about-1', NULL, 0, 2, NULL, '2019-01-24 15:04:53', 1, NULL, NULL, '2019-01-24 15:04:53', NULL, 1, 1),
(6, 'About-1', '', NULL, NULL, 'about-1', NULL, 0, 2, NULL, '2019-01-24 15:57:28', 1, NULL, NULL, '2019-01-24 15:57:28', NULL, 1, 1),
(7, 'About-1', '', NULL, NULL, 'about-1', NULL, 0, 2, NULL, '2019-01-24 16:12:09', 1, NULL, NULL, '2019-01-24 16:12:09', NULL, 1, 1),
(8, 'a', 'a', NULL, NULL, 'a', NULL, 0, 2, NULL, '2019-01-24 16:42:51', 1, NULL, NULL, '2019-01-24 16:42:51', NULL, 1, 1),
(9, 'e', 'e', NULL, NULL, 'e', NULL, 0, 2, NULL, '2019-01-25 12:48:01', 1, NULL, NULL, '2019-01-25 12:48:01', NULL, 1, 1),
(10, 'a', 'aaa', NULL, NULL, 'a', NULL, 0, 2, NULL, '2019-01-26 08:06:22', 1, NULL, NULL, '2019-01-26 08:06:22', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_page_media`
--

CREATE TABLE `cms_page_media` (
  `page_media_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_page_type`
--

CREATE TABLE `cms_page_type` (
  `page_type_id` int(3) NOT NULL,
  `page_type_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_page_type`
--

INSERT INTO `cms_page_type` (`page_type_id`, `page_type_name`) VALUES
(1, 'page_type'),
(2, 'categories_type'),
(3, 'custom_link_type');

-- --------------------------------------------------------

--
-- Table structure for table `cms_portfolio`
--

CREATE TABLE `cms_portfolio` (
  `portfolio_id` int(11) NOT NULL,
  `portfolio_author` int(11) NOT NULL,
  `portfolio_title` varchar(100) DEFAULT NULL,
  `portfolio_link` varchar(100) DEFAULT NULL,
  `portfolio_slug` varchar(100) DEFAULT NULL,
  `portfolio_excerpt` text,
  `portfolio_content` text,
  `portfolio_content_2` text,
  `portfolio_content_3` text,
  `portfolio_content_4` text,
  `portfolio_content_5` text,
  `portfolio_content_6` text,
  `portfolio_content_7` text,
  `portfolio_thumbnail` varchar(100) DEFAULT NULL,
  `portfolio_thumbnail_2` varchar(100) DEFAULT NULL,
  `portfolio_thumbnail_3` varchar(100) DEFAULT NULL,
  `portfolio_thumbnail_4` varchar(100) DEFAULT NULL,
  `portfolio_thumbnail_5` varchar(100) DEFAULT NULL,
  `portfolio_thumbnail_6` varchar(100) DEFAULT NULL,
  `portfolio_is_thumbnail` int(11) DEFAULT NULL,
  `portfolio_created_at` datetime DEFAULT NULL,
  `portfolio_created_by` int(11) DEFAULT NULL,
  `portfolio_modified_at` datetime DEFAULT NULL,
  `portfolio_modified_by` int(11) DEFAULT NULL,
  `portfolio_publish_date` datetime DEFAULT NULL,
  `portfolio_status` int(1) DEFAULT NULL,
  `portfolio_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_portfolio`
--

INSERT INTO `cms_portfolio` (`portfolio_id`, `portfolio_author`, `portfolio_title`, `portfolio_link`, `portfolio_slug`, `portfolio_excerpt`, `portfolio_content`, `portfolio_content_2`, `portfolio_content_3`, `portfolio_content_4`, `portfolio_content_5`, `portfolio_content_6`, `portfolio_content_7`, `portfolio_thumbnail`, `portfolio_thumbnail_2`, `portfolio_thumbnail_3`, `portfolio_thumbnail_4`, `portfolio_thumbnail_5`, `portfolio_thumbnail_6`, `portfolio_is_thumbnail`, `portfolio_created_at`, `portfolio_created_by`, `portfolio_modified_at`, `portfolio_modified_by`, `portfolio_publish_date`, `portfolio_status`, `portfolio_is_trash`) VALUES
(1, 1, 'Super Invasion', 'https://invasion.supermusic.id', 'super-invasion', NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', '<ul>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>\r\n	<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>\r\n</ul>\r\n', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', '', '', '', NULL, '', '', 1, '2019-04-29 15:55:34', 1, '2019-04-29 16:00:24', 1, '2019-04-29 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_posts`
--

CREATE TABLE `cms_posts` (
  `post_id` int(11) NOT NULL,
  `post_author` int(11) NOT NULL,
  `post_title` varchar(100) DEFAULT NULL,
  `post_subtitle` varchar(100) DEFAULT NULL,
  `post_slug` varchar(100) DEFAULT NULL,
  `post_excerpt` text,
  `post_content` text,
  `post_thumbnail` varchar(100) DEFAULT NULL,
  `post_category_id` int(11) NOT NULL,
  `post_is_thumbnail` int(11) DEFAULT NULL,
  `post_is_editor_choice` int(11) DEFAULT '0',
  `post_views` int(11) DEFAULT '0',
  `post_share` int(11) DEFAULT '0',
  `post_created_at` datetime DEFAULT NULL,
  `post_created_by` int(11) DEFAULT NULL,
  `post_modified_at` datetime DEFAULT NULL,
  `post_modified_by` int(11) DEFAULT NULL,
  `post_publish_date` datetime DEFAULT NULL,
  `post_status` int(1) DEFAULT NULL,
  `post_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_post_media`
--

CREATE TABLE `cms_post_media` (
  `post_media_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cms_post_taxonomy`
--

CREATE TABLE `cms_post_taxonomy` (
  `post_taxonomy_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_post_taxonomy`
--

INSERT INTO `cms_post_taxonomy` (`post_taxonomy_id`, `post_id`, `taxonomy_id`) VALUES
(1, 2, 1),
(18, 3, 4),
(20, 6, 4),
(21, 15, 4),
(26, 17, 4),
(27, 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `cms_section_page`
--

CREATE TABLE `cms_section_page` (
  `page_id` int(11) NOT NULL,
  `page_name` varchar(45) DEFAULT NULL,
  `page_description` text,
  `page_title` varchar(100) DEFAULT NULL,
  `page_subtitle` varchar(100) DEFAULT NULL,
  `page_slug` varchar(45) DEFAULT NULL,
  `page_content` text,
  `page_parent_id` int(11) NOT NULL,
  `page_type_id` int(3) NOT NULL,
  `page_url` varchar(100) DEFAULT NULL,
  `page_created_at` datetime DEFAULT NULL,
  `page_created_by` int(11) DEFAULT NULL,
  `page_modified_at` datetime DEFAULT NULL,
  `page_modified_by` int(11) DEFAULT NULL,
  `page_publishdate` datetime DEFAULT NULL,
  `page_photo` varchar(100) DEFAULT NULL,
  `page_status` int(1) DEFAULT NULL,
  `page_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_section_page`
--

INSERT INTO `cms_section_page` (`page_id`, `page_name`, `page_description`, `page_title`, `page_subtitle`, `page_slug`, `page_content`, `page_parent_id`, `page_type_id`, `page_url`, `page_created_at`, `page_created_by`, `page_modified_at`, `page_modified_by`, `page_publishdate`, `page_photo`, `page_status`, `page_is_trash`) VALUES
(1, 'a', 'aaa', NULL, NULL, 'a', NULL, 0, 2, NULL, '2019-01-24 16:40:33', 1, NULL, NULL, '2019-01-24 16:40:33', NULL, 1, 1),
(2, 'a', 'aaa', NULL, NULL, 'a', NULL, 0, 2, NULL, '2019-01-24 17:02:05', 1, NULL, NULL, '2019-01-24 17:02:05', NULL, 1, 1),
(3, 'a', 'aa', NULL, NULL, 'a', NULL, 0, 2, NULL, '2019-01-24 17:09:03', 1, NULL, NULL, '2019-01-24 17:09:03', NULL, 1, 1),
(4, 'q', 'q', NULL, NULL, 'q', NULL, 0, 2, NULL, '2019-01-24 17:21:17', 1, NULL, NULL, '2019-01-24 17:21:17', NULL, 1, 1),
(5, 'a', 'aaa', NULL, NULL, 'a', NULL, 0, 2, NULL, '2019-01-25 17:59:06', 1, NULL, NULL, '2019-01-25 17:59:06', NULL, 1, 1),
(6, 'ww', 'ww', NULL, NULL, 'ww', NULL, 0, 2, NULL, '2019-01-25 18:11:22', 1, NULL, NULL, '2019-01-25 18:11:22', NULL, 1, 1),
(7, 'a', 'aaa', NULL, NULL, 'a', NULL, 0, 2, NULL, '2019-01-26 07:27:49', 1, NULL, NULL, '2019-01-26 07:27:49', NULL, 1, 1),
(8, 'sad', 'sa', NULL, NULL, 'sad', NULL, 2, 2, NULL, '2019-01-26 10:53:31', 1, NULL, NULL, '2019-01-26 10:53:31', NULL, 1, 1),
(9, 'About-1', '', NULL, NULL, 'about-1', NULL, 0, 2, NULL, '2019-01-26 11:03:55', 1, NULL, NULL, '2019-01-26 11:03:55', NULL, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_taxonomy`
--

CREATE TABLE `cms_taxonomy` (
  `taxonomy_id` int(11) NOT NULL,
  `taxonomy_name` text,
  `taxonomy_description` text,
  `taxonomy_slug` text,
  `taxonomy_type` text,
  `taxonomy_created_at` datetime DEFAULT NULL,
  `taxonomy_created_by` int(11) DEFAULT NULL,
  `taxonomy_modified_at` datetime DEFAULT NULL,
  `taxonomy_modified_by` int(11) DEFAULT NULL,
  `taxonomy_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_taxonomy`
--

INSERT INTO `cms_taxonomy` (`taxonomy_id`, `taxonomy_name`, `taxonomy_description`, `taxonomy_slug`, `taxonomy_type`, `taxonomy_created_at`, `taxonomy_created_by`, `taxonomy_modified_at`, `taxonomy_modified_by`, `taxonomy_is_trash`) VALUES
(4, '', '', '', 'tag', '2019-01-26 13:45:04', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_team`
--

CREATE TABLE `cms_team` (
  `team_id` int(11) NOT NULL,
  `team_author` int(11) NOT NULL,
  `team_name` varchar(100) DEFAULT NULL,
  `team_role` varchar(100) DEFAULT NULL,
  `team_thumbnail` varchar(100) DEFAULT NULL,
  `team_thumbnail_2` varchar(100) DEFAULT NULL,
  `team_is_thumbnail` int(11) DEFAULT NULL,
  `team_category_id` int(11) NOT NULL,
  `team_created_at` datetime DEFAULT NULL,
  `team_created_by` int(11) DEFAULT NULL,
  `team_modified_at` datetime DEFAULT NULL,
  `team_modified_by` int(11) DEFAULT NULL,
  `team_publish_date` datetime DEFAULT NULL,
  `team_status` int(1) DEFAULT NULL,
  `team_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_team`
--

INSERT INTO `cms_team` (`team_id`, `team_author`, `team_name`, `team_role`, `team_thumbnail`, `team_thumbnail_2`, `team_is_thumbnail`, `team_category_id`, `team_created_at`, `team_created_by`, `team_modified_at`, `team_modified_by`, `team_publish_date`, `team_status`, `team_is_trash`) VALUES
(13, 0, 'John Doe', 'CEO', '107', '108', 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 0),
(14, 0, 'John Doe', 'Director', '109', '110', 1, 2, NULL, NULL, NULL, NULL, NULL, 1, 0),
(15, 0, 'John Doe', 'Director', '111', '112', 1, 3, NULL, NULL, NULL, NULL, NULL, 1, 0),
(16, 0, 'John Doe', 'Director', '113', '114', 1, 4, NULL, NULL, NULL, NULL, NULL, 1, 0),
(17, 0, 'John Doe', 'CEO', '107', '108', 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 0),
(18, 0, 'John Doe', 'CEO', '107', '108', 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 0),
(19, 0, 'John Doe', 'Director', '109', '110', 1, 2, NULL, NULL, NULL, NULL, NULL, 1, 0),
(20, 0, 'John Doe', 'Director', '109', '110', 1, 2, NULL, NULL, NULL, NULL, NULL, 1, 0),
(21, 0, 'John Doe', 'Director', '111', '112', 1, 3, NULL, NULL, NULL, NULL, NULL, 1, 0),
(22, 0, 'John Doe', 'Director', '111', '112', 1, 3, NULL, NULL, NULL, NULL, NULL, 1, 0),
(23, 0, 'John Doe', 'Director', '113', '114', 1, 4, NULL, NULL, NULL, NULL, NULL, 1, 0),
(24, 0, 'John Doe', 'Director', '113', '114', 1, 4, NULL, NULL, NULL, NULL, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `user_id` int(11) NOT NULL,
  `user_first_name` varchar(100) DEFAULT NULL,
  `user_last_name` varchar(100) DEFAULT NULL,
  `user_gender` enum('M','F') DEFAULT NULL,
  `user_address` text,
  `user_phone` varchar(20) DEFAULT NULL,
  `user_mobile` varchar(20) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_photo` text,
  `group_id` int(11) NOT NULL,
  `user_username` varchar(25) NOT NULL,
  `user_password` varchar(255) DEFAULT '',
  `user_activation_code` varchar(45) DEFAULT NULL,
  `user_cookie` varchar(100) DEFAULT NULL,
  `user_forgotten_password_code` varchar(45) DEFAULT NULL,
  `user_forgotten_password_time` int(11) DEFAULT NULL,
  `user_last_login` int(11) DEFAULT NULL,
  `user_created_at` datetime DEFAULT NULL,
  `user_created_by` int(11) DEFAULT NULL,
  `user_modified_at` datetime DEFAULT NULL,
  `user_modified_by` int(11) DEFAULT NULL,
  `user_status` int(1) DEFAULT NULL,
  `user_forgot_status` int(1) DEFAULT NULL,
  `user_is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`user_id`, `user_first_name`, `user_last_name`, `user_gender`, `user_address`, `user_phone`, `user_mobile`, `user_email`, `user_photo`, `group_id`, `user_username`, `user_password`, `user_activation_code`, `user_cookie`, `user_forgotten_password_code`, `user_forgotten_password_time`, `user_last_login`, `user_created_at`, `user_created_by`, `user_modified_at`, `user_modified_by`, `user_status`, `user_forgot_status`, `user_is_trash`) VALUES
(1, 'Admin', 'Mankind', 'M', 'duren tiga', '(+62 21) 2753 2278', '089658155683', 'register@bymankind.com', '1548300770.png', 1, 'admin', '3cc31cd246149aec68079241e71e98f6', '20170427105207n24rs', NULL, 'yv506tznmqwd', 1493289142, 1556527850, '2017-04-27 10:52:07', 1, '2019-04-26 13:31:36', 1, 1, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_access`
--
ALTER TABLE `cms_access`
  ADD PRIMARY KEY (`access_id`);

--
-- Indexes for table `cms_access_action`
--
ALTER TABLE `cms_access_action`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_id` (`access_id`),
  ADD KEY `action_id` (`action_id`);

--
-- Indexes for table `cms_access_group_action`
--
ALTER TABLE `cms_access_group_action`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_id` (`access_id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `action_id` (`action_id`);

--
-- Indexes for table `cms_action`
--
ALTER TABLE `cms_action`
  ADD PRIMARY KEY (`action_id`);

--
-- Indexes for table `cms_career`
--
ALTER TABLE `cms_career`
  ADD PRIMARY KEY (`career_id`);

--
-- Indexes for table `cms_config`
--
ALTER TABLE `cms_config`
  ADD PRIMARY KEY (`config_id`);

--
-- Indexes for table `cms_contents`
--
ALTER TABLE `cms_contents`
  ADD PRIMARY KEY (`post_id`,`post_category_id`),
  ADD KEY `post_category_id` (`post_category_id`);

--
-- Indexes for table `cms_content_taxonomy`
--
ALTER TABLE `cms_content_taxonomy`
  ADD PRIMARY KEY (`post_taxonomy_id`,`post_id`,`taxonomy_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `taxonomy_id` (`taxonomy_id`);

--
-- Indexes for table `cms_division`
--
ALTER TABLE `cms_division`
  ADD PRIMARY KEY (`division_id`,`division_type_id`),
  ADD KEY `page_type_id` (`division_type_id`);

--
-- Indexes for table `cms_groups`
--
ALTER TABLE `cms_groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `cms_media`
--
ALTER TABLE `cms_media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`page_id`,`page_type_id`),
  ADD KEY `page_type_id` (`page_type_id`);

--
-- Indexes for table `cms_page_media`
--
ALTER TABLE `cms_page_media`
  ADD PRIMARY KEY (`page_media_id`);

--
-- Indexes for table `cms_page_type`
--
ALTER TABLE `cms_page_type`
  ADD PRIMARY KEY (`page_type_id`);

--
-- Indexes for table `cms_portfolio`
--
ALTER TABLE `cms_portfolio`
  ADD PRIMARY KEY (`portfolio_id`);

--
-- Indexes for table `cms_posts`
--
ALTER TABLE `cms_posts`
  ADD PRIMARY KEY (`post_id`,`post_category_id`),
  ADD KEY `post_category_id` (`post_category_id`);

--
-- Indexes for table `cms_post_media`
--
ALTER TABLE `cms_post_media`
  ADD PRIMARY KEY (`post_media_id`,`post_id`,`media_id`),
  ADD KEY `media_id` (`media_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `cms_post_taxonomy`
--
ALTER TABLE `cms_post_taxonomy`
  ADD PRIMARY KEY (`post_taxonomy_id`,`post_id`,`taxonomy_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `taxonomy_id` (`taxonomy_id`);

--
-- Indexes for table `cms_section_page`
--
ALTER TABLE `cms_section_page`
  ADD PRIMARY KEY (`page_id`,`page_type_id`),
  ADD KEY `page_type_id` (`page_type_id`);

--
-- Indexes for table `cms_taxonomy`
--
ALTER TABLE `cms_taxonomy`
  ADD PRIMARY KEY (`taxonomy_id`);

--
-- Indexes for table `cms_team`
--
ALTER TABLE `cms_team`
  ADD PRIMARY KEY (`team_id`,`team_category_id`),
  ADD KEY `post_category_id` (`team_category_id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`user_id`,`group_id`,`user_username`),
  ADD KEY `group_id` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_access`
--
ALTER TABLE `cms_access`
  MODIFY `access_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `cms_access_action`
--
ALTER TABLE `cms_access_action`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `cms_access_group_action`
--
ALTER TABLE `cms_access_group_action`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=169;

--
-- AUTO_INCREMENT for table `cms_action`
--
ALTER TABLE `cms_action`
  MODIFY `action_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cms_career`
--
ALTER TABLE `cms_career`
  MODIFY `career_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_config`
--
ALTER TABLE `cms_config`
  MODIFY `config_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_contents`
--
ALTER TABLE `cms_contents`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `cms_content_taxonomy`
--
ALTER TABLE `cms_content_taxonomy`
  MODIFY `post_taxonomy_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_division`
--
ALTER TABLE `cms_division`
  MODIFY `division_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_groups`
--
ALTER TABLE `cms_groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_media`
--
ALTER TABLE `cms_media`
  MODIFY `media_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cms_page_media`
--
ALTER TABLE `cms_page_media`
  MODIFY `page_media_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_page_type`
--
ALTER TABLE `cms_page_type`
  MODIFY `page_type_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cms_portfolio`
--
ALTER TABLE `cms_portfolio`
  MODIFY `portfolio_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_posts`
--
ALTER TABLE `cms_posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_post_media`
--
ALTER TABLE `cms_post_media`
  MODIFY `post_media_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_post_taxonomy`
--
ALTER TABLE `cms_post_taxonomy`
  MODIFY `post_taxonomy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `cms_section_page`
--
ALTER TABLE `cms_section_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cms_taxonomy`
--
ALTER TABLE `cms_taxonomy`
  MODIFY `taxonomy_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cms_team`
--
ALTER TABLE `cms_team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cms_access_action`
--
ALTER TABLE `cms_access_action`
  ADD CONSTRAINT `cms_access_action_ibfk_1` FOREIGN KEY (`access_id`) REFERENCES `cms_access` (`access_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cms_access_action_ibfk_2` FOREIGN KEY (`action_id`) REFERENCES `cms_action` (`action_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_posts`
--
ALTER TABLE `cms_posts`
  ADD CONSTRAINT `cms_posts_ibfk_1` FOREIGN KEY (`post_category_id`) REFERENCES `cms_pages` (`page_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
