(function($){
  "use strict"; // start of use strict
  
// document ready

  $(document).ready(function(){

// scroll to top

    // hide #back-top first
    $("#back-top").hide();
        
    // fade in #back-top
    $(function () {
      windowT.scroll(function () {
        if ($(this).scrollTop() > 100) {
          $('#back-top').fadeIn();
        } else {
          $('#back-top').fadeOut();
        }
      });

      // scroll body to 0px on click
      $('#back-top a').on('click',function () {
        $('body,html').animate({
          scrollTop: 0
        }, 600);
        return false;
      });
    });

   // function
   
    if ( $('#google-map').length ){
      initMap();
    };
    
  }); 

// platform detect

  var htmlT    = $('html'),
      windowT    = $(window),
      ieDetect = false,
      mobileDetect = false,
      ua = window.navigator.userAgent,
      old_ie = ua.indexOf('MSIE '),
      new_ie = ua.indexOf('Trident/');
    
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)) {
      mobileDetect = true;
      htmlT.addClass('mobile');
    } else {
      htmlT.addClass('no-mobile');
    };   
    
    //IE Detect
    if ((old_ie > -1) || (new_ie > -1)) {
      ieDetect = true;
    };
    
})(jQuery); // End of use strict


// wow function

$(() => {
  new WOW().init();
});

$(() => {
  wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: false, // default
    live: true, // default
  }, );
  wow.init();
});


// slider 

function setREVStartSize(e){                  
  try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
    if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})          
  }catch(d){console.log("Failure at Presize of Slider:"+d)}           
};
var revapi26,
  tpj;  
(function() {     
  if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();  
  function onLoad() {       
    if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
  if(tpj("#rev_slider_26_1").revolution == undefined){
    revslider_showDoubleJqueryError("#rev_slider_26_1");
  }else{
    revapi26 = tpj("#rev_slider_26_1").show().revolution({
      sliderType:"hero",
      jsFileLocation:"",
      sliderLayout:"fullscreen",
      dottedOverlay:"none",
      delay:9000,
      responsiveLevels:[1240,1024,778,480],
      visibilityLevels:[1240,1024,778,480],
      gridwidth:[1920,1024,778,480],
      gridheight:[1080,768,960,720],
      lazyType:"none",
      shadow:0,
      spinner:"off",
      autoHeight:"off",
      fullScreenAutoWidth:"off",
      fullScreenAlignForce:"off",
      fullScreenOffsetContainer: "",
      fullScreenOffset: "",
      disableProgressBar:"on",
      hideThumbsOnMobile:"off",
      hideSliderAtLimit:0,
      hideCaptionAtLimit:0,
      hideAllCaptionAtLilmit:0,
      debugMode:false,
      fallbacks: {
        simplifyAll:"off",
        disableFocusListener:false,
      }
    });
  }; /* END OF revapi call */
  
 }; /* END OF ON LOAD FUNCTION */
}()); /* END OF WRAPPING FUNCTION */

// google maps

var gmMapDiv = $("#google-map");

function initMap(){
  (function($){

      var gmCenterAddress = gmMapDiv.attr("data-address");
      var gmMarkerAddress = gmMapDiv.attr("data-address");
      
      gmMapDiv.gmap3({
          action: "init",
          marker: {
              address: gmMarkerAddress,
              options: {
                  icon: icon_map /* Location marker */
              }
          },
          map: {
              options: {
                  zoom: 17,
                  zoomControl: true,
                  zoomControlOptions: {
                      style: google.maps.ZoomControlStyle.SMALL
                  },
                  mapTypeControl: true, /* hide/show (false/true) mapTypeControl*/
                  scaleControl: true, /*hide/show (false/true) scaleControl */
                  scrollwheel: false, /*hide/show (false/true) scrollwheel*/
                  streetViewControl: false, /*hide/show (false/true) streetViewControl*/
                  draggable: true,
                  styles:[
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#cccccc"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#cccccc"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
]
              }
          }
      });

  })(jQuery);
}

if ( $(window).width() < 768) {      
  function openNav() {
  document.getElementById('mySidenav').style.width = '100%';
  $('.overlay2').show();
}

} 
else {
  //Add your javascript for small screens here 
}

// sidemenu function

function openNav() {
  document.getElementById('mySidenav').style.width = '500px';
  $('.overlay2').show();
}

function closeNav() {
  document.getElementById('mySidenav').style.width = '0';
  $('.overlay2').hide();
}

// login-menu transition 

$('.dropdown').on('show.bs.dropdown', function () {
  $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
});
// Add slideUp animation to Bootstrap dropdown when collapsing.
$('.dropdown').on('hide.bs.dropdown', function () {
  $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
});


// close sidemeu when click outside 

$(document).ready(() => {
  $('.menu-btn').click((e) => {
    e.preventDefault();
    e.stopPropagation();
    $('mySidenav').toggle();
  });


  $('.overlay2').click(() => {
    document.getElementById('mySidenav').style.width = '0';
    $('.overlay2').hide();
  });
});

const dropdown = document.getElementsByClassName('dropdown-btn');
let i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener('click', function () {
    this.classList.toggle('active');
    const dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === 'block') {
      dropdownContent.style.display = 'none';
    } else {
      dropdownContent.style.display = 'block';
    }
  });
}

// division click function

if ( $(window).width() > 768) {      

$("#btn-frasa").hover(function(){
    $('#div-all').css({"display": "none"});
    $('#div-studio').css({"display": "none"});
    $('#div-techno').css({"display": "none"});
    $('#div-frasa').css({"display": "block"});
    $('.desc-frasa1').css({"display": "block"});
    $('.desc-frasa2').css({"display": "block"});
    $('#btn-close').css({"display": "block"});
});
$("#btn-studio").hover(function(){
    $('#div-all').css({"display": "none"});
    $('#div-frasa').css({"display": "none"});
    $('#div-techno').css({"display": "none"});
    $('.desc-studio1').css({"display": "block"});
    $('.desc-studio2').css({"display": "block"});
    $('#div-studio').css({"display": "block"});
    $('#btn-close').css({"display": "block"});
});
$("#btn-techno").hover(function(){
    $('#div-all').css({"display": "none"});
    $('#div-frasa').css({"display": "none"});
    $('#div-studio').css({"display": "none"});
    $('.desc-techno1').css({"display": "block"});
    $('.desc-techno2').css({"display": "block"});
    $('#div-techno').css({"display": "block"});
    $('#btn-close').css({"display": "block"});
});
$("#btn-frasa").mouseleave(function(){
    $('#div-all').css({"display": "block"});
    $('#div-frasa').css({"display": "none"});
    $('#div-studio').css({"display": "none"});
    $('#div-techno').css({"display": "none"});
    $('#btn-close').css({"display": "none"});
    $('.desc-frasa1').css({"display": "none"});
    $('.desc-frasa2').css({"display": "none"});
    $('.desc-studio1').css({"display": "none"});
    $('.desc-studio2').css({"display": "none"});
    $('.desc-techno1').css({"display": "none"});
    $('.desc-techno2').css({"display": "none"});
});
$("#btn-studio").mouseleave(function(){
    $('#div-all').css({"display": "block"});
    $('#div-frasa').css({"display": "none"});
    $('#div-studio').css({"display": "none"});
    $('#div-techno').css({"display": "none"});
    $('#btn-close').css({"display": "none"});
    $('.desc-frasa1').css({"display": "none"});
    $('.desc-frasa2').css({"display": "none"});
    $('.desc-studio1').css({"display": "none"});
    $('.desc-studio2').css({"display": "none"});
    $('.desc-techno1').css({"display": "none"});
    $('.desc-techno2').css({"display": "none"});
});
$("#btn-techno").mouseleave(function(){
    $('#div-all').css({"display": "block"});
    $('#div-frasa').css({"display": "none"});
    $('#div-studio').css({"display": "none"});
    $('#div-techno').css({"display": "none"});
    $('#btn-close').css({"display": "none"});
    $('.desc-frasa1').css({"display": "none"});
    $('.desc-frasa2').css({"display": "none"});
    $('.desc-studio1').css({"display": "none"});
    $('.desc-studio2').css({"display": "none"});
    $('.desc-techno1').css({"display": "none"});
    $('.desc-techno2').css({"display": "none"});
});

// $("#btn-close").click(function(){
//     $('#div-all').css({"display": "block"});
//     $('#div-frasa').css({"display": "none"});
//     $('#div-studio').css({"display": "none"});
//     $('#div-techno').css({"display": "none"});
//     $('#btn-close').css({"display": "none"});
//     $('.desc-frasa1').css({"display": "none"});
//     $('.desc-frasa2').css({"display": "none"});
//     $('.desc-studio1').css({"display": "none"});
//     $('.desc-studio2').css({"display": "none"});
//     $('.desc-techno1').css({"display": "none"});
//     $('.desc-techno2').css({"display": "none"});
// });

} 
else {
  //Add your javascript for small screens here 
}

// $(".thisis-block").hover(function(){
//   $(".overlay-bg").toggleClass("overlay-bg-blue")
// });
// $(".thisis-block").mouseleave(function(){
//   $(".rev_slider_wrapper").toggleClass("rev_slider")
// });
// $(".how-block").hover(function(){
//   $(".overlay-bg").toggleClass("overlay-bg-red")
// });
// $(".how-block").mouseleave(function(){
//   $(".rev_slider_wrapper").toggleClass("rev_slider")
// });
// $(".we-block").hover(function(){
//   $(".overlay-bg").toggleClass("overlay-bg-blue")
// });
// $(".we-block").mouseleave(function(){
//   $(".rev_slider_wrapper").toggleClass("rev_slider")
// });
// $(".doit-block").hover(function(){
//   $(".overlay-bg").toggleClass("overlay-bg-red")
// });
// $(".doit-block").mouseleave(function(){
//   $(".rev_slider_wrapper").toggleClass("rev_slider")
// });
